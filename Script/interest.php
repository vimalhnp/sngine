<?php

// fetch bootstrap
require('bootstrap.php');

try {

    $current_value = $_GET['view'];
    if ($_GET['view'] == "interest_create_success") {
        $_GET['view'] = "";
        $current_value = "";
    }

    $smarty->assign('current_value', $current_value);

    switch ($_GET['view']) {
        case 'single_interest':
            // [1] get main interest info
            $get_interest = $db->query(sprintf("SELECT i1.*, i2.text AS parent_interest FROM `interest_mst` AS i1
                                                LEFT JOIN interest_mst AS i2 ON ( i1.parent_id = i2.interest_id ) WHERE i1.interest_id = %s;", secure($_GET['interest_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_interest->num_rows == 0) {
//                _error(404);
            }
            $interest_data = $get_interest->fetch_assoc();

            $related_interest_arr = array();

            $get_related_interest = $db->query(sprintf("SELECT im.*, ( SELECT count(ui.user_interest_id) FROM user_interest AS ui WHERE ui.interest = im.interest_id AND ui.`status` = 1 ) AS total_follower
                                                        FROM `interest_mst` AS im WHERE im.parent_id = %s AND im.`status` = 1 AND im.interest_id != %s", secure($interest_data['parent_id']), secure($interest_data['interest_id']))) or _error(SQL_ERROR_THROWEN);
            while ($related_interest_data = $get_related_interest->fetch_assoc()) {
                if ($related_interest_data['image'] == '') {
                    $related_interest_data['image'] = $system['system_url'] . '/content/themes/' . $system['theme'] . '/images/blank_group.png';
                } else {
                    $related_interest_data['image'] = $system['system_url'] . '/content/uploads/' . $related_interest_data['image'];
                }
                $i_follow_related = 0;

                $get_follow = $db->query(sprintf("SELECT * FROM `user_interest` WHERE interest = %s AND user_id = %s", secure($related_interest_data['interest_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_follow->num_rows > 0) {
                    $i_follow_related = 1;
                    //continue;
                }

                $related_interest_data['i_follow_related'] = $i_follow_related;
                $related_interest_arr[] = $related_interest_data;
            }
    // page header
    page_header($system['system_title'] . ' - ' . $interest_data['text']);
    // assign variables
    $smarty->assign('interest_data', $interest_data);
    $smarty->assign('view', $_GET['view']);

    $i = 0;
    $totalSessions = 0;
    $totalQues = 0;
    $totalAttendes = 0;
    $sessionInterestArr = array();
    $sessions = array(
        'upcoming' => array(),
        'running' => array(),
        'expired' => array()
    );

    $getTotalAttendes = $db->query(sprintf("SELECT count(*) AS cnt2 FROM `user_interest` WHERE `interest` = %s AND `status` = '1' ", secure($interest_data['interest_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    if ($getTotalAttendes->num_rows > 0) {
        $cnt = $getTotalAttendes->fetch_assoc();
        $totalAttendes += intval($cnt['cnt2']);
    }

    /* get the thank you button */
    $i_follow = 0;
    if ($user->_logged_in) {
        $get_follow = $db->query(sprintf("SELECT * FROM `user_interest` WHERE interest = %s AND user_id = %s", secure($interest_data['interest_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_follow->num_rows > 0) {
            $i_follow = 1;
        }
    }

    $get_seession_interest = $db->query(sprintf("SELECT
                                        sessions.*
                                FROM
                                        `sessions_interest`
                                INNER JOIN sessions ON sessions.sessions_id = sessions_interest.sessions_id
                                WHERE
                                        sessions_interest.interest = %s     
                    AND sessions.`status` != 0;", secure($interest_data['interest_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
    while ($session_interest = $get_seession_interest->fetch_assoc()) {
        $event_date = $session_interest['event_date'] . " " . $session_interest['event_time'];
        $endtime = strtotime("+" . $session_interest['event_duration'] . " minutes", strtotime($event_date));
        $event_expired = FALSE;
        if (intval($endtime) > 0 && time() >= $endtime) {
            $event_expired = TRUE;
        }
        $session_interest['event_expired'] = $event_expired;

        /* get the connection */
        $session_interest['i_attend'] = false;
        if ($user->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_interest['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_likes->num_rows > 0) {
                $session_interest['i_attend'] = true;
            }
        }
        $session_interest['i_join'] = false;
        if ($user->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_interest['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_likes->num_rows > 0) {
                $session_interest['i_join'] = true;
            }
        }
        /* get the connection */
        $session_interest['i_accept'] = false;
        if ($user->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_interest['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_likes->num_rows > 0) {
                $session_interest['i_accept'] = true;
            }
        }

        /* get the thank you button */
        $session_interest['i_thanks'] = 0;
        if ($user->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_interest['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_likes->num_rows > 0) {
                $session_interest['i_thanks'] = 1;
            }
        }

        $session_interest['created_by'] = $user->get_user_by_id($session_interest['created_by']);
        $session_interest['created_by'] = $session_interest['created_by'][0];

        $presentors = '';
        $addEditVenue = '';
        if ($session_interest['presentors'] != '' && !$event_expired) {
            $session_interest['presentors'] = $user->get_user_by_id($session_interest['presentors']);
            if (isset($session_interest['presentors']) && count($session_interest['presentors']) > 0) {
                foreach ($session_interest['presentors'] as $pv) {
                    if ($pv['user_id'] == $user->_data['user_id']) {
                        $addEditVenue .= '<li>
                                        <div class="about-list-item">
                                            <i class="fa fa-pencil fa-fw fa-lg"></i>
                                            <a href="" class="nav" style="width:150px;" data-toggle="modal" data-url="session/accept_editor.php?sessions_id=' . $_GET['sessions_id'] . '&action=edit_info">Update Info</a>
                                        </div>
                                    </li>';
                    }
                    $presentors .= $pv['user_name'] . ",";
                }
            }
            $presentors = rtrim($presentors, ",");
        }
        $session_interest['addEditVenue'] = $addEditVenue;
        $session_interest['presentors'] = $presentors;

        $session_interest['total_attend'] = 0;
        if ($user->_logged_in) {
            $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_interest['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($get_likes->num_rows > 0) {
                $cnt = $get_likes->fetch_assoc();
                $session_interest['total_attend'] = $cnt['cnt'];
//                $totalAttendes += intval($cnt['cnt']);
            }
        }


        if ($session_interest['status'] == 2) {
            $totalSessions++;
            $sessions['upcoming'][] = $session_interest;
        } else if ($session_interest['status'] == 3) {
            $totalSessions++;
            $sessions['running'][] = $session_interest;
        } else if ($session_interest['status'] == 4) {
            $totalSessions++;
            $sessions['expired'][] = $session_interest;
        }

        $get_totalQues = $db->query(sprintf("SELECT count(sessions_qa_id) AS cnt1 FROM `sessions_qa` WHERE sessions_id = %s AND status = 1", secure($session_interest['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_totalQues->num_rows > 0) {
            $sessQues = $get_totalQues->fetch_assoc();
            $totalQues += intval($sessQues['cnt1']);
        }

        $i++;
    }

    $smarty->assign('totalSessions', $totalSessions);
    $smarty->assign('totalQues', $totalQues);
    $smarty->assign('totalAttendes', $totalAttendes);
    $smarty->assign('i_follow', $i_follow);
    $smarty->assign('related_interest_arr', $related_interest_arr);
    $smarty->assign('sessionInterestArr', $sessions);

    // page footer
    page_footer("single_interest");
    exit();
    break;
default:
    _error(404);
    break;
}
} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page header
page_header($system['system_title'] . ' - ' . $interests['page_title']);

// page footer
page_footer("interests");
?>