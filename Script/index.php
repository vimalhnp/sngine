<?php

/**
 * index
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('bootstrap.php');

try {

    // check user logged in
    if (!$user->_logged_in) {

        // page header
        page_header(__("Welcome to") . ' ' . $system['system_title']);

        // get random users
        /* $random_users = array();
          $get_random_users = $db->query("SELECT user_name, user_fullname, user_gender, user_picture FROM users ORDER BY RAND() LIMIT 4") or _error(SQL_ERROR_THROWEN);
          if($get_random_users->num_rows > 0) {
          while($random_user = $get_random_users->fetch_assoc()) {
          $random_user['user_picture'] = User::get_picture($random_user['user_picture'], $random_user['user_gender']);
          $random_users[] = $random_user;
          }
          } */
        /* assign variables */
//		$smarty->assign('random_users', $random_users);
        // get custom fields
        $smarty->assign('custom_fields', $user->get_custom_fields());
    } else {

        // user access
        user_access();

        // get view content
        switch ($_GET['view']) {
            case '':
                // page header
                page_header($system['system_title']);

                // prepare publisher
                $smarty->assign('market_categories', $user->get_market_categories());
                $smarty->assign('feelings', get_feelings());
                $smarty->assign('feelings_types', get_feelings_types());

                // get boosted post
                if ($system['packages_enabled']) {
                    $boosted_post = $user->get_boosted_post();
                    $smarty->assign('boosted_post', $boosted_post);
                }

                // get posts (newsfeed)
                $posts = $user->get_posts();
                /* assign variables */
                $smarty->assign('posts', $posts);
                break;

            case 'saved':
                // page header
                page_header(__("Saved Posts"));

                // get posts (saved)
                $posts = $user->get_posts(array('get' => 'saved'));
                /* assign variables */
                $smarty->assign('posts', $posts);
                break;

            case 'products':
                // page header
                page_header(__("My Products"));

                // get posts (saved)
                $posts = $user->get_posts(array('get' => 'posts_profile', 'id' => $user->_data['user_id'], 'filter' => 'product'));
                /* assign variables */
                $smarty->assign('posts', $posts);
                break;

            case 'pages':
                // page header
                page_header(__("Pages"));
                break;

            case 'groups':
                // page header
                page_header(__("Groups"));
                break;

            case 'interest':

                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Interest"));

                // get games
                $sessions = $user->get_interest();
                /* assign variables */
                $smarty->assign('sessions', $sessions);

                break;

            case 'my_interest':

                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("My Interest"));

                /* search interest */
                $interestArr = array();
                $get_interest = $db->query(sprintf('SELECT * FROM user_interest WHERE user_id = %s AND `status` = 1', secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_interest->num_rows > 0) {
                    while ($interest = $get_interest->fetch_assoc()) {
                        $interestArr[] = $interest['interest'];
                    }
                }

                $html = "";
                $get_rows1 = $db->query("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' order by parent_id") or _error(SQL_ERROR);
                if ($get_rows1->num_rows > 0) {
                    while ($row1 = $get_rows1->fetch_assoc()) {

                        $selected = (isset($interestArr) && in_array($row1['interest_id'], $interestArr)) ? 'selected="selected"' : "";

                        if ($row1['parent_id'] == 0) {
                            $html.= '<option value="' . $row1['interest_id'] . '" ' . $selected . '>' . $row1['text'] . '</option>';
                        } else {
                            $tmpHtml = "";
                            $parentHtmlArr = $user->buildParentTree($row1['parent_id']);

                            $new_parent_html = implode("/", $parentHtmlArr);

                            $html.= '<option value="' . $row1['interest_id'] . '" data-section="' . $new_parent_html . '" ' . $selected . '>' . $row1['text'] . '</option>';
                        }
                    }
                }

                // assign variables
                $smarty->assign('html', $html);

                break;

            case 'create_page':
                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Create Page"));

                // get pages categories
                $categories = $user->get_pages_categories();
                /* assign variables */
                $smarty->assign('categories', $categories);
                break;

            case 'create_group':
                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Create Group"));
                break;

            case 'create_session':
                // page header

                $html = "";
                $get_rows1 = $db->query("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' order by parent_id") or _error(SQL_ERROR);
                if ($get_rows1->num_rows > 0) {
                    while ($row1 = $get_rows1->fetch_assoc()) {

                        if ($row1['parent_id'] == 0) {
                            $html.= '<option value="' . $row1['interest_id'] . '">' . $row1['text'] . '</option>';
                        } else {
                            $tmpHtml = "";
                            $parentHtmlArr = $user->buildParentTree($row1['parent_id']);

                            $new_parent_html = implode("/", $parentHtmlArr);

                            $html.= '<option value="' . $row1['interest_id'] . '" data-section="' . $new_parent_html . '">' . $row1['text'] . '</option>';
                        }
                    }
                }

                // assign variables
                $smarty->assign('interest_html', $html);

                page_header($system['system_title'] . " &rsaquo; " . __("Create Session"));
                break;

            case 'conduct_session':
                // page header

                $html = "";
                $get_rows1 = $db->query("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' order by parent_id") or _error(SQL_ERROR);
                if ($get_rows1->num_rows > 0) {
                    while ($row1 = $get_rows1->fetch_assoc()) {

                        if ($row1['parent_id'] == 0) {
                            $html.= '<option value="' . $row1['interest_id'] . '">' . $row1['text'] . '</option>';
                        } else {
                            $tmpHtml = "";
                            $parentHtmlArr = $user->buildParentTree($row1['parent_id']);

                            $new_parent_html = implode("/", $parentHtmlArr);

                            $html.= '<option value="' . $row1['interest_id'] . '" data-section="' . $new_parent_html . '">' . $row1['text'] . '</option>';
                        }
                    }
                }

                // assign variables
                $smarty->assign('interest_html', $html);

                page_header($system['system_title'] . " &rsaquo; " . __("Conduct Session"));
                break;

            case 'session_list':
                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Manage Session"));

                $rows_active = [];
                // get Active Sessions data
                $get_rows1 = $db->query("SELECT * FROM `sessions` WHERE `status` = '2'") or _error(SQL_ERROR);
                if ($get_rows1->num_rows > 0) {
                    while ($row1 = $get_rows1->fetch_assoc()) {
                        $row1['created_by'] = $user->get_user_by_id($row1['created_by']);
                        $row1['created_by'] = $row1['created_by'][0];

                        $rows_active[] = $row1;
                    }
                }
                // assign variables
                $smarty->assign('rows_active', $rows_active);


                break;

//            case 'all_sessions':
//                // page header
//
//                $interestArr = array();
//                $get_my_interest = $db->query(sprintf("SELECT * FROM `user_interest` WHERE `user_id` = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                while ($interest = $get_my_interest->fetch_assoc()) {
//                    $interestArr[] = $interest['interest'];
//                }
//
//                $sessions = [];
//                // get conducting sessions
//                $get_sessions = $db->query(sprintf("SELECT * FROM sessions ORDER BY `event_date` ASC ")) or _error(SQL_ERROR_THROWEN);
//                if ($get_sessions->num_rows == 0) {
//                    _error(404);
//                }
//                while ($row = $get_sessions->fetch_assoc()) {
//
//                    $sessionInterestArr = array();
//                    $get_seession_interest = $db->query(sprintf("SELECT * FROM `sessions_interest` WHERE `sessions_id` = %s", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                    while ($session_interest = $get_seession_interest->fetch_assoc()) {
//                        $sessionInterestArr[] = $session_interest['interest'];
//                    }
//
//                    if (count($sessionInterestArr) > 0 && !empty($sessionInterestArr)) {
//
//                        $resultArr = array_intersect($sessionInterestArr, $interestArr);
//
//                        if (isset($resultArr) && count($resultArr) > 0) {
//                            $row['created_by'] = $user->get_user_by_id($row['created_by']);
//                            $row['created_by'] = $row['created_by'][0];
//
//                            $presentors = '';
//                            if ($row['presentors'] != '') {
//                                $row['presentors'] = $user->get_user_by_id($row['presentors']);
//                                if (isset($row['presentors']) && count($row['presentors']) > 0) {
//                                    foreach ($row['presentors'] as $pv) {
//                                        $presentors.=$pv['user_name'] . ",";
//                                    }
//                                }
//                                $presentors = rtrim($presentors, ",");
//                            }
//                            $row['presentors'] = $presentors;
//
//                            /* get total friends attendes */
//                            $row['session_attend_friends'] = "";
//
//                            $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", secure(1, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                            $my_friends_ids = $my_friends->fetch_assoc();
//
//                            if ($row['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
//                                $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ", secure($row['sessions_id'], 'int'), $my_friends_ids['my_friends'])) or _error(SQL_ERROR_THROWEN);
//                                if ($sessions_attends->num_rows > 0) {
//                                    while ($session_attend_users = $sessions_attends->fetch_assoc()) {
//
//                                        $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                                        $friends_data = $friends->fetch_assoc();
//
//                                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
//
//                                        $row['session_attend_friends'].=
//                                                '<span class="name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
//                                            </a>
//                                        </span>';
//                                    }
//                                }
//                            }
//
//                            /* get total friends attendes */
//                            $row['session_engaged_peoples'] = "";
//                            $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", secure(1, 'int'), secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                            $user_ids = $my_friends->fetch_assoc();
//
//                            $final_user_ids = "";
//                            if ($user_ids['users'] != "") {
//                                $final_user_ids = $user_ids['users'] . ",";
//                            }
//
//                            $sessions_attends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                            $sessions_user_ids = $sessions_attends->fetch_assoc();
//
//                            if ($sessions_user_ids['users'] != "") {
//                                $final_user_ids.= $sessions_user_ids['users'];
//                            }
//
//                            if ($final_user_ids != "") {
//
//                                $final_user_ids = rtrim($final_user_ids, ",");
//
//                                $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids)) or _error(SQL_ERROR_THROWEN);
//
//                                while ($friends_data = $friends->fetch_assoc()) {
//                                    $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
//
//                                    $row['session_engaged_peoples'].=
//                                            '<span class="name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                                        <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                                            <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
//                                                        </a>
//                                                    </span>';
//                                }
//                            }
//
//                            /* get People attendes */
//                            $row['session_attend_peoples'] = "";
//                            $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                            if ($sessions_attends->num_rows > 0) {
//                                while ($session_attend_users = $sessions_attends->fetch_assoc()) {
//
//                                    $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
//                                    $friends_data = $friends->fetch_assoc();
//
//                                    $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);
//
//                                    $row['session_attend_peoples'].=
//                                            '<span class="name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                        <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_" data-uid="' . $friends_data['user_id'] . '">
//                                            <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
//                                        </a>
//                                    </span>';
//                                }
//                            }
//                            if (strtotime($row['event_date'] . " " . $row['event_time']) == '' || strtotime($row['event_date'] . " " . $row['event_time']) >= time()) {
//                                // event date not given or its upcoming one.
//                                $sessions[2][] = $row;
//                            } else {
//                                $sessions[$row['status']][] = $row;
//                            }
//                        }
//                    }
//
//                    $users_attended_sessions = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($row['sessions_id'], 'int'), $user->_data['user_id'])) or _error(SQL_ERROR_THROWEN);
//                    if ($users_attended_sessions->num_rows > 0) {
//                        $sessions[2][] = $row;
//                    }
//                }
//
//                //get Conducting a session and Requesting For a Session list from DB and store in two array
//                // page header
//                page_header($system['system_title'] . " &rsaquo; " . __("Sessions List"));
//
//                $sessions['page_title'] = 'Sessions List';
//                /* assign variables */
//                $smarty->assign('sessions', $sessions);
//
//                break;
            case 'all_interests':
                // page header

                $interestArr = array();
                $get_my_interest = $db->query(sprintf("SELECT * FROM `user_interest` WHERE `user_id` = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                while ($interest = $get_my_interest->fetch_assoc()) {
                    $interestArr[] = $interest['interest'];
                }

                $sessions = [];
                // get conducting sessions 
                $get_sessions = $db->query(sprintf("SELECT * FROM sessions ORDER BY `event_date` ASC ")) or _error(SQL_ERROR_THROWEN);
                if ($get_sessions->num_rows == 0) {
                    _error(404);
                }
                while ($row = $get_sessions->fetch_assoc()) {

                    $sessionInterestArr = array();
                    $get_seession_interest = $db->query(sprintf("SELECT * FROM `sessions_interest` WHERE `sessions_id` = %s", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    while ($session_interest = $get_seession_interest->fetch_assoc()) {
                        $sessionInterestArr[] = $session_interest['interest'];
                    }

                    if (count($sessionInterestArr) > 0 && !empty($sessionInterestArr)) {

                        $resultArr = array_intersect($sessionInterestArr, $interestArr);

                        if (isset($resultArr) && count($resultArr) > 0) {
                            $row['created_by'] = $user->get_user_by_id($row['created_by']);
                            $row['created_by'] = $row['created_by'][0];

                            $presentors = '';
                            if ($row['presentors'] != '') {
                                $row['presentors'] = $user->get_user_by_id($row['presentors']);
                                if (isset($row['presentors']) && count($row['presentors']) > 0) {
                                    foreach ($row['presentors'] as $pv) {
                                        $presentors.=$pv['user_name'] . ",";
                                    }
                                }
                                $presentors = rtrim($presentors, ",");
                            }
                            $row['presentors'] = $presentors;

                            /* get total friends attendes */
                            $row['session_attend_friends'] = "";

                            $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", secure(1, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            $my_friends_ids = $my_friends->fetch_assoc();

                            if ($row['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
                                $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ", secure($row['sessions_id'], 'int'), $my_friends_ids['my_friends'])) or _error(SQL_ERROR_THROWEN);
                                if ($sessions_attends->num_rows > 0) {
                                    while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                                        $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                                        $friends_data = $friends->fetch_assoc();

                                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                                        $row['session_attend_friends'].=
                                                '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                            </a>
                                        </span>';
                                    }
                                }
                            }

                            /* get total friends attendes */
                            $row['session_engaged_peoples'] = "";
                            $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", secure(1, 'int'), secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            $user_ids = $my_friends->fetch_assoc();

                            $final_user_ids = "";
                            if ($user_ids['users'] != "") {
                                $final_user_ids = $user_ids['users'] . ",";
                            }

                            $sessions_attends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            $sessions_user_ids = $sessions_attends->fetch_assoc();

                            if ($sessions_user_ids['users'] != "") {
                                $final_user_ids.= $sessions_user_ids['users'];
                            }

                            if ($final_user_ids != "") {

                                $final_user_ids = rtrim($final_user_ids, ",");

                                $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids)) or _error(SQL_ERROR_THROWEN);

                                while ($friends_data = $friends->fetch_assoc()) {
                                    $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                                    $row['session_engaged_peoples'].=
                                            '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                                        <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                                            <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                                        </a>
                                                    </span>';
                                }
                            }

                            /* get People attendes */
                            $row['session_attend_peoples'] = "";
                            $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            if ($sessions_attends->num_rows > 0) {
                                while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                                    $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                                    $friends_data = $friends->fetch_assoc();

                                    $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                                    $row['session_attend_peoples'].=
                                            '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                        <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                            <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                        </a>
                                    </span>';
                                }
                            }

                            if (strtotime($row['event_date'] . " " . $row['event_time']) == '' || strtotime($row['event_date'] . " " . $row['event_time']) >= time()) {
                                // event date not given or its upcoming one.
                                $sessions[2][] = $row;
                            } else {
                                $sessions[$row['status']][] = $row;
                            }
                        }
                    }
                }

                //get Conducting a session and Requesting For a Session list from DB and store in two array
                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Sessions List"));

                $sessions['page_title'] = 'Sessions List';
                /* assign variables */
                $smarty->assign('sessions', $sessions);

                break;

            case 'games':
                // games enabled
                if (!$system['games_enabled']) {
                    _error(404);
                }

                // page header
                page_header($system['system_title'] . " &rsaquo; " . __("Games"));

                // get games
                $games = $user->get_games();
                /* assign variables */
                $smarty->assign('games', $games);
                break;

            default:
                _error(404);
                break;
        }
        /* assign variables */
        $smarty->assign('view', $_GET['view']);

        if ($system['packages_enabled']) {
            // get pro members
            $pro_members = $user->get_pro_members();
            /* assign variables */
            $smarty->assign('pro_members', $pro_members);

            // get promoted pages
            $promoted_pages = $user->get_pages(array('promoted' => true));
            /* assign variables */
            $smarty->assign('promoted_pages', $promoted_pages);
        }

        // get new people
        $new_people = $user->get_new_people(0, true);
        /* assign variables */
        $smarty->assign('new_people', $new_people);

        // get new pages
        $new_pages = $user->get_pages(array('suggested' => true));
        /* assign variables */
        $smarty->assign('new_pages', $new_pages);

        // get new groups
        $new_groups = $user->get_groups(array('suggested' => true));
        /* assign variables */
        $smarty->assign('new_groups', $new_groups);

        // get new sessions
        $new_sessions = $user->get_sessions(array('suggested' => true));
        /* assign variables */
        $smarty->assign('new_sessions', $new_sessions);

        // get announcements
        $announcements = $user->announcements();
        /* assign variables */
        $smarty->assign('announcements', $announcements);

        // get ads
        $ads = $user->ads('home');
        /* assign variables */
        $smarty->assign('ads', $ads);

        // get widgets
        $widgets = $user->widgets('home');
        /* assign variables */
        $smarty->assign('widgets', $widgets);
    }
} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page footer
page_footer("index");
?>