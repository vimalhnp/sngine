<?php

/**
 * started
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('bootstrap.php');

// user access
if (!$user->_logged_in) {
    user_login();
}

try {

    /* check registration type */
    if ($system['registration_type'] == "paid" && !$user->_data['user_subscribed']) {
        redirect('/packages');
    }

    /* check if already getted started */
    if (!$system['getting_started'] || $user->_data['user_started']) {
        redirect();
    }

    
    /* get finished */
    if (isset($_GET['finished'])) {
        /* update user */
        $db->query(sprintf("UPDATE users SET user_started = '1' WHERE user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        redirect();
    }

    // get new people
    $new_people = $user->get_new_people(0, true);
    /* assign variables */
    $smarty->assign('new_people', $new_people);

    $html = "";
    $get_rows1 = $db->query("SELECT * FROM `interest_mst` AS im WHERE im.`status` = '1' order by parent_id") or _error(SQL_ERROR);
    if ($get_rows1->num_rows > 0) {
        while ($row1 = $get_rows1->fetch_assoc()) {

            if ($row1['parent_id'] == 0) {
                $html.= '<option value="' . $row1['interest_id'] . '">' . $row1['text'] . '</option>';
            } else {
//                $parentHtmlArr = (isset($parentHtmlArr) && count($parentHtmlArr) > 0) ? $parentHtmlArr : array();
                $tmpHtml = "";
                $parentHtmlArr = $user->buildParentTree($row1['parent_id']);

                $new_parent_html = implode("/", $parentHtmlArr);

                $html.= '<option value="' . $row1['interest_id'] . '" data-section="' . $new_parent_html . '">' . $row1['text'] . '</option>';
            }
        }
    }

    // assign variables
    $smarty->assign('html', $html);
} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page header
page_header($system['system_title'] . " &rsaquo; " . __("Getting Started"));

// page footer
page_footer("started");

?>