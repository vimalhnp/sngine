<?php

// fetch bootstrap
require('bootstrap.php');

try {

    $current_value = $_GET['view'];
    if ($_GET['view'] == "session_create_success") {
        $_GET['view'] = "";
        $current_value = "";
    }

    $smarty->assign('current_value', $current_value);

    switch ($_GET['view']) {
        case '':
            $sessions = [];
            // get conducting sessions 
            /*           $get_sessions = $db->query(sprintf("SELECT s.*, (SELECT COUNT(*) FROM `sessions_comment` WHERE `sessions_id` = s.sessions_id) AS total_comments FROM sessions AS s WHERE s.`status` = %s OR s.`status` = %s", secure(3), secure(4))) or _error(SQL_ERROR_THROWEN);
              if ($get_sessions->num_rows == 0) {
              _error(404);
              }
              while ($row = $get_sessions->fetch_assoc()) {
              $row['created_by'] = $user->get_user_by_id($row['created_by']);
              $row['created_by'] = $row['created_by'][0];

              $presentors = '';
              if ($row['presentors'] != '') {
              $row['presentors'] = $user->get_user_by_id($row['presentors']);
              if (isset($row['presentors']) && count($row['presentors']) > 0) {
              foreach ($row['presentors'] as $pv) {
              $presentors.=$pv['user_name'] . ",";
              }
              }
              $presentors = rtrim($presentors, ",");
              }
              $row['presentors'] = $presentors;

              $sessions['conductingData'][] = $row;
              }

              // get requested sessions
              $get_sessions = $db->query(sprintf("SELECT * FROM sessions WHERE `status` = %s", secure(2))) or _error(SQL_ERROR_THROWEN);
              if ($get_sessions->num_rows == 0) {
              _error(404);
              }
              while ($row = $get_sessions->fetch_assoc()) {
              $sessions['requestingData'][] = $row;
              } */
//	$sessions = $get_sessions->fetch_assoc();
            //get Conducting a session and Requesting For a Session list from DB and store in two array

            $sessions = [];
            // get conducting sessions 
            $get_sessions = $db->query(sprintf("SELECT * FROM sessions ORDER BY `event_date` ASC ")) or _error(SQL_ERROR_THROWEN);
            if ($get_sessions->num_rows == 0) {
                _error(404);
            }
            while ($row = $get_sessions->fetch_assoc()) {
                $row['created_by'] = $user->get_user_by_id($row['created_by']);
                $row['created_by'] = $row['created_by'][0];

                $presentors = '';
                if ($row['presentors'] != '') {
                    $row['presentors'] = $user->get_user_by_id($row['presentors']);
                    if (isset($row['presentors']) && count($row['presentors']) > 0) {
                        foreach ($row['presentors'] as $pv) {
//                            $presentors.=$pv['user_name'] . ",";
                            $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                        }
                    }
                    $presentors = rtrim($presentors, ",");
                }
                $row['presentors'] = $presentors;

                $row['user_login'] = 0;
                if ($user->_logged_in) {
                    $row['user_login'] = 1;
                }

                /* get total friends attendes */
                $row['session_attend_friends'] = "";

                $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", secure(1, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $my_friends_ids = $my_friends->fetch_assoc();

                if ($row['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
                    $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ", secure($row['sessions_id'], 'int'), $my_friends_ids['my_friends'])) or _error(SQL_ERROR_THROWEN);
                    if ($sessions_attends->num_rows > 0) {
                        while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                            $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            $friends_data = $friends->fetch_assoc();

                            $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                            $row['session_attend_friends'] .=
                                '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                    <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                </a>
                            </span>';
                        }
                    }
                }

                /* get total friends attendes */
                $row['session_engaged_peoples'] = "";
                $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", secure(1, 'int'), secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $user_ids = $my_friends->fetch_assoc();

                $final_user_ids = "";
                if ($user_ids['users'] != "") {
                    $final_user_ids = $user_ids['users'] . ",";
                }

                $sessions_attends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_joins` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $sessions_user_ids = $sessions_attends->fetch_assoc();

                if ($sessions_user_ids['users'] != "") {
                    $final_user_ids .= $sessions_user_ids['users'];
                }

                if ($final_user_ids != "") {

                    $final_user_ids = rtrim($final_user_ids, ",");

                    $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids)) or _error(SQL_ERROR_THROWEN);

                    while ($friends_data = $friends->fetch_assoc()) {
                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                        $row['session_engaged_peoples'] .=
                            '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                            </a>
                                        </span>';
                    }
                }

                /* get People attendes */
                $row['session_attend_peoples'] = "";
                $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($sessions_attends->num_rows > 0) {
                    while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                        $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $friends_data = $friends->fetch_assoc();

                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                        $row['session_attend_peoples'] .=
                            '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                            </a>
                        </span>';
                    }
                }

                if (strtotime($row['event_date'] . " " . $row['event_time']) == '' || strtotime($row['event_date'] . " " . $row['event_time']) >= time()) {
                    // event date not given or its upcoming one.
                    $sessions[2][] = $row;
                } else {
                    $sessions[$row['status']][] = $row;
                }
            }

            $sessions['page_title'] = 'Sessions';
            /* assign variables */
            $smarty->assign('sessions', $sessions);
            break;

        case 'single_session':

            // [1] get main session info
            $get_session = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4') ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_session->num_rows == 0) {
//                _error(404);
            }
            $session_data = $get_session->fetch_assoc();

            $event_date = $session_data['event_date'] . " " . $session_data['event_time'];
            $endtime = strtotime("+" . $session_data['event_duration'] . " minutes", strtotime($event_date));
            $event_expired = FALSE;


            if (intval($endtime) > 0 && time() >= $endtime) {
//                echo "event expired";
                $event_expired = TRUE;
                $db->query(sprintf("UPDATE sessions SET `status` = '4' WHERE sessions_id = %s", secure($_GET['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                $get_noti = $db->query(sprintf("SELECT * FROM `notifications` WHERE `action` = 'entered_in_organized_initiator' AND `node_url` = %s AND to_user_id = %s  ", secure($_GET['sessions_id']), secure($session_data['created_by']))) or _error(SQL_ERROR_THROWEN);
                if ($get_noti->num_rows == 0) {
                    $user->post_notification($session_data['created_by'], 'entered_in_organized_initiator', 'session', $_GET['sessions_id']);
                }


                if ($session_data['presentors'] != '') {
                    $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                    if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                        foreach ($session_data['presentors'] as $pv) {
                            $get_noti = $db->query(sprintf("SELECT * FROM `notifications` WHERE (`action` = 'entered_in_organized_initiator' OR `action` = 'entered_in_organized_thanks') AND `node_url` = %s AND to_user_id = %s ", secure($_GET['sessions_id']), secure($pv['user_id']))) or _error(SQL_ERROR_THROWEN);
                            if ($get_noti->num_rows == 0) {
                                $user->post_notification($pv['user_id'], 'entered_in_organized_thanks', 'session', $_GET['sessions_id']);
                            }
                        }
                    }
                }

                $get_attendies = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
                while ($attendies_data = $get_attendies->fetch_assoc()) {
                    $get_noti = $db->query(sprintf("SELECT * FROM `notifications` WHERE (`action` = 'entered_in_organized_initiator' OR `action` = 'entered_in_organized_thanks' OR `action` = 'entered_in_organized') AND `node_url` = %s AND to_user_id = %s ", secure($_GET['sessions_id']), secure($attendies_data['user_id']))) or _error(SQL_ERROR_THROWEN);
                    if ($get_noti->num_rows == 0) {
                        $user->post_notification($attendies_data['user_id'], 'entered_in_organized', 'session', $_GET['sessions_id']);
                    }
                }
            }
            $session_data['event_expired'] = $event_expired;

            // page header
            page_header($system['system_title'] . ' - ' . $session_data['title']);

            $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
            $session_data['created_by'] = $session_data['created_by'][0];

            $presentors = '';
            $addEditVenue = '';
            if ($session_data['presentors'] != '' && !$event_expired) {
                $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                    foreach ($session_data['presentors'] as $pv) {
                        if ($pv['user_id'] == $user->_data['user_id']) {
                            $addEditVenue .= '<li>
                                        <div class="about-list-item">
                                            <i class="fa fa-pencil fa-fw fa-lg"></i>
                                            <a href="" class="nav" style="width:150px;" data-toggle="modal" data-url="session/accept_editor.php?sessions_id=' . $_GET['sessions_id'] . '&action=edit_info">Update Venue & Date</a>
                                        </div>
                                    </li>';
                        }
                    }
                }
            }

            if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                foreach ($session_data['presentors'] as $pv) {
                    $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                }
            }
            $presentors = rtrim($presentors, ",");
            $session_data['addEditVenue'] = $addEditVenue;
            $session_data['presentors'] = $presentors;

            /* get page picture */
            $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
            $session_data['page_picture'] = User::get_picture($session_data['cover_photo'], 'page');

            /* get total attendes */
            $session_data['total_attend'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $cnt = $get_likes->fetch_assoc();
                    $session_data['total_attend'] = $cnt['cnt'];
                }
            }
            $session_data['venue'] = $user->get_one_venue($session_data['venue']);
            $session_data['venue'] = "<a href='" . $system['system_url'] . "/venue/" . $session_data['venue']['venue_id'] . "'>" . $session_data['venue']['venue_name'] . "</a>";

            /* get total friends attendes */
            $session_data['session_attend_friends'] = "";
            if ($user->_logged_in) {
                $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", secure(1, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $my_friends_ids = $my_friends->fetch_assoc();

                if ($session_data['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
                    $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ", secure($session_data['sessions_id'], 'int'), $my_friends_ids['my_friends'])) or _error(SQL_ERROR_THROWEN);
                    if ($sessions_attends->num_rows > 0) {
                        while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                            $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                            $friends_data = $friends->fetch_assoc();

                            $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                            $session_data['session_attend_friends'] .=
                                '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                    <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                </a>
                            </span>';
                        }
                    }
                }
            }

            /* get total friends attendes */
            $session_data['session_engaged_peoples'] = "";
            if ($user->_logged_in) {
                $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", secure(1, 'int'), secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $user_ids = $my_friends->fetch_assoc();

                $final_user_ids = "";
                if ($user_ids['users'] != "") {
                    $final_user_ids = $user_ids['users'] . ",";
                }

                $sessions_attends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_joins` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $sessions_user_ids = $sessions_attends->fetch_assoc();

                if ($sessions_user_ids['users'] != "") {
                    $final_user_ids .= $sessions_user_ids['users'];
                }

                if ($final_user_ids != "") {
                    $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids)) or _error(SQL_ERROR_THROWEN);

                    while ($friends_data = $friends->fetch_assoc()) {
                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                        $session_data['session_engaged_peoples'] .=
                            '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                            </a>
                                        </span>';
                    }
                }
            }

            /* get People attendes */
            $session_data['session_attend_peoples'] = "";
            if ($user->_logged_in) {
                $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($sessions_attends->num_rows > 0) {
                    while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                        $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                        $friends_data = $friends->fetch_assoc();

                        $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                        $session_data['session_attend_peoples'] .=
                            '<span class="name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                            <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover_removed" data-uid="' . $friends_data['user_id'] . '">
                                <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                            </a>
                        </span>';
                    }
                }
            }

            /* get the connection */
            $session_data['i_attend'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_attend'] = true;
                }
            }
            $session_data['i_join'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_join'] = true;
                }
            }
            /* get the connection */
            $session_data['i_accept'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_accept'] = true;
                }
            }

            $presenter_data = $get_likes->fetch_assoc();
            $presenter_data['questions'] = html_entity_decode($presenter_data['questions']);
            $session_data['presenter_data'] = json_decode($presenter_data['questions']);

            /* get the thank you button */
            $session_data['i_thanks'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_thanks'] = 1;
                }
            }

            /* prepare publisher */
            $smarty->assign('market_categories', $user->get_market_categories());
            $smarty->assign('feelings', get_feelings());
            $smarty->assign('feelings_types', get_feelings_types());

            /* get posts */
            $posts = $user->get_posts(array('get' => 'posts_page', 'id' => $session_data['sessions_id']));
            /* assign variables */
            $smarty->assign('posts', $posts);
            // page header
            page_header($session_data['title']);

            /* GET COUNT OF ALL */
            $session_data['photos_cnt'] = $user->get_session_photos_cnt($_GET['sessions_id']);
            $session_data['discussions_cnt'] = $user->get_session_discussions_cnt($_GET['sessions_id']);
            $session_data['comments_cnt'] = $user->get_session_comments_cnt($_GET['sessions_id']);
            $session_data['qaData_cnt'] = $user->get_session_qa_cnt($_GET['sessions_id']);

            // assign variables
            $smarty->assign('session_data', $session_data);
            $smarty->assign('view', $_GET['view']);

            $i = 0;
            $sessionInterestArr = array();
            $get_seession_interest = $db->query(sprintf("SELECT *, ( SELECT text FROM `interest_mst` WHERE interest_id = si.interest ) AS session_name FROM `sessions_interest` AS si WHERE si.`sessions_id` = %s", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            while ($session_interest = $get_seession_interest->fetch_assoc()) {
                $sessionInterestArr[$i]['interest_id'] = $session_interest['interest'];
                $sessionInterestArr[$i]['session_name'] = $session_interest['session_name'];

                $i++;
            }

            $smarty->assign('sessionInterestArr', $sessionInterestArr);

            // page footer
            page_footer("single_session");
            exit();
            break;
        case 'photos':
            /* get photos */
            // [1] get main session info
            $get_session = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4') ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_session->num_rows == 0) {
//                _error(404);
            }
            $session_data = $get_session->fetch_assoc();
            // page header
            page_header($system['system_title'] . ' - ' . $session_data['title']);

            $event_date = $session_data['event_date'] . " " . $session_data['event_time'];
            $endtime = strtotime("+" . $session_data['event_duration'] . " minutes", strtotime($event_date));
            $event_expired = FALSE;
            if (intval($endtime) > 0 && time() >= $endtime) {
//                echo "event expired";
                $event_expired = TRUE;
                $db->query(sprintf("UPDATE sessions SET `status` = '4' WHERE sessions_id = %s", secure($_GET['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                $get_noti = $db->query(sprintf("SELECT * FROM `notifications` WHERE `action` = 'entered_in_organized' AND `node_url` = %s ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
                if ($get_noti->num_rows == 0) {
                    $get_attendies = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
                    while ($attendies_data = $get_attendies->fetch_assoc()) {
                        $user->post_notification($attendies_data['user_id'], 'entered_in_organized', 'session', $_GET['sessions_id']);
                    }
                }
            }
            $session_data['event_expired'] = $event_expired;

            /* get page picture */
            $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
            $session_data['page_picture'] = User::get_picture($session_data['cover_photo'], 'page');

            /* get the connection */
            $session_data['i_attend'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_attend'] = true;
                }
            }
            $session_data['i_join'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_join'] = true;
                }
            }
            /* get the connection */
            $session_data['i_accept'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_accept'] = true;
                }
            }

            /* get the thank you button */
            $session_data['i_thanks'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_thanks'] = 1;
                }
            }

            /* get total attendes */
            $session_data['total_attend'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $cnt = $get_likes->fetch_assoc();
                    $session_data['total_attend'] = $cnt['cnt'];
                }
            }
            $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
            $session_data['created_by'] = $session_data['created_by'][0];

            $session_data['photos'] = $user->get_session_photos($_GET['sessions_id']);

            $presentors = '';
            if ($session_data['presentors'] != '') {
                $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                    foreach ($session_data['presentors'] as $pv) {
                        $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                    }
                }
                $presentors = rtrim($presentors, ",");
            }
            $session_data['presentors'] = $presentors;

            /* GET COUNT OF ALL */
            $session_data['photos_cnt'] = $user->get_session_photos_cnt($_GET['sessions_id']);
            $session_data['discussions_cnt'] = $user->get_session_discussions_cnt($_GET['sessions_id']);
            $session_data['comments_cnt'] = $user->get_session_comments_cnt($_GET['sessions_id']);
            $session_data['qaData_cnt'] = $user->get_session_qa_cnt($_GET['sessions_id']);

            // assign variables
            $smarty->assign('session_data', $session_data);
            $smarty->assign('view', $_GET['view']);

            // page footer
            page_footer("single_session");
            exit();
            break;

        case 'discussion':
            /* get photos */
            // [1] get main session info
            $get_session = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` IN ('2', '3', '4') ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_session->num_rows == 0) {
//                _error(404);
            }
            $session_data = $get_session->fetch_assoc();
            // page header
            page_header($system['system_title'] . ' - ' . $session_data['title']);

            $event_date = $session_data['event_date'] . " " . $session_data['event_time'];
            $endtime = strtotime("+" . $session_data['event_duration'] . " minutes", strtotime($event_date));
            $event_expired = FALSE;
            if (intval($endtime) > 0 && time() >= $endtime) {
//                echo "event expired";
                $event_expired = TRUE;
                $db->query(sprintf("UPDATE sessions SET `status` = '4' WHERE sessions_id = %s", secure($_GET['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);

                $get_noti = $db->query(sprintf("SELECT * FROM `notifications` WHERE `action` = 'entered_in_organized' AND `node_url` = %s ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
                if ($get_noti->num_rows == 0) {
                    $get_attendies = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
                    while ($attendies_data = $get_attendies->fetch_assoc()) {
                        $user->post_notification($attendies_data['user_id'], 'entered_in_organized', 'session', $_GET['sessions_id']);
                    }
                }
            }
            $session_data['event_expired'] = $event_expired;

            /* get page picture */
            $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
            $session_data['page_picture'] = User::get_picture($session_data['cover_photo'], 'page');

            $presentors = '';
            if ($session_data['presentors'] != '') {
                $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                    foreach ($session_data['presentors'] as $pv) {
                        $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                    }
                }
                $presentors = rtrim($presentors, ",");
            }
            $session_data['presentors'] = $presentors;

            /* get the connection */
            $session_data['i_attend'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_attend'] = true;
                }
            }
            $session_data['i_join'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_join'] = true;
                }
            }
            /* get the connection */
            $session_data['i_accept'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_accept'] = true;
                }
            }

            /* get the thank you button */
            $session_data['i_thanks'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_thanks'] = 1;
                }
            }

            /* get total attendes */
            $session_data['total_attend'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $cnt = $get_likes->fetch_assoc();
                    $session_data['total_attend'] = $cnt['cnt'];
                }
            }
            $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
            $session_data['created_by'] = $session_data['created_by'][0];

            $session_data['discussions'] = $user->get_session_discussions($_GET['sessions_id']);

            /* GET COUNT OF ALL */
            $session_data['photos_cnt'] = $user->get_session_photos_cnt($_GET['sessions_id']);
            $session_data['discussions_cnt'] = $user->get_session_discussions_cnt($_GET['sessions_id']);
            $session_data['comments_cnt'] = $user->get_session_comments_cnt($_GET['sessions_id']);
            $session_data['qaData_cnt'] = $user->get_session_qa_cnt($_GET['sessions_id']);

            // assign variables
            $smarty->assign('session_data', $session_data);
            $smarty->assign('view', $_GET['view']);

            // page footer
            page_footer("single_session");
            exit();
            break;

        case 'comments':
            /* get photos */
            // [1] get main session info
            $get_session = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` = ( '4') ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_session->num_rows == 0) {
//                _error(404);
            }
            $session_data = $get_session->fetch_assoc();
            // page header
            page_header($system['system_title'] . ' - ' . $session_data['title']);

            $event_date = $session_data['event_date'] . " " . $session_data['event_time'];
            $endtime = strtotime("+" . $session_data['event_duration'] . " minutes", strtotime($event_date));
            $event_expired = FALSE;
            if (time() >= $endtime) {
                $event_expired = TRUE;
            }
            $session_data['event_expired'] = $event_expired;

            /* get page picture */
            $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
            $session_data['page_picture'] = User::get_picture($session_data['cover_photo'], 'page');


            $presentors = '';
            if ($session_data['presentors'] != '') {
                $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                    foreach ($session_data['presentors'] as $pv) {
                        $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                    }
                }
                $presentors = rtrim($presentors, ",");
            }
            $session_data['presentors'] = $presentors;


            /* get the connection */
            $session_data['i_attend'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_attend'] = true;
                }
            }
            $session_data['i_join'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_join'] = true;
                }
            }
            /* get the connection */
            $session_data['i_accept'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_accept'] = true;
                }
            }

            /* get the thank you button */
            $session_data['i_thanks'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_thanks'] = 1;
                }
            }
            /* get total attendes */
            $session_data['total_attend'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $cnt = $get_likes->fetch_assoc();
                    $session_data['total_attend'] = $cnt['cnt'];
                }
            }
            $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
            $session_data['created_by'] = $session_data['created_by'][0];

            $session_data['comments'] = $user->get_session_comments($_GET['sessions_id']);

            /* GET COUNT OF ALL */
            $session_data['photos_cnt'] = $user->get_session_photos_cnt($_GET['sessions_id']);
            $session_data['discussions_cnt'] = $user->get_session_discussions_cnt($_GET['sessions_id']);
            $session_data['comments_cnt'] = $user->get_session_comments_cnt($_GET['sessions_id']);
            $session_data['qaData_cnt'] = $user->get_session_qa_cnt($_GET['sessions_id']);

            // assign variables
            $smarty->assign('session_data', $session_data);
            $smarty->assign('view', $_GET['view']);

            // page footer
            page_footer("single_session");
            exit();
            break;

        case 'queans':
            /* get photos */
            // [1] get main session info
            $get_session = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s AND `status` = ( '4') ", secure($_GET['sessions_id']))) or _error(SQL_ERROR_THROWEN);
            if ($get_session->num_rows == 0) {
//                _error(404);
            }
            $session_data = $get_session->fetch_assoc();
            // page header
            page_header($system['system_title'] . ' - ' . $session_data['title']);

            $event_date = $session_data['event_date'] . " " . $session_data['event_time'];
            $endtime = strtotime("+" . $session_data['event_duration'] . " minutes", strtotime($event_date));
            $event_expired = FALSE;
            if (time() >= $endtime) {
                $event_expired = TRUE;
            }
            $session_data['event_expired'] = $event_expired;

            /* get page picture */
            $session_data['page_picture_default'] = ($session_data['cover_photo']) ? false : true;
            $session_data['page_picture'] = User::get_picture($session_data['cover_photo'], 'page');

            /* get the connection */
            $session_data['i_attend'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_attend'] = true;
                }
            }
            $session_data['i_join'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_join'] = true;
                }
            }
            /* get the connection */
            $session_data['i_accept'] = false;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_accept'] = true;
                }
            }

            /* get the thank you button */
            $session_data['i_thanks'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT * FROM `sessions_thanks` WHERE session_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $session_data['i_thanks'] = 1;
                }
            }

            /* get total attendes */
            $session_data['total_attend'] = 0;
            if ($user->_logged_in) {
                $get_likes = $db->query(sprintf("SELECT count(*) AS cnt FROM `sessions_attends` WHERE sessions_id = %s ", secure($session_data['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                if ($get_likes->num_rows > 0) {
                    $cnt = $get_likes->fetch_assoc();
                    $session_data['total_attend'] = $cnt['cnt'];
                }
            }
            $session_data['created_by'] = $user->get_user_by_id($session_data['created_by']);
            $session_data['created_by'] = $session_data['created_by'][0];


            $presentors = '';
            if ($session_data['presentors'] != '') {
                $session_data['presentors'] = $user->get_user_by_id($session_data['presentors']);
                if (isset($session_data['presentors']) && count($session_data['presentors']) > 0) {
                    foreach ($session_data['presentors'] as $pv) {
                        $presentors .= '<a href="' . $system['system_url'] . '/' . $pv['user_name'] . '">' . $pv['user_name'] . "</a>,";
                    }
                }
                $presentors = rtrim($presentors, ",");
            }
            $session_data['presentors'] = $presentors;

            /* GET COUNT OF ALL */
            $session_data['photos_cnt'] = $user->get_session_photos_cnt($_GET['sessions_id']);
            $session_data['discussions_cnt'] = $user->get_session_discussions_cnt($_GET['sessions_id']);
            $session_data['comments_cnt'] = $user->get_session_comments_cnt($_GET['sessions_id']);
            $session_data['qaData_cnt'] = $user->get_session_qa_cnt($_GET['sessions_id']);

            $session_data['qaData'] = $user->get_session_qa($_GET['sessions_id']);
            // assign variables
            $smarty->assign('session_data', $session_data);
            $smarty->assign('view', $_GET['view']);

            // page footer
            page_footer("single_session");
            exit();
            break;

        default:
            _error(404);
            break;
    }
} catch (Exception $e) {
    _error(__("Error"), $e->getMessage());
}

// page header
page_header($system['system_title'] . ' - ' . $sessions['page_title']);

// page footer
page_footer("sessions");
?>