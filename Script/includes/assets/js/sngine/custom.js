$(document).ready(function () {

    loadGallery(true, 'a.thumbnail');
    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
        $('#show-previous-image, #show-next-image').show();
        if (counter_max == counter_current) {
            $('#show-next-image').hide();
        } else if (counter_current == 1) {
            $('#show-previous-image').hide();
        }
    }

    function loadGallery(setIDs, setClickAttr) {
        var current_image,
                selector,
                counter = 0;
        $('#show-next-image, #show-previous-image').click(function () {
            if ($(this).attr('id') == 'show-previous-image') {
                current_image--;
            } else {
                current_image++;
            }

            selector = $('[data-image-id="' + current_image + '"]');
            updateGallery(selector);
        });
        function updateGallery(selector) {
            var $sel = selector;
            current_image = $sel.data('image-id');
            $('#image-gallery-caption').text($sel.data('caption'));
            $('#image-gallery-title').text($sel.data('title'));
            $('#image-gallery-image').attr('src', $sel.data('image'));
            disableButtons(counter, $sel.data('image-id'));
        }

        if (setIDs == true) {
            $('[data-image-id]').each(function () {
                counter++;
                $(this).attr('data-image-id', counter);
            });
        }
        $(setClickAttr).on('click', function () {
            updateGallery($(this));
        });
    }
    $('#interest_dropdown').multiselect({
        enableClickableOptGroups: true,
//        enableCollapsibleOptGroups: true,
        enableFiltering: true,
        buttonWidth: '100%',
        dropRight: true,
        numberDisplayed: 5,
        includeSelectAllOption: true,
        maxHeight: 200
    });
    $("#queans_list").find('.question_div').click(function () {
        if (false == $(this).siblings('.answers').is(':visible')) {
            $(this).siblings('.answers').slideUp(300);
        }
        $(this).siblings('.answers').slideToggle(300);
    });
});

var tree2 = $("#test-select-2").treeMultiselect({enableSelectAll: true, searchable: true, startCollapsed: true});
var tree1 = $("#test-select-1").treeMultiselect({enableSelectAll: true, searchable: true, startCollapsed: true});

api['session/all_guest_sessions'] = ajax_path + "session/all_guest_sessions.php";

function sessionSortByTimeGuestUsers(current_value) {
    $.post(api['session/all_guest_sessions'], {'current_value': current_value}, function (response) {
        $("#session_tabs_div").html("").html(response.template);
        var div_id = $(".session_li.active > a").attr('href');
        $(div_id).addClass('active');
    }, "json")
    .fail(function () {
        modal('#modal-message', {title: __['Error'], message: __['There is something that went wrong!']});
    });
}