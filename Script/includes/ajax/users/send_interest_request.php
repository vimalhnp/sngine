<?php
/**
 * ajax -> users -> verify
 * 
 * @package Sngine v2+
 * @author Zamblek
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
	modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// verify
try {
    
    $db->query(sprintf("INSERT INTO `interest_request_mst` (created_by, interest, parent_interest, added_on, modified_on) VALUES (%s, %s, %s, %s, %s)", secure($user->_data['user_id'], 'int'), secure($_POST['interest_name']), secure($_POST['parent_interest_name']), secure($date), secure($date) )) or _error(SQL_ERROR_THROWEN);
    
} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}

?>