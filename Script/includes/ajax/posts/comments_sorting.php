<?php

/**
 * ajax -> sessions -> product editor
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if ($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can accept any session, you need to confirm your email address"));
}

// valid inputs
if (!isset($_REQUEST['current_value']) || !isset($_REQUEST['session_id'])) {
    _error(400);
}

// product editor
try {

    // initialize the return array
    $return = array();

    $session_data['sessions_id'] = $_REQUEST['current_value'];
    
    /* get the connection */
    $session_data['i_attend'] = false;
    if ($user->_logged_in) {
        $get_likes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_likes->num_rows > 0) {
            $session_data['i_attend'] = true;
        }
    }
    $session_data['i_join'] = false;
    if ($user->_logged_in) {
        $get_likes = $db->query(sprintf("SELECT * FROM `sessions_joins` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_likes->num_rows > 0) {
            $session_data['i_join'] = true;
        }
    }
    /* get the connection */
    $session_data['i_accept'] = false;
    if ($user->_logged_in) {
        $get_likes = $db->query(sprintf("SELECT * FROM `sessions_presentor` WHERE sessions_id = %s AND user_id = %s", secure($session_data['sessions_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($get_likes->num_rows > 0) {
            $session_data['i_accept'] = true;
        }
    }

    $session_data['qaData'] = $user->get_session_qa_sorting($_REQUEST['session_id'], $_REQUEST['current_value']);
    // assign variables
    $smarty->assign('session_data', $session_data);

    /* return */
    $return['template'] = $smarty->fetch("ajax.comments_sort.tpl");
    
    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>