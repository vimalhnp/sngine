<?php
/**
 * ajax -> posts -> likes
 * 
 * @package Sngine v2+
 * @author Zamblek
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if(!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if($system['activation_enabled'] && !$user->_data['user_activated']) {
	modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// valid inputs
/* if interest_id set but not numeric */
if(isset($_GET['interest_id']) && !is_numeric($_GET['interest_id'])) {
	_error(400);
}


// get likes
try {

	// initialize the return array
	$return = array();

	// get users who
	if(isset($_GET['interest_id'])) {
		/* like this post */
		$users = $user->who_interest_followed( array('interest_id' => $_GET['interest_id']) );
		$get = 'interest_followed';
		$id = $_GET['interest_id'];
	}
	/* assign variables */
	$smarty->assign('users', $users);
	$smarty->assign('get', $get);
	$smarty->assign('id', $id);
	/* return */
	$return['template'] = $smarty->fetch("ajax.who_interest_followed.tpl");
	$return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.template);";

	// return & exit
	return_json($return);

} catch (Exception $e) {
	modal(ERROR, __("Error"), $e->getMessage());
}


?>