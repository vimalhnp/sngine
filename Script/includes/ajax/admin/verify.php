<?php
/**
 * ajax -> admin -> verify
 *
 * @package Sngine v2+
 * @author Zamblek
 */

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();


// check admin logged in
if (!$user->_logged_in || !$user->_is_admin) {
    modal(MESSAGE, __("System Message"), __("You don't have the right permission to access this"));
}


// valid inputs
if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
    _error(400);
}

// verify
try {

    switch ($_POST['handle']) {

        case 'user':
            /* approve request */
            $db->query(sprintf("UPDATE verification_requests SET status = '1' WHERE node_type = 'user' AND node_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update user */
            $db->query(sprintf("UPDATE users SET user_verified = '1' WHERE user_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        case 'page':
            /* approve request */
            $db->query(sprintf("UPDATE verification_requests SET status = '1' WHERE node_type = 'page' AND node_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            /* update page */
            $db->query(sprintf("UPDATE pages SET page_verified = '1' WHERE page_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        case 'decline':
            /* decline request */
            $db->query(sprintf("UPDATE verification_requests SET status = '-1' WHERE request_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        case 'session':

            $session_details = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s ", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
            $sessionDetails = $session_details->fetch_assoc();

            $user->post_notification($sessionDetails['created_by'], 'session_verify_initiator', 'session', $_POST['id']);

            $interested_users = $db->query(sprintf("SELECT ui.user_id FROM user_interest AS ui WHERE ui.interest IN (
                              SELECT si.interest FROM `sessions_interest` AS si WHERE si.sessions_id = %s AND si.`status` = 1 
                                  ) GROUP BY ui.user_id", secure($_POST['id']))) or _error(SQL_ERROR_THROWEN);
            while ($ius = $interested_users->fetch_assoc()) {
                if ($sessionDetails['created_by'] != $ius['user_id']) {
                    $user->post_notification($ius['user_id'], 'session_verify', 'session', $_POST['id']);
                }
            }

            /* decline request */
            $db->query(sprintf("UPDATE sessions SET status = '2' WHERE sessions_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        case
        'session_decline':
            /* decline request */
            $db->query(sprintf("UPDATE sessions SET status = '0' WHERE sessions_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        case 'session_assign':
            /* assign request */
            $db->query(sprintf("UPDATE `sessions_presentor` SET status = '2' WHERE sessions_presentor_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $db->query(sprintf("UPDATE `sessions` SET presentors = CONCAT_WS( ',', IF ( LENGTH(`presentors`), `presentors`, NULL ), %s ), `status`=3 WHERE sessions_id = %s", secure($_POST['sharerid'], 'int'), secure($_POST['sessionid'], 'int'))) or _error(SQL_ERROR_THROWEN);

            $session_details = $db->query(sprintf("SELECT * FROM `sessions` WHERE `sessions_id` = %s ", secure($_POST['sessionid']))) or _error(SQL_ERROR_THROWEN);
            $sessionDetails = $session_details->fetch_assoc();
            $alreadyNotiSent = array();

            $user->post_notification($sessionDetails['created_by'], 'session_assign_initiator', 'session', $_POST['sessionid'] . "," . $_POST['sharerid']);
            $alreadyNotiSent[] = $sessionDetails['created_by'];

            $user->post_notification($_POST['sharerid'], 'session_assign_sharer', 'session', $_POST['sessionid'] . "," . $_POST['sharerid']);
            $alreadyNotiSent[] = $_POST['sharerid'];


            $session_attendes = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE `sessions_id` = %s ", secure($_POST['sessionid']))) or _error(SQL_ERROR_THROWEN);
            while ($sessionDetails = $session_attendes->fetch_assoc()) {
                if (!in_array($sessionDetails['user_id'], $alreadyNotiSent)) {
                    $user->post_notification($sessionDetails['user_id'], 'session_assign_attendees', 'session', $_POST['sessionid'] . "," . $_POST['sharerid']);
                    $alreadyNotiSent[] = $sessionDetails['user_id'];
                }
            }

            //Notifications Not sent to those who have the added the interest when a session of interest is assigned by Admin.
            $sessions_interest = $db->query(sprintf("SELECT `interest` FROM `sessions_interest` WHERE `sessions_id` = %s ", secure($_POST['sessionid']))) or _error(SQL_ERROR_THROWEN);
            $interestsArr = array();
            while ($sessionIntDetails = $sessions_interest->fetch_assoc()) {
                $interestsArr[] = $sessionIntDetails['interest'];
            }
            $interests = implode("','", $interestsArr);
            $interests = "'" . $interests . "'";
            $usersInterest = $db->query(sprintf("SELECT DISTINCT `user_id` FROM `user_interest` WHERE `interest` IN (%s) ", ($interests))) or _error(SQL_ERROR_THROWEN);
            while ($userInterest = $usersInterest->fetch_assoc()) {
                if (!in_array($userInterest['user_id'], $alreadyNotiSent)) {
                    $user->post_notification($userInterest['user_id'], 'session_assign_attendees', 'session', $_POST['sessionid'] . "," . $_POST['sharerid']);
                }
            }
            break;

        case 'session_unassign':
            /* unassign request */
            $db->query(sprintf("UPDATE `sessions_presentor` SET status = '0' WHERE sessions_presentor_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            break;

        default:
            _error(400);
            break;
    }

    // return
    return_json();

} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}

?>