<?php

/**
 * ajax -> admin -> verify
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();


// check admin logged in
if (!$user->_logged_in || !$user->_is_admin) {
    modal(MESSAGE, __("System Message"), __("You don't have the right permission to access this"));
}


// valid inputs
if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
    _error(400);
}

// verify
try {

    switch ($_POST['handle']) {

        case 'interest_request_decline':
            /* decline request */
            $db->query(sprintf("UPDATE interest_request_mst SET status = '2' WHERE interest_request_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $user->post_notification($_POST['created_by'], 'interest_declined', 'interest', '');
            break;

        case 'interest_request_approve':
            /* assign request */
            global $user;

            $db->query(sprintf("UPDATE interest_request_mst SET status = '0' WHERE interest_request_id = %s", secure($_POST['id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            $user->post_notification($_POST['created_by'], 'interest_approved', 'interest', '');

            break;

        default:
            _error(400);
            break;
    }

    // return
    return_json();
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>