<?php

/**
 * ajax -> admin -> verify
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check admin logged in
if (!$user->_logged_in || !$user->_is_admin) {
    modal(MESSAGE, __("System Message"), __("You don't have the right permission to access this"));
}

// valid inputs
if (!isset($_POST['venue_name'])) {
    _error(400);
}

// verify
try {
    $teams = '';
    $tempTeamArr = array();
    foreach ($_POST['team_name'] as $key => $value) {
        $tempTeamArr[$value] = $_POST['team_role'][$key];
    }
    $teams = json_encode($tempTeamArr);
    $db->query(sprintf("INSERT INTO `venue` SET venue_name = %s,institute_id=%s, description = %s,courses_offered=%s,teams=%s, added_on = %s, modified_on = %s", secure($_POST['venue_name']), secure($_POST['institute_id']), secure($_POST['description']), secure($_POST['courses_offered']), secure($teams), secure($date, 'datetime'), secure($date, 'datetime'))) or _error(SQL_ERROR_THROWEN);
    return_json(array('callback' => "modal('#modal-success', {title: __['Success'], message: __['Venue Created Successfully.']});"));
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>