<?php

/**
 * ajax -> admin -> verify
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check admin logged in
if (!$user->_logged_in || !$user->_is_admin) {
    modal(MESSAGE, __("System Message"), __("You don't have the right permission to access this"));
}

// valid inputs
if (!isset($_POST['name'])) {
    _error(400);
}

// verify
try {
    
    $get_rows = $db->query("SELECT * FROM `institute` WHERE name = '" . $_POST['name'] . "'") or _error(SQL_ERROR);
    if ($get_rows->num_rows > 0) {
        return_json( array('callback' => "modal('#modal-error', {title: __['Error'], message: __['Institute Already Exist']});") );
    } else {
        $db->query(sprintf("INSERT INTO `institute` SET name = %s, photo = %s, added_on = %s, modified_on = %s", secure($_POST['name']), secure($_POST['photo']), secure($date, 'datetime'), secure($date, 'datetime'))) or _error(SQL_ERROR_THROWEN);
    
        return_json( array('callback' => "modal('#modal-success', {title: __['Success'], message: __['Institute Added Successfully.']});") );
    }
    
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>