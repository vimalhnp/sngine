<?php

// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if ($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// edit
try {

    // initialize the return array
    $return = array();

    switch ($_POST['handle']) {

        case 'session_accept':

            // valid inputs
            /* if id is set & not numeric */
            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            /* if product info not set */
            if (!isset($_POST['title']) || !isset($_POST['ans1']) || !isset($_POST['ans2']) || !isset($_POST['ans3']) || !isset($_POST['ans4'])) {
                _error(400);
            }
            /* check product name */
            if (is_empty($_POST['title'])) {
                return_json(array('error' => true, 'message' => __("Add a title so people know what you are sharing")));
            }
            /* check product price */
            if (is_empty($_POST['ans1'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 1.")));
            }
            if (is_empty($_POST['ans2'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 2.")));
            }
            if (is_empty($_POST['ans3'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 3.")));
            }
            if (is_empty($_POST['ans4'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 4.")));
            }
            if (is_empty($_POST['total_allowed_members'])) {
                return_json(array('error' => true, 'message' => __("Please add Total Allowed Members.")));
            }
            $ques = array($_POST['ans1'], $_POST['ans2'], $_POST['ans3'], $_POST['ans4']);
            // edit product
            $user->edit_session($_POST['id'], $_POST['title'], json_encode($ques, TRUE), $_POST['total_allowed_members']);
            /* return */
            $return['callback'] = 'window.location = "' . $system['system_url'] . '/session/' . $_POST['id'] . '";';
            break;
        case 'session_edit':

            // valid inputs
            /* if id is set & not numeric */
            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }
            /* if product info not set */
            if (!isset($_POST['event_date']) || !isset($_POST['event_time']) || !isset($_POST['venue'])) {
                _error(400);
            }
            /* check product price */
            if (is_empty($_POST['event_date'])) {
                return_json(array('error' => true, 'message' => __("Please add your Event Date")));
            }
            if (is_empty($_POST['event_time'])) {
                return_json(array('error' => true, 'message' => __("Please add your Event Time.")));
            }
            if (is_empty($_POST['event_duration'])) {
                return_json(array('error' => true, 'message' => __("Please add your Event Duration.")));
            }
            if (is_empty($_POST['venue'])) {
                return_json(array('error' => true, 'message' => __("Please add your Venue.")));
            }

            $eventPhotos = '';
            for ($index = 1; $index <= 5; $index++) {
                if (isset($_POST['event_photo' . $index]) && $_POST['event_photo' . $index] != '') {
                    $eventPhotos .= $_POST['event_photo' . $index] . ',';
                }
            }
            $eventPhotos = rtrim($eventPhotos, ",");

            // edit product
            $user->edit_sessions_data($_POST['id'], $_POST['event_date'], $_POST['event_time'], $_POST['venue'], $_POST['event_duration'], $eventPhotos);
            /* return */
            $return['callback'] = 'window.location = "' . $system['system_url'] . '/session/' . $_POST['id'] . '";';
            break;

        case 'session_photos_edit':

            // valid inputs
            /* if id is set & not numeric */
            if (!isset($_POST['id']) || !is_numeric($_POST['id'])) {
                _error(400);
            }

            $eventPhotos = '';
            $eventPhotosEdit = '';
            $edit_ids=[];
            foreach ($_POST['event_photo'] AS $image_id => $value) {
                if (isset($value) && $value != '') {
                    $eventPhotos .= $value . ',';
                }
            }
            foreach ($_POST['edit_event_photo'] AS $image_id => $value) {
                if (isset($value) && $value != '') {
                    $eventPhotosEdit .= $value . ',';
                    $edit_ids[]=$image_id;
                }
            }

            $eventPhotos = rtrim($eventPhotos, ",");
            $eventPhotosEdit = rtrim($eventPhotosEdit, ",");
            // edit product
            $user->edit_sessions_photos($_POST['id'], $eventPhotos, $eventPhotosEdit,$edit_ids);
            /* return */
            $return['callback'] = 'window.location = "' . $system['system_url'] . '/session/' . $_POST['id'] . '/photos";';
            break;

        default:
            _error(400);
            break;
    }

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>