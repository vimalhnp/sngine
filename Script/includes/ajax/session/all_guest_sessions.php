<?php

/**
 * ajax -> sessions -> product editor
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// product editor
try {

    // initialize the return array
    $return = array();

    $order_by = " ORDER BY `event_date` ASC ";
    if ($_REQUEST['current_value'] == "time_desc") {
        $order_by = " ORDER BY `event_date` DESC ";
    }

    $sessions = [];
    // get conducting sessions 
    $get_sessions = $db->query(sprintf("SELECT * FROM sessions $order_by ")) or _error(SQL_ERROR_THROWEN);
    if ($get_sessions->num_rows == 0) {
        _error(404);
    }
    while ($row = $get_sessions->fetch_assoc()) {
        $row['created_by'] = $user->get_user_by_id($row['created_by']);
        $row['created_by'] = $row['created_by'][0];

        $presentors = '';
        if ($row['presentors'] != '') {
            $row['presentors'] = $user->get_user_by_id($row['presentors']);
            if (isset($row['presentors']) && count($row['presentors']) > 0) {
                foreach ($row['presentors'] as $pv) {
                    $presentors.=$pv['user_name'] . ",";
                }
            }
            $presentors = rtrim($presentors, ",");
        }
        $row['presentors'] = $presentors;

        $row['user_login'] = 0;
        if ($user->_logged_in) {
            $row['user_login'] = 1;
        }

        /* get total friends attendes */
        $row['session_attend_friends'] = "";

        $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_two_id) AS my_friends FROM `friends` WHERE status = %s AND user_one_id = %s ", secure(1, 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $my_friends_ids = $my_friends->fetch_assoc();

        if ($row['sessions_id'] != "" && $my_friends_ids['my_friends'] != "") {
            $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s AND user_id IN (%s) ", secure($row['sessions_id'], 'int'), $my_friends_ids['my_friends'])) or _error(SQL_ERROR_THROWEN);
            if ($sessions_attends->num_rows > 0) {
                while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                    $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                    $friends_data = $friends->fetch_assoc();

                    $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                    $row['session_attend_friends'].=
                            '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                        <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                            <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                        </a>
                    </span>';
                }
            }
        }

        /* get total friends attendes */
        $row['session_engaged_peoples'] = "";
        $my_friends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_comment` WHERE status = %s AND sessions_id = %s ", secure(1, 'int'), secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $user_ids = $my_friends->fetch_assoc();

        $final_user_ids = "";
        if ($user_ids['users'] != "") {
            $final_user_ids = $user_ids['users'] . ",";
        }

        $sessions_attends = $db->query(sprintf("SELECT GROUP_CONCAT(user_id) AS users FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        $sessions_user_ids = $sessions_attends->fetch_assoc();

        if ($sessions_user_ids['users'] != "") {
            $final_user_ids.= $sessions_user_ids['users'];
        }

        if ($final_user_ids != "") {

            $final_user_ids = rtrim($final_user_ids, ",");

            $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id IN (%s) ", $final_user_ids)) or _error(SQL_ERROR_THROWEN);

            while ($friends_data = $friends->fetch_assoc()) {
                $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                $row['session_engaged_peoples'].=
                        '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                    <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                                        <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                                    </a>
                                </span>';
            }
        }

        /* get People attendes */
        $row['session_attend_peoples'] = "";
        $sessions_attends = $db->query(sprintf("SELECT * FROM `sessions_attends` WHERE sessions_id = %s ", secure($row['sessions_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
        if ($sessions_attends->num_rows > 0) {
            while ($session_attend_users = $sessions_attends->fetch_assoc()) {

                $friends = $db->query(sprintf("SELECT * FROM `users` WHERE user_id = %s ", secure($session_attend_users['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
                $friends_data = $friends->fetch_assoc();

                $friends_data['user_picture'] = $user->get_picture($friends_data['user_picture'], $friends_data['user_gender']);

                $row['session_attend_peoples'].=
                        '<span class="name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                    <a href="' . $system['system_url'] . '/' . $friends_data['user_name'] . '" class="col-sm-1 name js_user-popover" data-uid="' . $friends_data['user_id'] . '">
                        <img class="data-avatar" src="' . $friends_data['user_picture'] . '" alt="' . $friends_data['user_fullname'] . '">
                    </a>
                </span>';
            }
        }
        if (strtotime($row['event_date'] . " " . $row['event_time']) == '' || strtotime($row['event_date'] . " " . $row['event_time']) >= time()) {
            // event date not given or its upcoming one.
            $sessions[2][] = $row;
        } else {
            $sessions[$row['status']][] = $row;
        }
    }

    /* assign variables */
    $smarty->assign('sessions', $sessions);

    /* return */
    $return['template'] = $smarty->fetch("ajax.sessions_sort.tpl");

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>