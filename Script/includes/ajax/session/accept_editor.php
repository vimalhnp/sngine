<?php

/**
 * ajax -> sessions -> product editor
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if ($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can accept any session, you need to confirm your email address"));
}

// valid inputs
if (!isset($_GET['sessions_id']) || !is_numeric($_GET['sessions_id'])) {
    _error(400);
}

// product editor
try {

    // initialize the return array
    $return = array();
    switch ($_GET['action']) {
        case '':
            // get session
            $session = $user->get_session($_GET['sessions_id']);
            if (!$session) {
                _error(400);
            }
            /* assign variables */
            $smarty->assign('session', $session);

            /* return */
            $return['template'] = $smarty->fetch("ajax.session_editor.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.template);";

            break;
        case 'edit_info':
            // get session
            $session = $user->get_session($_GET['sessions_id']);
            if (!$session) {
                _error(400);
            }
            
            $eventPhotoArr = array();
            if(isset($session['event_photos']) && $session['event_photos'] != "") {
                $photoArr = explode(",", $session['event_photos']);
                
                foreach ($photoArr as $photo_key => $photo_value) {
                    $cnt = $photo_key + 1;
                    $eventPhotoArr[$cnt] = $photo_value;
                }
            }
            
            $venues_data = $user->get_venue();
            /* assign variables */
            $smarty->assign('session', $session);
            $smarty->assign('eventPhotoArr', $eventPhotoArr);
            $smarty->assign('venues_data', $venues_data);

            /* return */
            $return['template'] = $smarty->fetch("ajax.session_venue_editor.tpl");
            $return['callback'] = "$('#modal').modal('show'); $('.modal-content:last').html(response.template);";

            break;
    }

    // return & exit
    return_json($return);
} catch (Exception $e) {
    modal(ERROR, __("Error"), $e->getMessage());
}
?>