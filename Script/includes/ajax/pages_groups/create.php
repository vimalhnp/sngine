<?php

/**
 * ajax -> data -> create|edit <-> page|group
 * 
 * @package Sngine v2+
 * @author Zamblek
 */
// fetch bootstrap
require('../../../bootstrap.php');

// check AJAX Request
is_ajax();

// check user logged in
if (!$user->_logged_in) {
    modal(LOGIN);
}

// check user activated
if ($system['activation_enabled'] && !$user->_data['user_activated']) {
    modal(MESSAGE, __("Not Activated"), __("Before you can interact with other users, you need to confirm your email address"));
}

// valid inputs
$valid['type'] = array('page', 'group', 'session');
if (!in_array($_GET['type'], $valid['type'])) {
    _error(400);
}
$valid['do'] = array('create', 'edit','conduct');
if (!in_array($_GET['do'], $valid['do'])) {
    _error(400);
}

// (create|edit) (page|group)
try {

    // initialize the return array
    $return = array();
    $return['callback'] = 'window.location.replace(response.path);';
    
    // page (create|edit)
    if ($_GET['type'] == "page") {

        // page create
        if ($_GET['do'] == "create") {
            $user->page_create($_POST['category'], $_POST['title'], $_POST['username'], $_POST['description']);
            $return['path'] = $system['system_url'] . '/pages/' . $_POST['username'];

            // page edit
        } elseif ($_GET['do'] == "edit") {
            /* check id */
            if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            /* check if the user is the page admin */
            $check = $db->query(sprintf("SELECT * FROM pages WHERE page_id = %s AND page_admin = %s", secure($_GET['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check->num_rows == 0) {
                _error(403);
            }
            $spage = $check->fetch_assoc();
            /* edit page */
            $user->page_edit($_GET['id'], $_POST['category'], $_POST['title'], $_POST['username'], $spage['page_name'], $_POST['description']);
            $return['path'] = $system['system_url'] . '/pages/' . $_POST['username'];
        }

        // group (create|edit)
    } elseif ($_GET['type'] == "group") {

        // group create
        if ($_GET['do'] == "create") {
            $user->group_create($_POST['title'], $_POST['username'], $_POST['description']);
            $return['path'] = $system['system_url'] . '/groups/' . $_POST['username'];

            // group edit
        } elseif ($_GET['do'] == "edit") {
            /* check id */
            if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
                _error(400);
            }
            /* check if the user is the group admin */
            $check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_GET['id'], 'int'), secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
            if ($check->num_rows == 0) {
                _error(403);
            }
            $group = $check->fetch_assoc();
            /* edit group */
            $user->group_edit($_GET['id'], $_POST['title'], $_POST['username'], $group['group_name'], $_POST['description']);
            $return['path'] = $system['system_url'] . '/groups/' . $_POST['username'];
        }
    } elseif ($_GET['type'] == "session") {
        global $date;
        // group create
        if ($_GET['do'] == "create") {
            $eventPhotos = '';
            for ($index = 1; $index <= 8; $index++) {
                if (isset($_POST['event_photo' . $index]) && $_POST['event_photo' . $index] != '') {
                    $eventPhotos.=$_POST['event_photo' . $index] . ',';
                }
            }
            $eventPhotos = rtrim($eventPhotos, ",");

            $sessionId = $user->session_create($_POST['title'], $_POST['description'], $_POST['cover_photo'], $eventPhotos, $_POST['location'],1);
            $user->create_session_interest($sessionId, $_POST['interest_dropdown']);
//            $return['path'] = $system['system_url'] . '/sessions/session_create_success';
            $return['path'] = $system['system_url'] . '/all_sessions';
//            return_json( array('success' => true, 'message' => "Session Created Successfully") );
        } else if ($_GET['do'] == "conduct") {
            $eventPhotos = '';
            for ($index = 1; $index <= 8; $index++) {
                if (isset($_POST['event_photo' . $index]) && $_POST['event_photo' . $index] != '') {
                    $eventPhotos.=$_POST['event_photo' . $index] . ',';
                }
            }
            $eventPhotos = rtrim($eventPhotos, ",");

            $sessionId = $user->session_create($_POST['title'], $_POST['description'], $_POST['cover_photo'], $eventPhotos, $_POST['location'],5);
            $user->create_session_interest($sessionId, $_POST['interest_dropdown']);

            // adding presentor entry
            if (is_empty($_POST['ans1'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 1.")));
            }
            if (is_empty($_POST['ans2'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 2.")));
            }
            if (is_empty($_POST['ans3'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 3.")));
            }
            if (is_empty($_POST['ans4'])) {
                return_json(array('error' => true, 'message' => __("Please add your Answer of Question 4.")));
            }
            $ques = array($_POST['ans1'], $_POST['ans2'], $_POST['ans3'], $_POST['ans4']);
            $user->edit_session($sessionId, $_POST['title'], json_encode($ques, TRUE));

            $return['path'] = $system['system_url'] . '/all_sessions';
//            header("Location:" . $system['system_url'] . "/all_sessions");
//            return_json( array('success' => true, 'message' => "Session Conducted Successfully") );

            // session edit
        } elseif ($_GET['do'] == "edit") {
            /* check id */
//			if(!isset($_GET['id']) || !is_numeric($_GET['id'])) {
//				_error(400);
//			}
//			/* check if the user is the group admin */
//			$check = $db->query(sprintf("SELECT * FROM groups WHERE group_id = %s AND group_admin = %s", secure($_GET['id'], 'int'), secure($user->_data['user_id'], 'int') )) or _error(SQL_ERROR_THROWEN);
//			if($check->num_rows == 0) {
//				_error(403);
//			}
//			$group = $check->fetch_assoc();
//			/* edit group */
//			$user->group_edit($_GET['id'], $_POST['title'], $_POST['username'], $group['group_name'], $_POST['description']);
//			$return['path'] = $system['system_url'].'/groups/'.$_POST['username'];
        }
    }

    // return & exit
    return_json($return);
} catch (Exception $e) {
    return_json(array('error' => true, 'message' => $e->getMessage()));
}
?>