{if $_parent == "profile"}<li class="col-sm-12 col-md-6"><div class="box-container">{else}<li class="feeds-item">{/if}
    <div class="data-container">
        <a href="{$system['system_url']}/session/{$_session['sessions_id']}">
            <img class="data-avatar" src="{$_session['cover_photo']}" alt="{$_session['title']}">
        </a>
        <div class="data-content">
            <div class="pull-right flip">
                
            </div>
            <div>
                <span class="name">
                    <a href="{$system['system_url']}/session/{$_session['sessions_id']}">{$_session['title']}</a>
                </span>
                <div>{$_session['total_attends']} {__("Attendees")}</div>
            </div>
        </div>
    </div>
{if $_parent == "profile"}</div></li>{else}</li>{/if}