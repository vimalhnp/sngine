
<div class="col-xs-12 offcanvas-mainbar">

    <div class="container">
        <div class="row">
            <div class="col-xs-5 text-readable ptb10">
                <ul class="nav nav-pills">
                    {*<li class="active">
                    <a data-toggle="pill" href="#tab1">Not Verified</a>
                    </li>*}
                    <li class="active">
                        <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
                    </li>
                    <li>
                        <a data-toggle="pill" href="#tab3">Other Sessions</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="tab2" class="tab-pane fade in active">
                        <div class="row">
                            <div id="" class="col-md-12">
                                <div class="panel-group" id="accordion">
                                    {foreach from=$sessions[2] item=conductingVal}
                                        <div class="panel panel-default" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                    <h4 class="panel-title">
                                                        {$conductingVal['title']}
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                    <div class="clearfix"></div>
                                                    <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">
                                                    <p>{$conductingVal['description']}</p>

                                                    {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                                    {foreach from=$event_photos item=ep}
                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                    <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                    </a>
                                                    </div>
                                                    {/foreach}*}

                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    Post by : {$conductingVal['created_by']['user_name']}
                                                    {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                    <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab3" class="tab-pane fade in">
                        <div class="row">
                            <div id="" class="col-md-12">
                                <div class="panel-group" id="accordion">
                                    {foreach from=$sessions[3] item=conductingVal}
                                        <div class="panel panel-default" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                    <h4 class="panel-title">
                                                        {$conductingVal['title']}
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                    <div class="clearfix"></div>
                                                    <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">
                                                    <p>{$conductingVal['description']}</p>

                                                    {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                                    {foreach from=$event_photos item=ep}
                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                    <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                    </a>
                                                    </div>
                                                    {/foreach}*}

                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    Post by : {$conductingVal['created_by']['user_name']}
                                                    {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                    <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}


                                    {foreach from=$sessions[4] item=conductingVal}
                                        <div class="panel panel-default" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                    <h4 class="panel-title">
                                                        {$conductingVal['title']}
                                                    </h4>
                                                </a>
                                            </div>
                                            <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                    <div class="clearfix"></div>
                                                    <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">
                                                    <p>{$conductingVal['description']}</p>

                                                    {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                                    {foreach from=$event_photos item=ep}
                                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                    <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                    </a>
                                                    </div>
                                                    {/foreach}*}

                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    Post by : {$conductingVal['created_by']['user_name']}
                                                    {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                    <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>