<div id="tab2" class="tab-pane fade in active">
    <ul>
        {foreach from=$sessions[2] item=conductingVal}
            <li class="feeds-item ptb10 plr10">
                <h4 class="mt0 session_name">
                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                </h4>
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Initiated By : </label>
                    {$conductingVal['created_by']['user_name']}
                </div>
                {if $conductingVal['presentors']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Sharer : </label>
                        {$conductingVal['presentors']}
                    </div>
                {/if}
                {if $conductingVal['total_attend']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Attendees : </label>
                        {$conductingVal['total_attend']}
                    </div>
                {/if}
                {if $conductingVal['total_shares']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Share(s) : </label>
                        {$conductingVal['total_shares']}
                    </div>
                {/if}
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Started At : </label>
                    {$conductingVal['added_on']|date_format:"%e %B %Y"}
                </div>
                <div style="clear: both;"></div>
                <h5 class="session_about">
                    <p class="text-muted">{$conductingVal['description']} </p>
                </h5>

            </li>
        {/foreach}
    </ul>
</div>
<div id="tab3" class="tab-pane fade in">
    <ul>
        {foreach from=$sessions[3] item=conductingVal}
            <li class="feeds-item ptb10 plr10">
                <h4 class="mt0 session_name">
                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                </h4>
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Initiated By : </label>
                    {$conductingVal['created_by']['user_name']}
                </div>
                {if $conductingVal['presentors']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Sharer : </label>
                        {$conductingVal['presentors']}
                    </div>
                {/if}
                {if $conductingVal['total_attend']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Attendees : </label>
                        {$conductingVal['total_attend']}
                    </div>
                {/if}
                {if $conductingVal['total_shares']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Share(s) : </label>
                        {$conductingVal['total_shares']}
                    </div>
                {/if}
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Started At : </label>
                    {$conductingVal['added_on']|date_format:"%e %B %Y"}
                </div>
                <div style="clear: both;"></div>
                <h5 class="session_about">
                    <p class="text-muted">{$conductingVal['description']} </p>
                </h5>
            </li>
        {/foreach}
    </ul>
</div>
<div id="tab4" class="tab-pane fade in">
    <ul>
        {foreach from=$sessions[4] item=conductingVal}
            <li class="feeds-item ptb10 plr10">
                <h4 class="mt0 session_name">
                    <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                </h4>
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Initiated By : </label>
                    {$conductingVal['created_by']['user_name']}
                </div>
                {if $conductingVal['presentors']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Sharer : </label>
                        {$conductingVal['presentors']}
                    </div>
                {/if}
                {if $conductingVal['total_attend']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Attendees : </label>
                        {$conductingVal['total_attend']}
                    </div>
                {/if}
                {if $conductingVal['total_shares']!=''}
                    <div style="width: 50%; float: left;">
                        <label>Total Share(s) : </label>
                        {$conductingVal['total_shares']}
                    </div>
                {/if}
                <div class="text-muted" style="width: 50%; float: left;">
                    <label>Started At : </label>
                    {$conductingVal['added_on']|date_format:"%e %B %Y"}
                </div>
                <div style="clear: both;"></div>
                <h5 class="session_about">
                    <p class="text-muted">{$conductingVal['description']} </p>
                </h5>
            </li>
        {/foreach}
    </ul>
</div>