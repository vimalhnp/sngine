{foreach $session_data['qaData'] as $key=>$qas}
    <li data-id="{$qas['sessions_qa_id']}" data-userid="{$qas['user_id']}">
        <div class="conversation clearfix question_div {if $qas['user_id'] == $user->_data['user_id']}{*right*}{/if}" id="{$qas['sessions_qa_id']}">
            <div class="conversation-user">
                <a href="{$system['system_url']}/{$qas['user_data']['user_name']}">
                    <img src="{$qas['user_data']['user_picture']}" title="{$qas['user_data']['user_fullname']}" alt="{$qas['user_data']['user_fullname']}">
                </a>
            </div>
            <div class="conversation-body">
                <div class="text">
                    <p><a href="{$system['system_url']}/{$qas['user_data']['user_name']}">{$qas['user_data']['user_name']}</a></p>
                        {$qas['post']}
                </div>
                <div class="time js_moment" data-time="{$qas['added_on']}">
                    {$qas['added_on']}
                </div>
            </div>
        </div>
        <div class="clearfix row" style="margin:25px 0 5px 40px">
            <span class="text-link js_open_answer_box">Answer</span> ({$qas['answers']|count}) | 
            {if $qas['i_like']}
                <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
            {else}
                <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
            {/if}
            (<span class="js_qacomment-likes-num_{$qas['sessions_qa_id']}">{$qas['total_likes']}</span>) |
            <span class="text-link publish_session">Share</span>

            <form class="js_ajax_add_qa_answer form-horizontal" method="post">
                <input type="hidden" name="id" value="{$qas['sessions_qa_id']}">
                <input type="hidden" name="posted_user_id" value="{$qas['user_id']}">
                <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                <input type="hidden" name="edit" value="add_qa_answer">
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Answer above Question
                    </label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="comment_post" type="text" required="" {if !$session_data['i_join']}disabled{/if}></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>SUBMIT</button>
                    </div>
                </div>
            </form>
        </div>
        <ul class="answers">
            {foreach $qas['answers'] as $k=>$subqas}
                <li data-id="{$subqas['sessions_qa_id']}" data-userid="{$subqas['user_id']}">
                    <div class="conversation clearfix ">
                        <div class="conversation-user">
                            <a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">
                                <img src="{$subqas['user_data']['user_picture']}" title="{$subqas['user_data']['user_fullname']}" alt="{$subqas['user_data']['user_fullname']}">
                            </a>
                        </div>
                        <div class="conversation-body full_box">
                            <div class="text">
                                <p><a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">{$subqas['user_data']['user_name']}</a></p>
                                <p class="answer_div more"> {$subqas['post']} </p>
                            </div>
                            <div class="time js_moment" data-time="{$subqas['added_on']}">
                                {$subqas['added_on']}
                            </div>
                        </div>
                        {foreach $subqas['replys'] as $k=>$replys} 
                            <div class="conversation-body full_box reply_data">
                                <div class="text">
                                    <p><a href="{$system['system_url']}/{$replys['user_data']['user_name']}">{$replys['user_data']['user_name']}</a></p>
                                    <p class="answer_div more"> {$replys['post']} </p>
                                </div>
                                <div class="time js_moment" data-time="{$replys['added_on']}">
                                    {$replys['added_on']}
                                </div>
                            </div>
                        {/foreach}
                    </div>
                    <div class="conversation clearfix">
                        {if $subqas['i_like']}
                            <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
                        {else}
                            <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
                        {/if}
                        (<span class="js_qacomment-likes-num_{$subqas['sessions_qa_id']}">{$subqas['total_likes']}</span>)
                        &nbsp;&nbsp;
                        <span class="text-link js_open_reply_box">Reply</span>
                        <div class="reply_box_div">
                            <br/>
                            <form class="js_ajax_add_qa_reply form-horizontal" method="post">
                                <input type="hidden" name="id" value="{$subqas['sessions_qa_id']}">
                                <input type="hidden" name="posted_user_id" value="{$subqas['user_id']}">
                                <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                                <input type="hidden" name="edit" value="add_qa_reply">
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <textarea class="form-control" name="comment_post" type="text" rows="1" required="" {if !$session_data['i_join']}disabled{/if}></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-11">
                                        <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>REPLY</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
            {/foreach}
        </ul>
    </li>
{/foreach}