{assign var="post" value=$photo['post']}

<div class="lightbox-post" data-id="{$post['session_event_image_id']}">
    <div class="js_scroller js_scroller-lightbox" data-slimScroll-height="100%">
        <div class="post-body">
            <div class="mb10 post-header">
                <!-- post picture -->
                <div class="post-avatar">
                    <span class="post-avatar-picture" style="background-image:url({$system['system_url']}/content/uploads/{$photo['sessions_data']['cover_photo']});">
                    </span>
                </div>
                <!-- post picture -->

                <!-- post meta -->
                <div class="post-meta">
                    <!-- post author name & menu -->
                    <div>
                        <!-- post author name -->
                        <span class="js_user-popover" data-type="{$post['user_type']}" data-uid="{$post['user_id']}">
                            <span>{$photo['sessions_data']['title']}</span>
                        </span>
                        <!-- post author name -->
                    </div>
                    <!-- post author name & menu -->

                    <!-- post time & location & privacy -->
                    <div class="post-time">
                        <a href="{$system['system_url']}/posts/{$post['session_event_image_id']}" class="js_moment" data-time="{$post['added_on']}">{$post['added_on']}</a>

                        {if $post['location']}
                            ·
                            <i class="fa fa-map-marker"></i> <span>{$post['location']}</span>
                        {/if}

                        - 
                        <i class="fa fa-globe" data-toggle="tooltip" data-placement="top" title='{__("Shared with")}: {__("Public")}'></i>
                    </div>
                    <!-- post time & location & privacy -->
                </div>
                <!-- post meta -->
            </div>
            <!-- post header -->

            <!-- post actions -->
            <div class="post-actions">
                {if $post['i_like']}
                    <span class="text-link js_unlike-session_photo" date-id="{$post['session_event_image_id']}">{__("Unlike")}</span> - 
                {else}
                    <span class="text-link js_like-session_photo" data-id="{$post['session_event_image_id']}">{__("Like")}</span> - 
                {/if}
                <span class="text-link js_comment">{__("Comment")}</span>
            </div>
            <!-- post actions -->

        </div>

        <!-- post footer -->
        <div class="post-footer">
            {if $photo['is_single']}
                <!-- post stats -->
                <div class="post-stats {if {$post['likes']} == 0}x-hidden{/if}">
                    <!-- likes -->
                    <span class="js_post-likes {if {$post['likes']} == 0}x-hidden{/if}">
                        <i class="fa fa-thumbs-o-up"></i> <span class="text-link" data-toggle="modal" data-url="posts/who_likes_session_image.php?photo_id={$post['session_event_image_id']}"><span class="js_post-likes-num">{$post['likes']}</span> {__("people")}</span> {__("like this")}
                    </span>
                    <!-- likes -->
                </div>
                <!-- post stats -->

                <!-- comments -->
                {include file='__feeds_post.session_image_comments.tpl'}
                <!-- comments -->
            {else}
                <!-- post stats -->
                <div class="post-stats {if {$photo['likes']} == 0}x-hidden{/if}">
                    <!-- likes -->
                    <span class="js_post-likes {if $photo['likes']} == 0}x-hidden{/if}">
                        <i class="fa fa-thumbs-o-up"></i> <span class="text-link" data-toggle="modal" data-url="posts/who_likes_session_image.php?photo_id={$post['session_event_image_id']}"><span class="js_post-likes-num">{$photo['likes']}</span> {__("people")}</span> {__("like this")}
                    </span>
                    <!-- likes -->
                </div>
                <!-- post stats -->

                <!-- comments -->
                {include file='__feeds_photo.session_image_comments.tpl'}
                <!-- comments -->
            {/if}
        </div>
        <!-- post footer -->

    </div>
</div>