{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <!-- conversation -->
        <div class="col-sm-12 offcanvas-mainbar js_conversation-container">
            <div class="panel panel-default panel-messages">
                <div class="panel-body mtb10 mlr5">
                    <div class="row">
                        <div class="col-sm-4  ">
                            <div class="col-sm-12 left-session-box">
                                <div class="row">
                                    <div class="bg-coverimage">
                                        <div class="overlay">
                                            <img src="{$system['system_url']}/content/uploads/{$venue_data['photo']}"/>
                                        </div>
                                    </div>
                                </div>
                                <div >
                                    <div class="">
                                        <div class="another-box">
                                            <div class="col-sm-12">
                                                <h3 class="text-semibold">{$venue_data['venue_name']}</h3>
                                            </div>
                                            <div class="col-sm-12 text-right">
                                                <button class="btn btn-accept">Notify Me</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 left-session-box">
                                <div class="small_title clearfix"> 
                                    <span class="pull-left"> Ratings : </span>
                                    <fieldset class="rating pull-left">
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3half" name="rating" checked="" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                        <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                    </fieldset>
                                </div>
                                <div class="small_title clearfix">Total Students: 1234</div>
                                <div class="small_title clearfix">Joined On: which date to show?</div>
                            </div>

                        </div>
                        <div class="col-sm-8">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <h3>About Venue</h3>
                                    <p>{$venue_data['description']}</p>
                                    <p>{$venue_data['courses_offered']}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style=" margin: 10px 0 0;"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Sessions</h3>
                        </div>
                        <div class="col-sm-12">
                            <ul class="nav nav-pills m-t-10">
                                <li class="active session_li session_first_li">
                                    <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
                                </li>
                                <li class="session_li">
                                    <a data-toggle="pill" href="#tab3">Assigned Sessions</a>
                                </li>
                                <li class="session_li">
                                    <a data-toggle="pill" href="#tab4">Organized Sessions</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="session_tabs_div">
                                <div id="tab2" class="tab-pane fade in active">
                                    <div class="row">
                                        <div id="" class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                {foreach from=$sessions[2] item=conductingVal}
                                                    <div class="panel panel-default" style="margin-top: 10px;">
                                                        <div class="panel-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                <h4 class="panel-title">
                                                                    {$conductingVal['title']}
                                                                </h4>
                                                            </a>
                                                        </div>
                                                        <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                <div class="clearfix"></div>
                                                                <p>{$conductingVal['description']}</p>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                                </div>

                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                </div>
                                                            </div>
                                                            <div class="panel-footer">
                                                                Post by : {$conductingVal['created_by']['user_name']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab3" class="tab-pane fade in">
                                    <div class="row">
                                        <div id="" class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                {foreach from=$sessions[3] item=conductingVal}
                                                    <div class="panel panel-default" style="margin-top: 10px;">
                                                        <div class="panel-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                <h4 class="panel-title">
                                                                    {$conductingVal['title']}
                                                                </h4>
                                                            </a>
                                                        </div>
                                                        <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                <div class="clearfix"></div>
                                                                <p>{$conductingVal['description']}</p>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                                </div>

                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                </div>
                                                            </div>
                                                            <div class="panel-footer">
                                                                Post by : {$conductingVal['created_by']['user_name']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab4" class="tab-pane fade in">
                                    <div class="row">
                                        <div id="" class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                {foreach from=$sessions[4] item=conductingVal}
                                                    <div class="panel panel-default" style="margin-top: 10px;">
                                                        <div class="panel-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                <h4 class="panel-title">
                                                                    {$conductingVal['title']}
                                                                </h4>
                                                            </a>
                                                        </div>
                                                        <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                <div class="clearfix"></div>
                                                                <p>{$conductingVal['description']}</p>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Engaged in Session : {$conductingVal['session_engaged_peoples']}</span>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">People Attend Session : {$conductingVal['session_attend_peoples']}</span>
                                                                </div>

                                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                                    <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                </div>
                                                            </div>
                                                            <div class="panel-footer">
                                                                Post by : {$conductingVal['created_by']['user_name']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr style=" margin: 10px 0 0;"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Teams</h3>
                        </div>
                        <div class="col-sm-12">
                            <ul class="team_ul">
                                {foreach $teams as $teamname => $role}
                                    <li><span> {$teamname} </span> <span class="pull-right"> {$role}</span></li>
                                    {/foreach}

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- conversation -->


    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}