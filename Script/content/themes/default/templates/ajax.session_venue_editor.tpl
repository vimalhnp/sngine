<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Manage Session")}</h5>
</div>
<form class="js_ajax-forms" data-url="session/edit.php" enctype="multipart/form-data">
    <div class="modal-body">
        <div class="form-group">
            <label>{__("Title")}</label>
            <input type="hidden" name="handle" value="session_edit">
            <input type="hidden" name="id" value="{$session['sessions_id']}">
            <input name="title" type="text" value="{$session['title']}" class="form-control" required autofocus disabled>
        </div>
        <div class="form-group">
            <label>{__("Date")}</label>
            <input name="event_date" type="date" value="{$session['event_date']}" class="form-control" required autofocus>
        </div>
        <div class="form-group">
            <label>{__("Time")}</label>
            <input name="event_time" type="time" value="{$session['event_time']}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>{__("Session Duration (in minutes)")}</label>
            <input name="event_duration" type="text" value="{$session['event_duration']}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>{__("Venue")}</label>
            <select  class="form-control js_geocomplete" name="venue">
                {foreach $venues_data as $vv}
                    <option value="{$vv['venue_id']}" {if $vv['venue_id']==$session['venue']}selected{/if}> {$vv['venue_name']} </option>
                {/foreach}
            </select>
        </div>
        <div class="form-group event_photos_div">
            <label>{__("Photos")}</label>
            <p style='clear: both;'></p>
            {if $session['event_photos'] != ""}
                {for $img_cnt=1 to 5}
                    {if $eventPhotoArr[$img_cnt] != ""}
                        <div class="x-image" style="background-image: url('{$system['system_uploads']}/{$eventPhotoArr[$img_cnt]}')">
                            <button type="button" class="close js_x-image-remover" title='{__("Remove")}'>
                                <span>×</span>
                            </button>
                            <div class="loader loader_small x-hidden"></div>
                            <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                            <input type="hidden" class="js_x-image-input" name="event_photo{$img_cnt}" value="{$eventPhotoArr[$img_cnt]}">
                        </div>
                    {else}
                        <div class="x-image">
                            <button type="button" class="close x-hidden js_x-image-remover" title='{__("Remove")}'>
                                <span>×</span>
                            </button>
                            <div class="loader loader_small x-hidden"></div>
                            <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                            <input type="hidden" class="js_x-image-input" name="event_photo{$img_cnt}" value="">
                        </div>
                    {/if}
                {/for}
            {else}
                {for $img_cnt=1 to 5}
                    <div class="x-image">
                        <button type="button" class="close x-hidden js_x-image-remover" title='{__("Remove")}'>
                            <span>×</span>
                        </button>
                        <div class="loader loader_small x-hidden"></div>
                        <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                        <input type="hidden" class="js_x-image-input" name="event_photo{$img_cnt}" value="">
                    </div>
                {/for}
            {/if}
        </div>
        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{__("Edit")}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{__("Cancel")}</button>
    </div>
</form>