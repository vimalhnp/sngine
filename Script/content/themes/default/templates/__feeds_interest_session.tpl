<li class="feeds-item ptb10 plr10" >
    <h4 class="mt0 session_name">
        <a href="{$system['system_url']}/session/{$session_data['sessions_id']}">{$session_data['title']}</a>
    </h4>
    <div class="text-muted" style="width: 50%; float: left;"> 
        <label>Initiated By : </label>
        {$session_data['created_by']['user_name']}
    </div>
    {if $session_data['presentors']!=''}
        <div style="width: 50%; float: left;">
            <label>Sharer : </label>
            {$session_data['presentors']}
        </div>
    {/if}
    {if $session_data['total_attend']!=''}
        <div style="width: 50%; float: left;">
            <label>Total Attendees : </label>
            {$session_data['total_attend']}
        </div>
    {/if}
    {if $session_data['total_shares']!=''}
        <div style="width: 50%; float: left;">
            <label>Total Share(s) : </label>
            {$session_data['total_shares']}
        </div>
    {/if}
    <div class="text-muted" style="width: 50%; float: left;"> 
        <label>Started At : </label>
        {$session_data['added_on']|date_format:"%e %B %Y"}
    </div>
    <div style="clear: both;"></div>
    <h5 class="session_about">
        <p class="text-muted">{$session_data['description']} </p>
    </h5>
    <div class="session_buttons">
        {if !$session_data['event_expired']}
            {if $user->_logged_in && $session_data['i_attend']}
                <button type="button" class="btn btn-default js_unattend-page" data-id="{$session_data['sessions_id']}">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Attending")}
                </button>
            {else}
                <button type="button" class="btn btn-primary js_attend-page" data-id="{$session_data['sessions_id']}">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Attend")}
                </button>
            {/if}
        {else}
            {if $user->_logged_in && $session_data['i_join']}
                <button type="button" class="btn btn-default js_unjoin-page" data-id="{$session_data['sessions_id']}">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Joined")}
                </button>
            {else}
                <button type="button" class="btn btn-primary js_join-page" data-id="{$session_data['sessions_id']}">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Join")}
                </button>
            {/if}
        {/if}

        {if $session_data['presentors'] == ""}
            {if $user->_logged_in && $session_data['i_accept'] && $session_data['status'] != 4}
                <a href="" class="btn btn-default disabled" data-toggle="modal">
                    <div class="action no-desc">
                        <i class="fa fa-user-o"></i> {__("Accept")}
                    </div>
                </a>
            {elseif $session_data['status'] != 4}
                <a href="" class="btn btn-default" data-toggle="modal" data-url="session/accept_editor.php?sessions_id={$session_data['sessions_id']}">
                    <div class="action no-desc">
                        <i class="fa fa-user-o"></i> {__("Accept")}
                    </div>
                </a>
            {/if}
        {/if}

        {if $user->_logged_in && $session_data['status'] == 4}
            {if $session_data['i_thanks'] == 1}
                <button type="button" class="btn btn-default" data-id="{$session_data['sessions_id']}" disabled="disabled">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Thank You")}
                </button>
            {else}
                <button type="button" class="btn btn-primary js_thanks-page" data-id="{$session_data['sessions_id']}">
                    <i class="fa fa-thumbs-o-up"></i>
                    {__("Thanks")}
                </button>
            {/if}
        {/if}

        <a href="javascript:void(0);" class="btn btn-info" id="share_session_btn">
            <div class="action no-desc" onclick="openShareSessionDiv();">
                <i class="fa fa-share-alt"></i> {__("Share")}
            </div>
        </a>
        <div id="share_session_div" class="share_session_div_interest" style="display: none;">
            <a href="http://www.facebook.com/sharer.php?u={$system['system_url']}/session/{$session_data['sessions_id']}" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/intent/tweet?url={$system['system_url']}/session/{$session_data['sessions_id']}&amp;text={$session_data['title']}" target="_blank"><i class="fa fa-twitter"></i></a>
            <i class="fa fa-newspaper-o" onclick="shareSession('{$system['system_url']}/session/{$session_data['sessions_id']}')"></i>
            <i class="fa fa-times" onclick="hideShareSessionBtns()"></i>
        </div>
    </div>
</li>