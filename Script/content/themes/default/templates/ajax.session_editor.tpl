<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5 class="modal-title">{__("Accept Session")}</h5>
</div>
<form class="js_ajax-forms" data-url="session/edit.php">
    <div class="modal-body">
        <div class="form-group">
            <label>{__("Title")}</label>
            <input type="hidden" name="handle" value="session_accept">
            <input type="hidden" name="id" value="{$session['sessions_id']}">
            <input name="title" type="text" value="{$session['title']}" class="form-control" required autofocus>
        </div>
        {*<div class="form-group">
            <label>{__("Date")}</label>
            <input name="event_date" type="date" value="{$session['event_date']}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>{__("Time")}</label>
            <input name="event_time" type="time" value="{$session['event_time']}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>{__("Venue")}</label>
            <input name="venue" type="text" value="{$session['venue']}" class="form-control js_geocomplete">
        </div>*}
        <div class="form-group">
            <label>{__("Dummy question 1")}</label>
            <input name="ans1" type="text" value="" class="form-control">
        </div>
        <div class="form-group">
            <label>{__("Dummy question 2")}</label>
            <input name="ans2" type="text" value="" class="form-control">
        </div>
        <div class="form-group">
            <label>{__("Dummy question 3")}</label>
            <input name="ans3" type="text" value="" class="form-control">
        </div>
        <div class="form-group">
            <label>{__("Dummy question 4")}</label>
            <input name="ans4" type="text" value="" class="form-control">
        </div>
        <div class="form-group">
            <label>{__("Number of Allowed Members")}</label>
            <input name="total_allowed_members" type="number" value="1" min="1" class="form-control">
        </div>
        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{__("Accept")}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{__("Cancel")}</button>
    </div>
</form>