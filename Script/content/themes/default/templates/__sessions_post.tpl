{if !$standalone}<li>{/if}
    <!-- post -->
    <div class="post session-main {if $boosted}boosted{/if}" data-id="{$post['session_data']['sessions_id']}" id="{$post['session_data']['sessions_id']}">

        {if $standalone && $pinned}
            <div class="pin-icon" data-toggle="tooltip" title="{__('Pinned Post')}">
                <i class="fa fa-bookmark"></i>
            </div>
        {/if}

        {if $standalone && $boosted}
            <div class="boosted-icon" data-toggle="tooltip" title="{__('Promoted')}">
                <i class="fa fa-bullhorn"></i>
            </div>
        {/if}

        <!-- post body -->
        <div class="post-body">
            <!-- post header -->
            <div class="post-header">
                <!-- post picture -->
                <div class="post-avatar">
                    <a class="post-avatar-picture" href="{$post['user_data']['post_author_url']}" style="background-image:url({$post['user_data']['post_author_picture']});">
                    </a>
                </div>
                <!-- post picture -->

                <!-- post meta -->
                <div class="post-meta">
                    <!-- post author name & menu -->
                    <div>
                        {if $user->_logged_in}
                            <!-- post menu -->
                            <div class="pull-right flip dropdown">
                                <i class="fa fa-chevron-down dropdown-toggle" data-toggle="dropdown"></i>
                                <ul class="dropdown-menu post-dropdown-menu">
                                    <li>
                                        {if $post['i_save']}
                                            <a href="#" class="js_unsave-post">
                                                <div class="action no-desc">
                                                    <i class="fa fa-bookmark fa-fw"></i> <span>{__("Unsave Post")}</span>
                                                </div>
                                            </a>
                                        {else}
                                            <a href="#" class="js_save-post">
                                                <div class="action no-desc">
                                                    <i class="fa fa-bookmark fa-fw"></i> <span>{__("Save Post")}</span>
                                                </div>
                                            </a>
                                        {/if}
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{$system['system_url']}/posts/{$post['session_data']['sessions_id']}" target="_blank">
                                            <div class="action no-desc">
                                                <i class="fa fa-link fa-fw"></i> {__("Open post in new tab")}
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- post menu -->
                        {/if}

                        <!-- post author name -->
                        <span class="js_user-popover" data-type="{$post['user_data']['user_type']}" data-uid="{$post['user_data']['user_id']}">
                            <a href="{$post['user_data']['post_author_url']}">{$post['user_data']['user_fullname']}</a>
                        </span>
                        {if $post['user_data']['post_author_verified']}
                            {if $post['user_data']['user_type'] == "user"}
                            <i data-toggle="tooltip" data-placement="top" title='{__("Verified User")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                            {else}
                            <i data-toggle="tooltip" data-placement="top" title='{__("Verified Page")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                            {/if}
                        {/if}
                        {if $post['user_data']['user_subscribed']}
                        <i data-toggle="tooltip" data-placement="top" title='{__("Pro User")}' class="fa fa-bolt fa-fw pro-badge"></i>
                        {/if}
                        <!-- post author name -->

                        <!-- post type meta -->
                        <span class="post-title">

                            {if $_get != 'posts_group' && $post['in_group']}
                                <i class="fa fa-caret-right ml5 mr5"></i><a href="{$system['system_url']}/groups/{$post['group_name']}">{$post['group_title']}</a>

                            {elseif $post['in_wall']}
                                <i class="fa fa-caret-right ml5 mr5"></i>
                                <span class="js_user-popover" data-type="user" data-uid="{$post['wall_id']}">
                                    <a href="{$system['system_url']}/{$post['user_data']['user_name']}">{$post['user_data']['user_fullname']}</a>
                                </span>
                            {/if}
                        </span>
                        <!-- post type meta -->
                    </div>
                    <!-- post author name & menu -->

                    <!-- post time & location & privacy -->
                    <div class="post-time">
                        <a href="{$system['system_url']}/session/{$post['session_data']['sessions_id']}" class="js_moment" data-time="{$post['session_data']['added_on']}">{$post['session_data']['added_on']}</a>
                        
                        {if $post['session_data']['location']}
                        ·
                        <i class="fa fa-map-marker"></i> <span>{$post['session_data']['location']}</span>
                        {/if}

                        - 
                    </div>
                    <!-- post time & location & privacy -->
                </div>
                <!-- post meta -->
            </div>
            <!-- post header -->

            <div class="mt10">
                <div class="post-media">
                    <div class="post-media-meta">
                        <a class="title mb5" href="{$post['link']['source_url']}">{$post['link']['source_title']}</a>
                        <a href="{$system['system_url']}/session/{$post['session_data']['sessions_id']}" class="redirect_url">{$post['session_data']['title']}</a>
                    </div>
                </div>
            </div>

            <!-- post actions & stats -->
            <div class="session-actions">

                <!-- post actions -->
                {if $user->_logged_in}
                    {if $post['i_like']}
                        <span class="text-link js_unlike-session">{__("Unlike")}</span> - 
                    {else}
                        <span class="text-link js_like-session">{__("Like")}</span> - 
                    {/if}
{*                    <span class="text-link js_comment">{__("Comment")}</span>*}
{*                    {if $post['privacy'] == "public"}*}
{*                         - *}
                        <span class="text-link session_share_btn">{__("Share")}</span>
{*                    {/if}*}
                {else}
                    <a href="{$system['system_url']}/signin">{__("Please log in to like, share and comment!")}</a>
                {/if}
                <!-- post actions -->

                <!-- post stats -->
                <span class="post-stats-alt {if $post['session_data']['likes']==0 && $post['comments']==0 && $post['shares']==0}x-hidden{/if}">
                    <i class="fa fa-thumbs-o-up"></i> <span class="js_session-likes-num">{$post['session_data']['likes']}</span> 
{*                    <i class="fa fa-comments"></i> <span class="js_session-comments-num">{$post['comments']}</span> *}
                    <i class="fa fa-share"></i> <span class="js_session-shares-num">{$post['shares']}</span>
                </span>
                <div class="share_session_div">
                    <i class="fa fa-facebook session_share_fb"></i>
                    <i class="fa fa-twitter session_share_tw"></i>
                    <i class="fa fa-newspaper-o session_share"></i>
                </div>
                <!-- post stats -->
            </div>
            <!-- post actions & stats -->
        </div>
        <!-- post body -->

        <!-- post footer -->
        <div class="post-footer {if $post['session_data']['likes'] == 0 && $post['comments'] == 0 && $post['shares'] == 0}x-hidden{/if}">

            <!-- post stats (likes & shares) -->
            <div class="post-stats clearfix {if $post['session_data']['likes'] == 0 && $post['shares'] == 0}x-hidden{/if}">
                <!-- shares -->
                <div class="pull-right flip js_session-shares {if $post['shares'] == 0}x-hidden{/if}">
                    <i class="fa fa-share"></i> 
                    <span class="text-link" data-toggle="modal" data-url="posts/who_shares.php?post_id={$post['session_data']['sessions_id']}">
                        {$post['shares']} {__("shares")}
                    </span>
                </div>
                <!-- shares -->

                <!-- likes -->
                <span class="js_session-likes {if {$post['session_data']['likes']} == 0}x-hidden{/if}">
                    <i class="fa fa-thumbs-o-up"></i> <span class="text-link" data-toggle="modal" data-url="posts/who_likes.php?post_id={$post['session_data']['sessions_id']}"><span class="js_session-likes-num">{$post['session_data']['likes']}</span> {__("people")}</span> {__("like this")}
                </span>
                <!-- likes -->
            </div>
            <!-- post stats (likes & shares) -->
        </div>
        <!-- post footer -->

    </div>
    <!-- post -->
{if !$standalone}</li>{/if}