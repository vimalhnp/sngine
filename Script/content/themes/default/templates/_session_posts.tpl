<div class="col-sm-12 offcanvas-mainbar">
    <div class="col-sm-12 col-lg-12 col-md-12 text-readable ptb10 session_nav">
        <div class="pull-right">
            <select class="form-control" onchange="sessionSortByTime(this.value)">
                <option value="time_asc">Date: Asc</option>
                <option value="time_desc">Date: Desc</option>
            </select>
        </div>
        <div class="clearfix"></div>
        <ul class="nav nav-pills m-t-10">
            {*<li class="active">
            <a data-toggle="pill" href="#tab1">Not Verified</a>
            </li>*}
            <li class="active session_li session_first_li">
                <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
            </li>
            <li class="session_li">
                <a data-toggle="pill" href="#tab3">Assigned Sessions</a>
            </li>
            <li class="session_li">
                <a data-toggle="pill" href="#tab4">Organized Sessions</a>
            </li>
        </ul>

        <div class="tab-content" id="session_tabs_div">
            <div id="tab2" class="tab-pane fade in active">
                <div class="row">
                    <div id="" class="col-md-12">
                        <div class="panel-group" id="accordion">
                            {foreach from=$sessions[2] item=conductingVal}
                                <div class="panel panel-default" style="margin-top: 10px;">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                            <h4 class="panel-title">
                                                {$conductingVal['title']}
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                            <div class="clearfix"></div>
                                            {*                                                            <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                            <p>{$conductingVal['description']}</p>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">Friends Attend Session : </span>
                                                <div class="user_image">{$conductingVal['session_attend_friends']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Engaged in Session : </span>
                                                <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Attend Session : </span>
                                                <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            Post by : {$conductingVal['created_by']['user_name']}
                                            {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                            <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab3" class="tab-pane fade in">
                <div class="row">
                    <div id="" class="col-md-12">
                        <div class="panel-group" id="accordion">
                            {foreach from=$sessions[3] item=conductingVal}
                                <div class="panel panel-default" style="margin-top: 10px;">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                            <h4 class="panel-title">
                                                {$conductingVal['title']}
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                            <div class="clearfix"></div>
                                            {*                                                            <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                            <p>{$conductingVal['description']}</p>

                                            {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                            {foreach from=$event_photos item=ep}
                                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                            <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                            </a>
                                            </div>
                                            {/foreach}*}

                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">Friends Attend Session : </span>
                                                <div class="user_image">{$conductingVal['session_attend_friends']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Engaged in Session : </span>
                                                <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Attend Session : </span>
                                                <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                            </div>

                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            Post by : {$conductingVal['created_by']['user_name']}
                                            {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                            <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab4" class="tab-pane fade in">
                <div class="row">
                    <div id="" class="col-md-12">
                        <div class="panel-group" id="accordion">
                            {foreach from=$sessions[4] item=conductingVal}
                                <div class="panel panel-default" style="margin-top: 10px;">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                            <h4 class="panel-title">
                                                {$conductingVal['title']}
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                            <div class="clearfix"></div>
                                            {*                                                            <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                            <p>{$conductingVal['description']}</p>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Engaged in Session : </span>
                                                <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">People Attend Session : </span>
                                                <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <span class="text-info">Sharer : </span>
                                                <div class="user_image">{$conductingVal['presentors']}</div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            Post by : {$conductingVal['created_by']['user_name']}
                                            {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                            <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
