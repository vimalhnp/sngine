{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="offcanvas">
    <div class="d">

        <!-- side panel -->
        <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
            {include file='_sidebar.tpl'}
        </div>
        <!-- side panel -->
        <div class="col-xs-12 offcanvas-mainbar">
            <div class="page-title">
                {$sessions['page_title']}
            </div>

            <div class="d">
                <div class="d">
                    <div class="col-sm-12 col-lg-8 col-lg-offset-2 col-md-12  text-readable ptb10">
                        <div class="d">
                            <div class="d">
                                <div class=" ptb10 session_nav">
                                    {if $current_value == "current_value"}
                                        <div class="alert alert-success alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            <strong>Success!</strong> Session Created Successfully.
                                        </div>
                                    {/if}
                                    <div class="col-sm-3 pull-right">
                                        <select class="form-control" onchange="sessionSortByTimeGuestUsers(this.value)">
                                            <option value="time_asc">Date: Asc</option>
                                            <option value="time_desc">Date: Desc</option>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <ul class="nav nav-pills m-t-10">
                                        {*<li class="active">
                                        <a data-toggle="pill" href="#tab1">Not Verified</a>
                                        </li>*}
                                        <li class="active session_li session_first_li">
                                            <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
                                        </li>
                                        <li class="session_li">
                                            <a data-toggle="pill" href="#tab3">Assigned Sessions</a>
                                        </li>
                                        <li class="session_li">
                                            <a data-toggle="pill" href="#tab4">Organized Sessions</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="session_tabs_div">
                                        <div id="tab2" class="tab-pane fade in active">
                                            <div class="row">
                                                <div id="" class="col-md-12">
                                                    <div class="panel-group" id="accordion">
                                                        {foreach from=$sessions[2] item=conductingVal}
                                                            <div class="panel panel-default" style="margin-top: 10px;">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                        <h4 class="panel-title">
                                                                            {$conductingVal['title']}
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                        <div class="clearfix"></div>
                                                                        {*                                                                        <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                                                        <p>{$conductingVal['description']}</p>

                                                                        {$event_photos = ","|explode:$conductingVal['event_photos']}

                                                                        {* {foreach from=$event_photos item=ep}
                                                                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                                        <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                                        </a>
                                                                        </div>
                                                                        {/foreach}*}

                                                                        {if $conductingVal['user_login'] == 1}
                                                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                                                <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                            </div>
                                                                        {/if}
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Engaged in Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Attend Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        Post by : {$conductingVal['created_by']['user_name']}
                                                                        {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                                        <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab3" class="tab-pane fade in">
                                            <div class="row">
                                                <div id="" class="col-md-12">
                                                    <div class="panel-group" id="accordion">
                                                        {foreach from=$sessions[3] item=conductingVal}
                                                            <div class="panel panel-default" style="margin-top: 10px;">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                        <h4 class="panel-title">
                                                                            {$conductingVal['title']}
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                        <div class="clearfix"></div>
                                                                        {*                                                                        <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                                                        <p>{$conductingVal['description']}</p>

                                                                        {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                                                        {foreach from=$event_photos item=ep}
                                                                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                                        <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                                        </a>
                                                                        </div>
                                                                        {/foreach}*}

                                                                        {if $conductingVal['user_login'] == 1}
                                                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                                                <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                            </div>
                                                                        {/if}
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Engaged in Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Attend Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        Post by : {$conductingVal['created_by']['user_name']}
                                                                        {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                                        <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab4" class="tab-pane fade in">
                                            <div class="row">
                                                <div id="" class="col-md-12">
                                                    <div class="panel-group" id="accordion">
                                                        {foreach from=$sessions[4] item=conductingVal}
                                                            <div class="panel panel-default" style="margin-top: 10px;">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                                                                        <h4 class="panel-title">
                                                                            {$conductingVal['title']}
                                                                        </h4>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
                                                                    <div class="panel-body">
                                                                        <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                                                        <div class="clearfix"></div>
                                                                        {*                                                                        <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">*}
                                                                        <p>{$conductingVal['description']}</p>

                                                                        {*{$event_photos = ","|explode:$conductingVal['event_photos']}

                                                                        {foreach from=$event_photos item=ep}
                                                                        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                                                                        <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                                                                        </a>
                                                                        </div>
                                                                        {/foreach}*}

                                                                        {if $conductingVal['user_login'] == 1}
                                                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                                                <span class="text-info">Friends Attend Session : {$conductingVal['session_attend_friends']}</span>
                                                                            </div>
                                                                        {/if}
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Engaged in Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_engaged_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">People Attend Session : </span>
                                                                            <div class="user_image">{$conductingVal['session_attend_peoples']}</div>
                                                                        </div>
                                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                                            <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel-footer">
                                                                        Post by : {$conductingVal['created_by']['user_name']}
                                                                        {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                                                                        <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--        <div class="container">
                        <div class="row">
                            <div class="col-xs-10 col-xs-offset-1 text-readable ptb10">
                                <ul class="nav nav-pills">
                                    <li class="active">
                                        <a data-toggle="pill" href="#tab1">Conducting a session</a>
                                    </li>
                                    <li>
                                        <a data-toggle="pill" href="#tab2">Requesting For a Session</a>
                                    </li>
                                </ul>
        
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane fade in active">
                                        <div class="row">
                                            <div id="" class="col-md-12">
                                                <div class="panel-group" id="accordion">
            {foreach from=$sessions['conductingData'] item=conductingVal}
                <div class="panel panel-default" style="margin-top: 10px;">
                    <div class="panel-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse-{$conductingVal['sessions_id']}" style="text-decoration:none;">
                            <h4 class="panel-title">
                {$conductingVal['title']}
            </h4>
        </a>
    </div>
    <div id="collapse-{$conductingVal['sessions_id']}" class="panel-collapse collapse">
        <div class="panel-body">
            <img src="{$system['system_uploads']}/{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">
            <p>{$conductingVal['description']}</p>

                {$event_photos = ","|explode:$conductingVal['event_photos']}

                {foreach from=$event_photos item=ep}
                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-caption="" data-image="{$system['system_uploads']}/{$ep}" data-target="#image-gallery">
                            <img class="img-responsive" src="{$system['system_uploads']}/{$ep}" alt="{$ep}">
                        </a>
                    </div>
                {/foreach}

                <div class="col-lg-12 col-md-12 col-xs-12">
                    <span class="text-info">Sharer : {$conductingVal['presentors']}</span>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <span class="text-info">Total Likes : {$conductingVal['likes']}</span>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <span class="text-info">Total Comments : {$conductingVal['total_comments']}</span>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <span class="text-info">Total Shares : {$conductingVal['shares']}</span>
                </div>
            </div>
            <div class="panel-footer">
                Post by : {$conductingVal['created_by']['user_name']}
                {*<div class="btn-group btn-group-xs"><span class="btn">Was this question useful?</span><a class="btn btn-success" href="#"><i class="fa fa-thumbs-up"></i> Yes</a> <a class="btn btn-danger" href="#"><i class="fa fa-thumbs-down"></i> No</a></div>
                <div class="btn-group btn-group-xs pull-right"><a class="btn btn-primary" href="#">Report this question</a></div>*}
            </div>
        </div>
    </div>
            {/foreach}
        </div>
    </div>
</div>
            {*{foreach from=$sessions['conductingData'] item=conductingVal}
            <h4>{$conductingVal['title']}</h4>
            <img src="{$conductingVal['cover_photo']}" alt='{$conductingVal['cover_photo']}' width="50%">
            <p>{$conductingVal['description']}</p>
            <hr/>
            {/foreach}*}
        </div>
        <div id="tab2" class="tab-pane fade">
            {foreach from=$sessions['requestingData'] item=requestingVal}
                <h4>{$requestingVal['title']}</h4>
                <p>{$requestingVal['description']}</p>
                <hr/>
            {/foreach}
        </div>

    </div>

</div>
</div>
</div> -->
        </div>

    </div>
    <!-- page content -->
    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive" src="">
                </div>
                <div class="modal-footer">

                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" id="show-previous-image">Previous</button>
                    </div>

                    <div class="col-md-8 text-justify" id="image-gallery-caption">
                        This text will be overwritten by jQuery
                    </div>

                    <div class="col-md-2">
                        <button type="button" id="show-next-image" class="btn btn-default">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {include file='_footer.tpl'}