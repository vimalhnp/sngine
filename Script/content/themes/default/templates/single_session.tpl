{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container offcanvas">
    <div class="row">
        <div class="col-xs-12 offcanvas-mainbar">
            <!-- profile-header -->
            <div class="session-header">
                <div class="row">
                    <!-- profile-cover -->
                    <div class="col-md-8 col-xs-8 col-lg-8 mt0">
                        <img class="session-cover-wrapper" src="{$session_data['page_picture']}"
                             alt="{$session_data['page_picture']}"/>
                    </div>
                    <!-- profile-cover -->
                    <div class="col-md-4 col-xs-4 col-lg-4 session_detail_div">
                        <div class="session_date_time">
                            <p id="session_month">{$session_data['added_on']|date_format:"%b"}</p>
                            <p id="session_day">{$session_data['added_on']|date_format:"%e"}</p>
                        </div>
                        <div class="session_name">
                            <p>{$session_data['title']}</p>
                        </div>
                        <div class="session_presentor">
                            <p>
                                by
                                {if $session_data['presentors'] != ""}
                                    {$session_data['presentors']}
                                {else}
                                    <a href="{$system['system_url']}/{$session_data['created_by']['user_name']}">{$session_data['created_by']['user_name']}</a>
                                {/if}
                            </p>
                        </div>
                        <div>
                            <span class="text-link" data-toggle="modal"
                                  data-url="posts/who_attended.php?sessions_id={$session_data['sessions_id']}"> <h3
                                        class="session_all_titles">Total Attendees ({$session_data['total_attend']}
                                    )</h3></span>
                        </div>
                    </div>
                </div>

            </div>
            <!-- profile-header -->

            <div class="USP_bar  panel panel-default mb0">
                <!-- profile-cover -->
                <div class="col-md-12 col-xs-12 col-lg-12 mt0 plr0">
                    <!-- profile-buttons -->
                    <div>
                        <div class="pull-left profile-tabs-wrapper" style="padding-left: 0!important;">
                            <ul class="nav">
                                <li>
                                    <a class="" href="{$system['system_url']}/session/{$session_data['sessions_id']}">
                                        {__("Details")}
                                    </a>
                                </li>
                                {if $user->_logged_in && $session_data['status'] == 4}
                                    <li>
                                        <a class=""
                                           href="{$system['system_url']}/session/{$session_data['sessions_id']}/photos">
                                            {__("Photos")} {if {$session_data['photos_cnt']} > 0 }({$session_data['photos_cnt']}){/if}
                                        </a>
                                    </li>
                                {/if}
                                {if !$session_data['event_expired']}
                                    <li>
                                        <a class=""
                                           href="{$system['system_url']}/session/{$session_data['sessions_id']}/discussion">
                                            {__("Discussion")} {if {$session_data['discussions_cnt']} > 0 }({$session_data['discussions_cnt']}){/if}
                                        </a>
                                    </li>
                                {else}
                                    <li>
                                        <a class=""
                                           href="{$system['system_url']}/session/{$session_data['sessions_id']}/comments">
                                            {__("Comments")} {if {$session_data['comments_cnt']} > 0 }({$session_data['comments_cnt']}){/if}
                                        </a>
                                    </li>
                                    <li>
                                        <a class=""
                                           href="{$system['system_url']}/session/{$session_data['sessions_id']}/queans">
                                            {__("Questionnaires")} {if {$session_data['qaData_cnt']} > 0 }({$session_data['qaData_cnt']}){/if}
                                        </a>
                                    </li>
                                {/if}
                            </ul>
                        </div>
                        <div class="pull-right session_buttons_list">
                            {if !$session_data['event_expired']}
                                {if $user->_logged_in && $session_data['i_attend']}
                                    <button type="button" class="btn btn-default js_unattend-page"
                                            data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Attending")}
                                    </button>
                                {else}
                                    <button type="button" class="btn btn-default js_attend-page"
                                            data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Attend")}
                                    </button>
                                {/if}
                            {else}
                                {if $user->_logged_in && $session_data['i_join']}
                                    <button type="button" class="btn btn-default js_unjoin-page"
                                            data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Joined")}
                                    </button>
                                {else}
                                    <button type="button" class="btn btn-default js_join-page"
                                            data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Join")}
                                    </button>
                                {/if}
                            {/if}

                            {if $session_data['presentors'] == ""}
                                {if $user->_logged_in && $session_data['i_accept'] && $session_data['status'] != 4}
                                    <a href="" class="btn btn-default disabled" data-toggle="modal">
                                        <div class="action no-desc">
                                            <i class="fa fa-user-o"></i> {__("Accept")}
                                        </div>
                                    </a>
                                {elseif $session_data['status'] != 4}
                                    <a href="" class="btn btn-default" data-toggle="modal"
                                       data-url="session/accept_editor.php?sessions_id={$session_data['sessions_id']}">
                                        <div class="action no-desc">
                                            <i class="fa fa-user-o"></i> {__("Accept")}
                                        </div>
                                    </a>
                                {/if}
                            {/if}

                            {if $user->_logged_in && $session_data['status'] == 4}
                                {if $session_data['i_thanks'] == 1}
                                    <button type="button" class="btn btn-default"
                                            data-id="{$session_data['sessions_id']}" disabled="disabled">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Thank You")}
                                    </button>
                                {else}
                                    <button type="button" class="btn btn-default js_thanks-page"
                                            data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Thanks")}
                                    </button>
                                {/if}
                            {/if}

                            <a href="javascript:void(0);" class="btn btn-default" id="share_session_btn">
                                <div class="action no-desc" onclick="openShareSessionDiv();">
                                    <i class="fa fa-share-alt"></i> {__("Share")}
                                </div>
                            </a>
                            <div id="share_session_div" class="share_session_div" style="display: none;">
                                <a href="http://www.facebook.com/sharer.php?u={$system['system_url']}/session/{$session_data['sessions_id']}"
                                   target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/intent/tweet?url={$system['system_url']}/session/{$session_data['sessions_id']}&amp;text={$session_data['title']}"
                                   target="_blank"><i class="fa fa-twitter"></i></a>
                                <i class="fa fa-newspaper-o"
                                   onclick="shareSession('{$system['system_url']}/session/{$session_data['sessions_id']}')"></i>
                                <i class="fa fa-times" onclick="hideShareSessionBtns()"></i>
                            </div>
                        </div>
                    </div>
                    <!-- profile-tabs -->
                </div>
            </div>

            <!-- view content -->
            {if $view == "single_session"}
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-body">


                            <!-- profile-cover -->
                            <div class="col-md-8 col-xs-8 col-lg-8 plr0">


                                <ul class="about-list">
                                    {$session_data['addEditVenue']}
                                    <!-- description -->
                                    {if $session_data['description']}
                                        <li>
                                            <h3 class="session_all_titles">Description</h3>
                                            <div class="about-list-item">
                                                {$session_data['description']}
                                            </div>
                                        </li>
                                    {/if}
                                    <!-- description -->
                                    {if $session_data['presenter_data']}
                                        <li>
                                            <h3 class="session_all_titles">Prerequisite knowledge</h3>
                                            <div class="about-list-item">
                                                {$session_data['presenter_data'][0]}
                                            </div>
                                        </li>
                                    {/if}

                                    {if $session_data['status'] != 4}
                                        <!-- category -->
                                        {if $session_data['session_attend_friends'] != ""}
                                            <!--                                            <li class="session_attend_friends_label_li">
                                                                                            <h3 class="session_all_titles">Friends Attend Session</h3>
                                                                                            <div class="about-list-item">
                                            {$session_data['session_attend_friends']}
                                        </div>
                                    </li>-->
                                        {/if}
                                        {if $session_data['session_attend_friends'] != ""}
                                            <!--                                            <li class="session_attend_friends_label_li">
                                                                                            <h3 class="session_all_titles">People Attended </h3>
                                                                                            <div class="about-list-item">
                                            {$session_data['session_attend_peoples']}
                                        </div>
                                    </li>-->
                                        {/if}
                                    {/if}
                                    {if $session_data['presentors'] != ""}
                                        <li class="clearfix">
                                            <h3 class="session_all_titles">initiator</h3>
                                            <div class="about-list-item">
                                                {if {$session_data['created_by']['user_name']} != ""}
                                                    <a href="{$system['system_url']}/{$session_data['created_by']['user_name']}">{$session_data['created_by']['user_name']}</a>
                                                {else}
                                                    {$session_data['presentors']}
                                                {/if}
                                            </div>
                                        </li>
                                    {/if}
                                    <li>
                                        <h3 class="session_all_titles">Total Share ({$session_data['total_shares']}
                                            )</h3>
                                    </li>
                                    <li>
                                        <h3 class="session_all_titles">Interests</h3>
                                        <div class="about-list-item">
                                            {foreach $sessionInterestArr as $interest_key => $interest_value}
                                                <a href="{$system['system_url']}/interest/{$interest_value['interest_id']}"
                                                   class="span interest_tag">{$interest_value['session_name']}</a>
                                            {/foreach}
                                        </div>
                                    </li>
                                    <!-- category -->
                                </ul>
                            </div>

                            <!-- profile-cover -->
                            <div class="col-md-4 col-xs-4 col-lg-4 session_detail_div plr0">
                                <h3 class="session_all_titles">Date and Time</h3>
                                <div>
                                    {$session_data['event_date']|date_format:"%e %B %Y"} {if $session_data['event_time'] != "00:00:00"} {$session_data['event_time']} {/if}
                                </div>
                                <br/><br/>
                                <h3 class="session_all_titles">Location</h3>
                                <div>
                                    {$session_data['location']} <br/> {$session_data['venue']}
                                </div>
                                <br/>
                                {if $session_data['session_engaged_peoples'] != ""}
                                    <h3 class="session_all_titles">People Engaged in Discussion</h3>
                                    <div style="height: 70px;">
                                        {$session_data['session_engaged_peoples']}
                                    </div>
                                {/if}
                            </div>
                        </div>

                    </div>
                </div>
            {elseif $view == "photos"}
                <!-- photos -->
                <div class="">
                    <div class="panel panel-default panel-photos">
                        <div class="panel-heading with-icon">
                            <!-- panel title -->
                            <div>
                                <i class="fa fa-picture-o pr5 panel-icon"></i>
                                <strong>{__("Photos")}</strong>
                            </div>
                            <!-- panel title -->

                        </div>
                        <div class="panel-body">
                            {if count($session_data['photos']) > 0}
                                <ul class="row">
                                    {foreach $session_data['photos'] as $key=>$photo}
                                        <li class="col-xs-6 col-sm-2">
                                            <a href="{$system['system_uploads']}/{$photo}"
                                               class="pg_photo session_images" data-id="{$key}"
                                               data-image="{$system['system_uploads']}/{$photo}" data-context="post"
                                               style="background-image:url({$system['system_uploads']}/{$photo});"></a>
                                        </li>
                                    {/foreach}
                                </ul>
                                {*{if count($session_data['photos']) >= $system['min_results_even']}
                                <!-- see-more -->
                                <div class="alert alert-info see-more js_see-more" data-get="photos" data-id="{$session_data['sessions_id']}" data-type='page'>
                                <span>{__("See More")}</span>
                                <div class="loader loader_small x-hidden"></div>
                                </div>
                                <!-- see-more -->
                                {/if}*}
                            {else}
                                <p class="text-center text-muted mt10">
                                    {$session_data['title']} {__("doesn't have photos")}
                                </p>
                            {/if}
                            {if $user->_is_admin}
                                <form class="js_ajax-forms" data-url="session/edit.php" enctype="multipart/form-data">
                                    <input type="hidden" name="handle" value="session_photos_edit">
                                    <input type="hidden" name="id" value="{$session_data['sessions_id']}">
                                    <div class="form-group event_photos_div">
                                        <label>{__("Photos")}</label>
                                        <p style='clear: both;'></p>
                                        {if count($session_data['photos']) > 0}
                                            {foreach $session_data['photos'] AS $img_cnt=> $imgs}
                                                {if $imgs != ""}
                                                    <div class="x-image"
                                                         style="background-image: url('{$system['system_uploads']}/{$imgs}')">
                                                        {*<button type="button" class="close js_x-image-remover"*}
                                                                {*title='{__("Remove")}'>*}
                                                            {*<span>×</span>*}
                                                        {*</button>*}
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader"
                                                           data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input"
                                                               name="edit_event_photo[{$img_cnt}]"
                                                               >
                                                    </div>
                                                {else}
                                                    <div class="x-image">
                                                        {*<button type="button" class="close x-hidden js_x-image-remover"*}
                                                                {*title='{__("Remove")}'>*}
                                                            {*<span>×</span>*}
                                                        {*</button>*}
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader"
                                                           data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input"
                                                               name="event_photo[{$img_cnt}]" value="">
                                                    </div>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                        {for $img_cnt=count($session_data['photos'])+1 to 10 }
                                            <div class="x-image">
                                                {*<button type="button" class="close x-hidden js_x-image-remover"*}
                                                        {*title='{__("Remove")}'>*}
                                                    {*<span>×</span>*}
                                                {*</button>*}
                                                <div class="loader loader_small x-hidden"></div>
                                                <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                                <input type="hidden" class="js_x-image-input"
                                                       name="event_photo[{$img_cnt}]" value="">
                                            </div>
                                        {/for}
                                        <div style="clear: both;">
                                            <button type="submit" class="btn btn-primary">{__("Save")}</button>
                                        </div>
                                    </div>
                                </form>
                            {/if}
                        </div>
                    </div>
                </div>
                <!-- photos -->

            {elseif $view == "discussion"}
                <!-- photos -->
                <div class="">
                    <div class="panel panel-default panel-photos">
                        <div class="panel-heading with-icon">
                            <!-- panel title -->
                            <div>
                                <i class="fa fa-info pr5 panel-icon"></i>
                                <strong>{__("Discussion")}</strong>
                            </div>
                            <!-- panel title -->

                        </div>
                        <div class="panel-body">
                            <ul class="discussed_list">

                                {foreach $session_data['discussions'] as $key=>$discussions}
                                    <li>
                                        <div class="conversation clearfix {if $discussions['user_id'] == $user->_data['user_id']}{*right*}{/if}"
                                             id="{$discussions['sessions_discussion_id']}">
                                            <div class="conversation-user">
                                                <a href="{$system['system_url']}/{$discussions['user_data']['user_name']}">
                                                    <img src="{$discussions['user_data']['user_picture']}"
                                                         title="{$discussions['user_data']['user_fullname']}"
                                                         alt="{$discussions['user_data']['user_fullname']}">
                                                </a>
                                            </div>
                                            <div class="conversation-body">
                                                <div class="text">
                                                    <p>
                                                        <a href="{$system['system_url']}/{$discussions['user_data']['user_name']}">{$discussions['user_data']['user_name']}</a>
                                                    </p>
                                                    {$discussions['post']}
                                                </div>
                                                <div class="time js_moment" data-time="{$discussions['added_on']}">
                                                    {$discussions['added_on']}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                {/foreach}
                            </ul>
                            <form class="js_ajax_add_discussion form-horizontal" method="post">
                                {if $session_data['event_expired']}
                                    <label class="alert alert-danger col-sm-12">This Session is Expired, you can Comment
                                        on it.</label>
                                {/if}
                                <input type="hidden" name="id" value="{$session_data['sessions_id']}">
                                <input type="hidden" name="edit" value="add_discussion">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Post new comment
                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="discussion_post" type="text" required=""
                                               {if $session_data['event_expired']}disabled{/if}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"
                                                {if $session_data['event_expired']}disabled{/if}>POST
                                        </button>
                                    </div>
                                </div>
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- photos -->
            {elseif $view == "comments"}
                <!-- photos -->
                <div class="">
                    <div class="panel panel-default panel-photos">
                        <div class="panel-heading with-icon">
                            <!-- panel title -->
                            <div>
                                <i class="fa fa-info pr5 panel-icon"></i>
                                <strong>{__("Comments")}</strong>
                            </div>
                            <!-- panel title -->

                        </div>
                        <div class="panel-body">
                            <ul class="discussed_list">

                                {foreach $session_data['comments'] as $key=>$comments}
                                    <li>
                                        <div class="conversation clearfix {if $comments['user_id'] == $user->_data['user_id']}{*right*}{/if}"
                                             id="{$comments['sessions_comment_id']}">
                                            <div class="conversation-user">
                                                <a href="{$system['system_url']}/{$comments['user_data']['user_name']}">
                                                    <img src="{$comments['user_data']['user_picture']}"
                                                         title="{$comments['user_data']['user_fullname']}"
                                                         alt="{$comments['user_data']['user_fullname']}">
                                                </a>
                                            </div>
                                            <div class="conversation-body">
                                                <div class="text">
                                                    <p>
                                                        <a href="{$system['system_url']}/{$comments['user_data']['user_name']}">{$comments['user_data']['user_name']}</a>
                                                    </p>
                                                    {$comments['post']}
                                                </div>
                                                <div class="time js_moment" data-time="{$comments['added_on']}">
                                                    {$comments['added_on']}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                {/foreach}
                            </ul>
                            <form class="js_ajax_add_discussion form-horizontal" method="post">
                                {if !$session_data['i_join']}
                                    <label class="alert alert-danger col-sm-12">Please <strong>Join</strong> the session
                                        to discuss on it.</label>
                                {/if}
                                <input type="hidden" name="id" value="{$session_data['sessions_id']}">
                                <input type="hidden" name="edit" value="add_comment">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Post new comment
                                    </label>
                                    <div class="col-sm-9">
                                        <input class="form-control" name="comment_post" type="text" required=""
                                               {if !$session_data['i_join']}disabled{/if}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9 col-sm-offset-3">
                                        <button type="submit" class="btn btn-primary"
                                                {if !$session_data['i_join']}disabled{/if}>POST
                                        </button>
                                    </div>
                                </div>
                                <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- photos -->
            {elseif $view == "queans"}
                <!-- photos -->
                <div class="">
                    <div class="panel panel-default panel-photos">
                        <div class="panel-heading with-icon">
                            <!-- panel title -->
                            <div>
                                <i class="fa fa-info pr5 panel-icon"></i>
                                <strong>{__("Questions and Answers")}</strong>
                            </div>
                            <!-- panel title -->

                        </div>
                        <div class="panel-body">
                            <div class="ask_a_question">
                                <form class="js_ajax_add_qa_question form-horizontal" method="post">
                                    <input type="hidden" name="id" value="0">
                                    <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                                    <input type="hidden" name="edit" value="add_qa_question">
                                    {if !$session_data['i_join']}
                                        <div class="alert alert-danger"> Please join the Session to ask Questions!</div>
                                    {/if}
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Ask new Question
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="question_post" type="text" required=""
                                                      {if !$session_data['i_join']}disabled{/if}></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button type="submit" class="btn btn-primary"
                                                    {if !$session_data['i_join']}disabled{/if}>POST
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="pull-right">
                                <select class="form-control"
                                        onchange="commentSorting(this.value, {$session_data['sessions_id']});">
                                    <option value="date_asc">Date : ASC</option>
                                    <option value="date_desc">Date : DESC</option>
                                    <option value="rating_asc">Rating : ASC</option>
                                    <option value="rating_desc">Rating : DESC</option>
                                </select>
                            </div>
                            <ul id="queans_list">

                                {foreach $session_data['qaData'] as $key=>$qas}
                                    <li data-id="{$qas['sessions_qa_id']}" data-userid="{$qas['user_id']}">
                                        <div class="conversation clearfix question_div {if $qas['user_id'] == $user->_data['user_id']}{*right*}{/if}"
                                             id="{$qas['sessions_qa_id']}">
                                            <div class="conversation-user">
                                                <a href="{$system['system_url']}/{$qas['user_data']['user_name']}">
                                                    <img src="{$qas['user_data']['user_picture']}"
                                                         title="{$qas['user_data']['user_fullname']}"
                                                         alt="{$qas['user_data']['user_fullname']}">
                                                </a>
                                            </div>
                                            <div class="conversation-body">
                                                <div class="text">
                                                    <p>
                                                        <a href="{$system['system_url']}/{$qas['user_data']['user_name']}">{$qas['user_data']['user_name']}</a>
                                                    </p>
                                                    {$qas['post']}
                                                </div>
                                                <div class="time js_moment" data-time="{$qas['added_on']}">
                                                    {$qas['added_on']}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix row" style="margin:25px 0 5px 40px">
                                            <span class="text-link js_open_answer_box">Answer</span>
                                            ({$qas['answers']|count}) |
                                            {if $qas['i_like']}
                                                <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
                                            {else}
                                                <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
                                            {/if}
                                            (<span class="js_qacomment-likes-num_{$qas['sessions_qa_id']}">{$qas['total_likes']}</span>)
                                            |
                                            <span class="text-link publish_session">Share</span>

                                            <form class="js_ajax_add_qa_answer form-horizontal" method="post">
                                                <input type="hidden" name="id" value="{$qas['sessions_qa_id']}">
                                                <input type="hidden" name="posted_user_id" value="{$qas['user_id']}">
                                                <input type="hidden" name="sessions_id"
                                                       value="{$session_data['sessions_id']}">
                                                <input type="hidden" name="edit" value="add_qa_answer">

                                                {if !$session_data['i_join']}
                                                    <div class="alert alert-danger"> Please join the Session to Answer
                                                        this Question!
                                                    </div>
                                                {/if}

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">
                                                        Answer above Question
                                                    </label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" name="comment_post" type="text"
                                                                  required=""
                                                                  {if !$session_data['i_join']}disabled{/if}></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-9 col-sm-offset-3">
                                                        <button type="submit" class="btn btn-primary"
                                                                {if !$session_data['i_join']}disabled{/if}>SUBMIT
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <ul class="answers">
                                            {foreach $qas['answers'] as $k=>$subqas}
                                                <li data-id="{$subqas['sessions_qa_id']}"
                                                    data-userid="{$subqas['user_id']}">
                                                    <div class="conversation clearfix ">
                                                        <div class="conversation-user">
                                                            <a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">
                                                                <img src="{$subqas['user_data']['user_picture']}"
                                                                     title="{$subqas['user_data']['user_fullname']}"
                                                                     alt="{$subqas['user_data']['user_fullname']}">
                                                            </a>
                                                        </div>
                                                        <div class="conversation-body full_box">
                                                            <div class="text">
                                                                <p>
                                                                    <a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">{$subqas['user_data']['user_name']}</a>
                                                                </p>
                                                                <p class="answer_div more"> {$subqas['post']} </p>
                                                            </div>
                                                            <div class="time js_moment"
                                                                 data-time="{$subqas['added_on']}">
                                                                {$subqas['added_on']}
                                                            </div>
                                                        </div>
                                                        {foreach $subqas['replys'] as $k=>$replys}
                                                            <div class="conversation-body full_box reply_data">
                                                                <div class="text">
                                                                    <p>
                                                                        <a href="{$system['system_url']}/{$replys['user_data']['user_name']}">{$replys['user_data']['user_name']}</a>
                                                                    </p>
                                                                    <p class="answer_div more"> {$replys['post']} </p>
                                                                </div>
                                                                <div class="time js_moment"
                                                                     data-time="{$replys['added_on']}">
                                                                    {$replys['added_on']}
                                                                </div>
                                                            </div>
                                                        {/foreach}
                                                    </div>
                                                    <div class="conversation clearfix">
                                                        {if $subqas['i_like']}
                                                            <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
                                                        {else}
                                                            <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
                                                        {/if}
                                                        (<span class="js_qacomment-likes-num_{$subqas['sessions_qa_id']}">{$subqas['total_likes']}</span>)
                                                        &nbsp;&nbsp;
                                                        <span class="text-link js_open_reply_box">Reply</span>
                                                        <div class="reply_box_div">
                                                            <br/>
                                                            <form class="js_ajax_add_qa_reply form-horizontal"
                                                                  method="post">
                                                                <input type="hidden" name="id"
                                                                       value="{$subqas['sessions_qa_id']}">
                                                                <input type="hidden" name="posted_user_id"
                                                                       value="{$subqas['user_id']}">
                                                                <input type="hidden" name="sessions_id"
                                                                       value="{$session_data['sessions_id']}">
                                                                <input type="hidden" name="edit" value="add_qa_reply">
                                                                {if !$session_data['i_join']}
                                                                    <div class="alert alert-danger"> Please join the
                                                                        Session to Reply on this Question!
                                                                    </div>
                                                                {/if}
                                                                <div class="form-group">
                                                                    <div class="col-sm-11">
                                                                        <textarea class="form-control"
                                                                                  name="comment_post" type="text"
                                                                                  rows="1" required=""
                                                                                  {if !$session_data['i_join']}disabled{/if}></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-11">
                                                                        <button type="submit" class="btn btn-primary"
                                                                                {if !$session_data['i_join']}disabled{/if}>
                                                                            REPLY
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- photos -->

            {/if}
        </div>
        <!-- profile-content -->
    </div>

</div>
</div>
<!-- page content -->

{include file='_footer.tpl'}