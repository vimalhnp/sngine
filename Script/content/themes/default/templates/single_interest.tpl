{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <!-- conversation -->
        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8 offcanvas-mainbar js_conversation-container">
            <div class="panel panel-default panel-messages">
                <div class="panel-heading clearfix">
                    <h3 class="mt5 text-semibold">{$interest_data['text']}</h3>
                    {if $interest_data['parent_interest']!=''}
                        <h4 class="mt0 text-muted">
                            <a href="{$system['system_url']}/interest/{$interest_data['parent_id']}">({$interest_data['parent_interest']}
                                )</a>
                        </h4>
                    {/if}
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills m-t-10 isession_tabs">
                        <li class="active session_li session_first_li">
                            <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
                        </li>
                        <li class="session_li">
                            <a data-toggle="pill" href="#tab3">Assigned Sessions</a>
                        </li>
                        <li class="session_li">
                            <a data-toggle="pill" href="#tab4">Organized Sessions</a>
                        </li>
                    </ul>
                    <div class="js_scroller isession_data tab-content" id="session_tabs_div"
                         data-slimScroll-height="100%">
                        <div id="tab2" class="tab-pane fade in active">
                            <ul>
                                {foreach $sessionInterestArr['upcoming'] as $session_data}
                                    {include file='__feeds_interest_session.tpl' conversation=$session_data}
                                {/foreach}
                            </ul>
                        </div>
                        <div id="tab3" class="tab-pane fade in">
                            <ul>
                                {foreach $sessionInterestArr['running'] as $session_data}
                                    {include file='__feeds_interest_session.tpl' conversation=$session_data}
                                {/foreach}
                            </ul>
                        </div>
                        <div id="tab4" class="tab-pane fade in">
                            <ul>
                                {foreach $sessionInterestArr['expired'] as $session_data}
                                    {include file='__feeds_interest_session.tpl' conversation=$session_data}
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- conversation -->

        <!-- threads -->
        <div class="col-sm-12 col-xs-12 col-lg-4 col-md-4 ">
            <div class="text-right mtb10">
                {if $i_follow == 1}
                    <button type="button" class="btn btn-sm js_unfollow-interest"
                            data-id="{$interest_data['interest_id']}">
                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                        {__("Following")}
                    </button>
                {else}
                    <button type="button" class="btn btn-sm btn-success js_follow-interest"
                            data-id="{$interest_data['interest_id']}">
                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                        {__("Follow Interest")}
                    </button>
                {/if}
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    {__("About")}
                </div>
                <div class="panel-body" style="padding: 5px;">
                    <ul class="interest_about_div">
                        <li>{$interest_data['description']}</li>
                    </ul>
                    <div class="numericBox">
                        <ul>
                            <li><p class="numeric_digit">{$totalSessions}</p>
                                <p class="numeric_label">Total Sessions</p></li>
                            <li><p class="numeric_digit">{$totalQues}</p>
                                <p class="numeric_label">Total Qurstions Asked</p></li>
                            <li>
                                <span class="text-link" data-toggle="modal" data-url="posts/who_interest_followed.php?interest_id={$interest_data['interest_id']}">
                                    <p class="numeric_digit">{$totalAttendes}</p> <p class="numeric_label">No. of People Followed</p>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    {__("Related Interests")}
                </div>
                <div class="panel-body">
                    <div class="js_scroller" data-slimScroll-height="100%">
                        <ul class="related_interest_div">
                            {foreach $related_interest_arr as $related}
                                <li>
                                    <div class="data-container">
                                        <a href="{$system['system_url']}/interest/{$related['interest_id']}">
                                            <img class="data-avatar" src="{$related['image']}" alt="test1">
                                        </a>
                                        <div class="data-content">
                                            <div class="pull-right flip">
                                                {if $related['i_follow_related'] == 1}
                                                    <button type="button"
                                                            class="btn btn-sm js_unfollow-interest"
                                                            data-id="{$related['interest_id']}">
                                                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                        {__("Following")}
                                                    </button>
                                                {else}
                                                    <button type="button"
                                                            class="btn btn-sm btn-success js_follow-interest"
                                                            data-id="{$related['interest_id']}">
                                                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                        {__("Follow Interest")}
                                                    </button>
                                                {/if}
                                            </div>
                                            <div>
                                                <span class="name"> <a
                                                            href="{$system['system_url']}/interest/{$related['interest_id']}">{$related['text']}</a> </span>
                                                <div>{if $related['total_follower'] > 0}
                                                        {$related['total_follower']} Followers
                                                    {/if}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/foreach}

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- threads -->

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}