{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">
        <!-- conversation -->
        <div class="col-md-8 col-sm-12 offcanvas-mainbar js_conversation-container">
            <div class="panel panel-default panel-messages">
                <div class="panel-heading clearfix">
                    <h3 class="mt5 text-semibold">Sessions</h3>
                </div>
                <div class="m-t-10 ml10 col-md-6 col-sm-12" style="float: left;">
                    <a href="{$system['system_url']}/create/session" class=" btn btn-sm btn-primary mr10 " style="width: 47%;">{__("Create Session")}</a>
                    <a href="{$system['system_url']}/conduct/session" class="btn btn-sm btn-primary " style="width: 47%;">{__("Conduct Session")}</a>
                </div>
                <div class="pull-right m-t-10 mr10">

                    <select class="form-control" onchange="sessionSortByTime(this.value)">
                        <option value="time_asc">Date: Asc</option>
                        <option value="time_desc">Date: Desc</option>
                    </select>
                </div>
                <div class="panel-heading clearfix">
                    <h3 class="mt5 text-semibold">{$sessions['text']}</h3>
                    {if $sessions['parent_interest']!=''}
                        <h4 class="mt0 text-muted">
                            <a href="{$system['system_url']}/interest/{$sessions['parent_id']}">({$sessions['parent_interest']}
                                )</a>
                        </h4>
                    {/if}
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills m-t-10 isession_tabs">
                        <li class="active session_li session_first_li">
                            <a data-toggle="pill" href="#tab2">Upcoming Sessions</a>
                        </li>
                        <li class="session_li">
                            <a data-toggle="pill" href="#tab3">Assigned Sessions</a>
                        </li>
                        <li class="session_li">
                            <a data-toggle="pill" href="#tab4">Organized Sessions</a>
                        </li>
                    </ul>
                    <div class="js_scroller isession_data tab-content" id="session_tabs_div"
                         data-slimScroll-height="100%">
                        <div id="tab2" class="tab-pane fade in active">
                            <ul>
                                {foreach from=$sessions[2] item=conductingVal}
                                    <li class="feeds-item ptb10 plr10">
                                        <h4 class="mt0 session_name">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                        </h4>
                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Initiated By : </label>
                                            {$conductingVal['created_by']['user_name']}
                                        </div>
                                        {if $conductingVal['presentors']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Sharer : </label>
                                                {$conductingVal['presentors']}
                                            </div>
                                        {/if}

                                        {if $conductingVal['total_attends']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Total Attendees : </label>
                                                {$conductingVal['total_attends']}
                                            </div>
                                        {/if}
                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Started At : </label>
                                            {$conductingVal['added_on']|date_format:"%e %B %Y"}
                                        </div>
                                        <div style="clear: both;"></div>
                                        <h5 class="session_about">
                                            <p class="text-muted">{$conductingVal['description']} </p>
                                        </h5>

                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                        <div id="tab3" class="tab-pane fade in">
                            <ul>
                                {foreach from=$sessions[3] item=conductingVal}
                                    <li class="feeds-item ptb10 plr10">
                                        <h4 class="mt0 session_name">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                        </h4>
                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Initiated By : </label>
                                            {$conductingVal['created_by']['user_name']}
                                        </div>
                                        {if $conductingVal['presentors']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Sharer : </label>
                                                {$conductingVal['presentors']}
                                            </div>
                                        {/if}
                                        {if $conductingVal['total_attends']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Total Attendees : </label>
                                                {$conductingVal['total_attends']}
                                            </div>
                                        {/if}

                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Started At : </label>
                                            {$conductingVal['added_on']|date_format:"%e %B %Y"}
                                        </div>
                                        <div style="clear: both;"></div>
                                        <h5 class="session_about">
                                            <p class="text-muted">{$conductingVal['description']} </p>
                                        </h5>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                        <div id="tab4" class="tab-pane fade in">
                            <ul>
                                {foreach from=$sessions[4] item=conductingVal}
                                    <li class="feeds-item ptb10 plr10">
                                        <h4 class="mt0 session_name">
                                            <a href="{$system['system_url']}/session/{$conductingVal['sessions_id']}">{$conductingVal['title']}</a>
                                        </h4>
                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Initiated By : </label>
                                            {$conductingVal['created_by']['user_name']}
                                        </div>
                                        {if $conductingVal['presentors']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Sharer : </label>
                                                {$conductingVal['presentors']}
                                            </div>
                                        {/if}
                                        {if $conductingVal['total_attends']!=''}
                                            <div style="width: 50%; float: left;">
                                                <label>Total Attendees : </label>
                                                {$conductingVal['total_attends']}
                                            </div>
                                        {/if}

                                        <div class="text-muted" style="width: 50%; float: left;">
                                            <label>Started At : </label>
                                            {$conductingVal['added_on']|date_format:"%e %B %Y"}
                                        </div>
                                        <div style="clear: both;"></div>
                                        <h5 class="session_about">
                                            <p class="text-muted">{$conductingVal['description']} </p>
                                        </h5>
                                    </li>
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- conversation -->

        <!-- threads -->
        <div class="col-md-4 col-sm-12 offcanvas-sidebar_">

            <!-- suggested interests -->
            {if count($related_interest_arr) > 0}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {__("Suggested Interests")}
                    </div>
                    <div class="panel-body">
                        <div class="js_scroller" data-slimScroll-height="100%">
                            <ul class="related_interest_div">
                                {foreach $related_interest_arr as $related}
                                    <li>
                                        <div class="data-container">
                                            <a href="{$system['system_url']}/interest/{$related['interest_id']}">
                                                <img class="data-avatar" src="{$related['image']}" alt="test1">
                                            </a>
                                            <div class="data-content">
                                                <div class="pull-right flip">
                                                    {if $related['i_follow_related'] == 1}
                                                        <button type="button"
                                                                class="btn btn-sm js_unfollow-interest"
                                                                data-id="{$related['interest_id']}">
                                                            <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                            {__("Following")}
                                                        </button>
                                                    {else}
                                                        <button type="button"
                                                                class="btn btn-sm btn-success js_follow-interest"
                                                                data-id="{$related['interest_id']}">
                                                            <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                            {__("Follow Interest")}
                                                        </button>
                                                    {/if}
                                                </div>
                                                <div>
                                                <span class="name"> <a
                                                            href="{$system['system_url']}/interest/{$related['interest_id']}">{$related['text']}</a> </span>
                                                    <div>{if $related['total_follower'] > 0}
                                                            {$related['total_follower']} Followers
                                                        {/if}
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                {/foreach}

                            </ul>
                        </div>
                    </div>
                </div>
            {/if}
            <!-- suggested interest -->

            <!-- people you may know -->
            {if count($new_people) > 0}
                <div class="panel panel-default panel-widget">
                    <div class="panel-heading">
                        <div class="pull-right flip">
                            <small><a href="{$system['system_url']}/friends/requests">{__("See All")}</a></small>
                        </div>
                        <strong>{__("People you may know")}</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            {foreach $new_people as $_user}
                                {include file='__feeds_user.tpl' _connection="add" _small=true}
                            {/foreach}
                        </ul>
                    </div>
                </div>
            {/if}
            <!-- people you may know -->

            <!-- suggested pages -->
            {if count($new_pages) > 0}
                <div class="panel panel-default panel-widget">
                    <div class="panel-heading">
                        <strong>{__("Suggested Pages")}</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            {foreach $new_pages as $_page}
                                {include file='__feeds_page.tpl'}
                            {/foreach}
                        </ul>
                    </div>
                </div>
            {/if}
            <!-- suggested pages -->

            <!-- suggested groups -->
            {if count($new_groups) > 0}
                <div class="panel panel-default panel-widget">
                    <div class="panel-heading">
                        <strong>{__("Suggested Groups")}</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            {foreach $new_groups as $_group}
                                {include file='__feeds_group.tpl'}
                            {/foreach}
                        </ul>
                    </div>
                </div>
            {/if}
            <!-- suggested groups -->


        </div>
        <!-- threads -->

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}