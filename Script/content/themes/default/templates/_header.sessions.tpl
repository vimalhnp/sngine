<li class="dropdown js_live-sessions">
    <a href="{$system['system_url']}/all_sessions">
        <i class="fa fa-list-ul fa-lg"></i>
        <span class="label {if $user->_data['user_live_sessions_counter'] == 0}hidden{/if}">
            {$user->_data['user_live_sessions_counter']}
        </span>
    </a>
</li>