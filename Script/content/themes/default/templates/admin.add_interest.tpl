<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list-ol pr5 panel-icon"></i>
        <strong>{__("Add Interest")}</strong>
    </div>
    <div class="panel-body">

        <!-- Design -->
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="panel-body">
                    <form class="js_ajax-forms form-horizontal" data-url="admin/add_interest.php">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Parent Interest")}
                            </label>
                            <div class="col-sm-9">
                                <select name="parent_id" class="form-control">
                                    <option value="0">No Parent</option>
                                    {foreach $rows AS $row_key => $row_value}
                                        <option value="{$row_value['interest_id']}">{$row_value['text']}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Interest Name")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Interest Description")}
                            </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="col-sm-3 control-label text-left">
                                {__("Cover Photo")}
                            </label> 
                            <div class="col-sm-9">
                                <div class="x-image">
                                    <button type="button" class="close x-hidden js_x-image-remover" title='{__("Remove")}'>
                                        <span>×</span>
                                    </button>
                                    <div class="loader loader_small x-hidden"></div>
                                    <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                    <input type="hidden" class="js_x-image-input" name="image" value="">
                                </div>
                            </div> 
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">{__("Add Interest")}</button>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
        </div>
        <!-- Design -->
    </div>
</div>