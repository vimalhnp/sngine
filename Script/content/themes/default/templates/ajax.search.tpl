<div class="js_scroller">
    <ul>
        {foreach $results as $result}
            {if $result['type'] == "user"}
                {include file='__feeds_user.tpl' _user=$result _connection=$result['connection']}

            {elseif $result['type'] == "page"}
                {include file='__feeds_page.tpl' _page=$result}

            {elseif $result['type'] == "group"}
                {include file='__feeds_group.tpl' _group=$result}

            {elseif $result['type'] == "interest"}
                {*{include file='__feeds_interest.tpl' _interest=$result}*}
                <li class="feeds-item">
                    <div class="data-container">
                        <a href="{$system['system_url']}/interest/{$result['interest_id']}">
                            <img class="data-avatar" src="{$result['image']}" alt="test1">
                        </a>
                        <div class="data-content">
                            <div class="pull-right flip">
                                {if $result['i_follow_related'] == 1}
                                    <button type="button" class="btn btn-sm js_unfollow-interest"
                                            data-id="{$result['interest_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                        {__("Following")}
                                    </button>
                                {else}
                                    <button type="button" class="btn btn-sm btn-success js_follow-interest"
                                            data-id="{$result['interest_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                        {__("Follow Interest")}
                                    </button>
                                {/if}
                            </div>
                            <div>
                                    <span class="name"> <a
                                                href="{$system['system_url']}/interest/{$result['interest_id']}">{$result['text']}</a> </span>
                                <div>{if $result['total_follower'] > 0}
                                        {$result['total_follower']} Followers
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            {/if}
        {/foreach}
    </ul>
</div>