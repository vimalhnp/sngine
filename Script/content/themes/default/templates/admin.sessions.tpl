<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list-ol pr5 panel-icon"></i>
        <strong>{__("Activate Sessions")}</strong>
    </div>
    <div class="panel-body">

        <!-- Design -->
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
        <div class="form-group row">
            <label class="col-sm-12">
                {__("Need to verify below Sessions")}
            </label>
            <div class="col-sm-12">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                                <tr>
                                    <th>{__("ID")}</th>
                                    <th>{__("Title")}</th>
                                    <th>{__("Created By")}</th>
                                    <th>{__("Location")}</th>
                                    <th>{__("Venue")}</th>
                                    <th>{__("Added Date")}</th>
                                    <th>{__("Actions")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $rows as $row}
                                    <tr>
                                        <td>{$row['sessions_id']}</td>
                                        <td>{$row['title']}</td>
                                        <td>{$row['created_by']['user_name']}</td>
                                        <td>{$row['location']}</td>
                                        <td>{$row['venue']}</td>
                                        <td>{$row['added_on']|date_format:"%e %B %Y"}</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='{__("Not approve")}' class="btn btn-xs btn-danger js_admin-session-decline" data-handle="session" data-id="{$row['sessions_id']}">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            <a data-toggle="tooltip" data-placement="top" title='{__("Approve")}' class="btn btn-xs btn-success js_admin-verify" data-handle="session" data-id="{$row['sessions_id']}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        <div class="divider"></div>
        <div class="form-group row">
            <label class="col-sm-12">
                {__("Active Sessions")}
            </label>
            <div class="col-sm-12">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                                <tr>
                                    <th>{__("ID")}</th>
                                    <th>{__("Title")}</th>
                                    <th>{__("Created By")}</th>
                                    <th>{__("Location")}</th>
                                    <th>{__("Venue")}</th>
                                    <th>{__("Added Date")}</th>
{*                                    <th>{__("Actions")}</th>*}
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $rows_active as $row}
                                    <tr>
                                        <td>{$row['sessions_id']}</td>
                                        <td>{$row['title']}</td>
                                        <td>{$row['created_by']['user_name']}</td>
                                        <td>{$row['location']}</td>
                                        <td>{$row['venue']}</td>
                                        <td>{$row['added_on']|date_format:"%e %B %Y"}</td>
                                        {*<td>
                                            <button data-toggle="tooltip" data-placement="top" title='{__("Not approve")}' class="btn btn-xs btn-danger js_admin-session-decline" data-handle="session" data-id="{$row['sessions_id']}">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            <a data-toggle="tooltip" data-placement="top" title='{__("Approve")}' class="btn btn-xs btn-success js_admin-verify" data-handle="session" data-id="{$row['sessions_id']}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </td>*}
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- Design -->
    </div>
</div>