{include file='_head.tpl'}
{include file='_header.tpl'}

{if !$user->_logged_in}
    <!-- page content -->
    <div class="index-wrapper" {if !$system['system_wallpaper_default'] && $system['system_wallpaper']} style="background-image: url('{$system['system_uploads']}/{$system['system_wallpaper']}')" {/if}>
        <div class="container">
            <div class="index-intro">
                <h1>
                    {__("Welcome to")} {$system['system_title']}
                </h1>
                <p>
                    {__("Share your memories, connect with others, make new friends")}
                </p>
            </div>

            <div class="row relative">

                {*{if $random_users}
                <!-- random 4 users -->
                {foreach $random_users as $random_user}
                <a href="{$system['system_url']}/{$random_user['user_name']}" class="fly-head" 
                {if $random_user@index == 0} style="top: 140px;left: 50px;" {/if}
                {if $random_user@index == 1} style="bottom: 60px;left: 210px;" {/if}
                {if $random_user@index == 2} style="top: 140px;right: 50px;" {/if}
                {if $random_user@index == 3} style="bottom: 60px;right: 210px;" {/if}
                data-toggle="tooltip" data-placement="top" title="{$random_user['user_fullname']}">
                <img src="{$random_user['user_picture']}">
                </a>
                {/foreach}
                <!-- random 4 users -->
                {/if}*}

                <div class="col-xs-12 col-sm-7">
                    <div style="margin-top: 20px; height: 100%; background-color: black;">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/Nj6-Fn3UplQ?ecver=1" frameborder="0"></iframe>
                    </div>
                </div>
                <!-- sign in/up form -->
                {include file='_sign_form.tpl'}
                <!-- sign in/up form -->

            </div>
        </div>
    </div>

    {if $system['directory_enabled']}
        {include file='_directory.tpl'}
    {/if}
    <!-- page content -->
{else}
    <!-- page content -->
    <div class="container mt20 offcanvas">
        <div class="row">

            <!-- left panel -->
            <div class="col-sm-4 col-md-2 offcanvas-sidebar">
                {include file='_sidebar.tpl'}
            </div>
            <!-- left panel -->

            <div class="col-sm-8 col-md-10 offcanvas-mainbar">
                <div class="row">
                    <!-- center panel -->
                    <div class="col-sm-12 col-md-7">
                        <!-- announcments -->
                        {include file='_announcements.tpl'}
                        <!-- announcments -->

                        {if $view == ""}
                            <!-- publisher -->
                            {include file='_publisher.tpl' _handle="me" _privacy=true}
                            <!-- publisher -->

                            <!-- boosted post -->
                            {if $boosted_post}
                                {include file='_boosted_post.tpl' post=$boosted_post}
                            {/if}
                            <!-- boosted post -->

                            <!-- posts stream -->
                            {include file='_posts.tpl' _get="newsfeed"}
                            <!-- posts stream -->

                        {elseif $view == "saved"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="saved" _title=__("Saved Posts")}
                            <!-- saved posts stream -->

                        {elseif $view == "products"}
                            <!-- saved posts stream -->
                            {include file='_posts.tpl' _get="posts_profile" _id=$user->_data['user_id'] _filter="product" _title=__("My Products")}
                            <!-- saved posts stream -->

                        {elseif $view == "pages"}
                            <div class="panel panel-default">
                                <div class="panel-heading light clearfix">
                                    <div class="pull-right flip">
                                        <a href="{$system['system_url']}/create/page" class="btn btn-default btn-sm">
                                            <i class="fa fa-flag fa-fw pr10"></i> {__("Create Page")}
                                        </a>
                                    </div>
                                    <div class="mt5">
                                        <strong>{__("Your Pages")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    {if count($user->_data['pages']) > 0}
                                        <ul>
                                            {foreach $user->_data['pages'] as $_page}
                                                {include file='__feeds_page.tpl'}
                                            {/foreach}
                                        </ul>
                                    {else}
                                        <p class="text-center text-muted">
                                            {__("No pages available")}
                                        </p>
                                    {/if}

                                    <!-- see-more -->
                                    {if count($user->_data['pages']) >= $system['max_results']}
                                        <div class="alert alert-info see-more js_see-more" data-get="pages">
                                            <span>{__("See More")}</span>
                                            <div class="loader loader_small x-hidden"></div>
                                        </div>
                                    {/if}
                                    <!-- see-more -->

                                </div>
                            </div>

                        {elseif $view == "groups"}
                            <div class="panel panel-default">
                                <div class="panel-heading light clearfix">
                                    <div class="pull-right flip">
                                        <a href="{$system['system_url']}/create/group" class="btn btn-default btn-sm">
                                            <i class="fa fa-flag fa-fw pr10"></i> {__("Create Group")}
                                        </a>
                                    </div>
                                    <div class="mt5">
                                        <strong>{__("Your Groups")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    {if count($user->_data['groups']) > 0}
                                        <ul>
                                            {foreach $user->_data['groups'] as $_group}
                                                {include file='__feeds_group.tpl'}
                                            {/foreach}
                                        </ul>
                                    {else}
                                        <p class="text-center text-muted">
                                            {__("No groups available")}
                                        </p>
                                    {/if}

                                    <!-- see-more -->
                                    {if count($user->_data['groups']) >= $system['max_results']}
                                        <div class="alert alert-info see-more js_see-more" data-get="groups">
                                            <span>{__("See More")}</span>
                                            <div class="loader loader_small x-hidden"></div>
                                        </div>
                                    {/if}
                                    <!-- see-more -->

                                </div>
                            </div>

                        {elseif $view == "create_page"}
                            <!-- create page -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="mt5">
                                        <strong>{__("Create Page")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="js_ajax-forms" data-url="pages_groups/create.php?type=page&do=create">
                                        <div class="form-group">
                                            <label for="category">{__("Category")}:</label>
                                            <select class="form-control" name="category" id="category">
                                                {foreach $categories as $category}
                                                    <option value="{$category['category_id']}">{$category['category_name']}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">{__("Title")}:</label>
                                            <input type="text" class="form-control" name="title" id="title" placeholder='{__("Title of your page")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">{__("Username")}:</label>
                                            <input type="text" class="form-control" name="username" id="username" placeholder='{__("Username, e.g. YouTubeOfficial")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{__("Description")}:</label>
                                            <textarea class="form-control" name="description" name="description"  placeholder='{__("Write about your page...")}'></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">{__("Create")}</button>

                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                    </form>
                                </div>
                            </div>
                            <!-- create page -->

                        {elseif $view == "create_group"}
                            <!-- create group -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="mt5">
                                        <strong>{__("Create Group")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="js_ajax-forms" data-url="pages_groups/create.php?type=group&do=create">
                                        <div class="form-group">
                                            <label for="title">{__("Title")}:</label>
                                            <input type="text" class="form-control" name="title" id="title" placeholder='{__("Title of your group")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">{__("Username")}:</label>
                                            <input type="text" class="form-control" name="username" id="username" placeholder='{__("Username, e.g. DevelopersGroup")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{__("Description")}:</label>
                                            <textarea class="form-control" name="description" placeholder='{__("Write about your group...")}'></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">{__("Create")}</button>

                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                    </form>
                                </div>
                            </div>
                            <!-- create group -->
                        {elseif $view == "create_session"}
                            <!-- create session -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="mt5">
                                        <strong>{__("Request for a Session")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="js_ajax-forms form-horizontal" data-url="pages_groups/create.php?type=session&do=create">
                                        <div class="form-group">
                                            <label for="title">{__("Title")}:</label>
                                            <input type="text" class="form-control" data-placement="bottom" data-toggle="tooltip" title="Enter Title of your session" name="title" id="title" placeholder='{__("Title of your session")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{__("Description")}:</label>
                                            <textarea class="form-control" name="description" data-placement="bottom" data-toggle="tooltip" title="Enter about your session" placeholder='{__("Write about your session...")}'></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label text-left">
                                                {__("Cover Photo")}
                                            </label>
                                            <div class="col-sm-9">
                                                {if $system['cover_photo'] == ''}
                                                    <div class="x-image">
                                                        <button type="button" class="close x-hidden js_x-image-remover" title='{__("Remove")}'>
                                                            <span>×</span>
                                                        </button>
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input" name="cover_photo" value="">
                                                    </div>
                                                {else}
                                                    <div class="x-image" style="background-image: url('{$system['system_uploads']}/{$system['cover_photo']}')">
                                                        <button type="button" class="close js_x-image-remover" title='{__("Remove")}'>
                                                            <span>×</span>
                                                        </button>
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input" name="cover_photo" value="{$system['cover_photo']}">
                                                    </div>
                                                {/if}

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">
                                                {__("Select Interest")}
                                            </label>
                                            <div class="col-sm-12">
                                                {*<select id="interest_dropdown" multiple="" name="interest_dropdown[]">
                                                <optgroup label="Group 1" class="group-1">
                                                <option value="1-1">Option 1.1</option>
                                                <option value="1-2">Option 1.2</option>
                                                <option value="1-3">Option 1.3</option>
                                                </optgroup>
                                                <optgroup label="Group 2" class="group-2">
                                                <option value="2-1">Option 2.1</option>
                                                <option value="2-2">Option 2.2</option>
                                                <option value="2-3">Option 2.3</option>
                                                </optgroup>
                                                </select>*}
                                                <select id="test-select-2" name="interest_dropdown[]">
                                                    {$interest_html}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="location">{__("Location")}:</label>
                                            <input type="text" class="form-control" name="location" id="location" data-placement="bottom" data-toggle="tooltip" title="Location of your session, Ex: your district" placeholder='{__("Location of your session, Ex: your district.")}'>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="{__("Create")}">

                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                        <!-- success -->
                                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- success -->
                                    </form>
                                </div>
                            </div>
                            <!-- Manage session -->
                        {elseif $view == "conduct_session"}
                            <!-- create session -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="mt5">
                                        <strong>{__("Conduct a Session")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form class="js_ajax-forms form-horizontal" data-url="pages_groups/create.php?type=session&do=conduct">
                                        <div class="form-group">
                                            <label for="title">{__("Title")}:</label>
                                            <input type="text" class="form-control" name="title" id="title" data-placement="bottom" data-toggle="tooltip" title="Enter Title of your session" placeholder='{__("Title of your session")}'>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{__("Description")}:</label>
                                            <textarea class="form-control" name="description" data-placement="bottom" data-toggle="tooltip" title="Enter about your session" placeholder='{__("Write about your session...")}'></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label text-left">
                                                {__("Cover Photo")}
                                            </label>
                                            <div class="col-sm-9">
                                                {if $system['cover_photo'] == ''}
                                                    <div class="x-image">
                                                        <button type="button" class="close x-hidden js_x-image-remover" title='{__("Remove")}'>
                                                            <span>×</span>
                                                        </button>
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input" name="cover_photo" value="">
                                                    </div>
                                                {else}
                                                    <div class="x-image" style="background-image: url('{$system['system_uploads']}/{$system['cover_photo']}')">
                                                        <button type="button" class="close js_x-image-remover" title='{__("Remove")}'>
                                                            <span>×</span>
                                                        </button>
                                                        <div class="loader loader_small x-hidden"></div>
                                                        <i class="fa fa-camera fa-lg js_x-uploader" data-handle="x-image"></i>
                                                        <input type="hidden" class="js_x-image-input" name="cover_photo" value="{$system['cover_photo']}">
                                                    </div>
                                                {/if}

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-12">
                                                {__("Select Interest")}
                                            </label>
                                            <div class="col-sm-12">
                                                <select id="test-select-2" name="interest_dropdown[]">
                                                    {$interest_html}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="location">{__("Location")}:</label>
                                            <input type="text" class="form-control" name="location" id="location" data-placement="bottom" data-toggle="tooltip" title="Location of your session, Ex: your district" placeholder='{__("Location of your session, Ex: your district.")}'>
                                        </div>
                                        <div class="form-group">
                                            <label>{__("Dummy question 1")}</label>
                                            <input name="ans1" type="text" value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{__("Dummy question 2")}</label>
                                            <input name="ans2" type="text" value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{__("Dummy question 3")}</label>
                                            <input name="ans3" type="text" value="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>{__("Dummy question 4")}</label>
                                            <input name="ans4" type="text" value="" class="form-control">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="{__("Create")}">

                                        <!-- error -->
                                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- error -->
                                        <!-- success -->
                                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                        <!-- success -->
                                    </form>
                                </div>
                            </div>
                            <!-- Manage session -->
                        {elseif $view == "session_list"}
                            <!-- create session -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="mt5">
                                        <strong>{__("Active Sessions")}</strong>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover js_dataTable">
                                            <thead>
                                                <tr>
                                                    <th>{__("ID")}</th>
                                                    <th>{__("Title")}</th>
                                                    <th>{__("Created By")}</th>
                                                    <th>{__("Location")}</th>
                                                    <th>{__("Venue")}</th>
                                                    <th>{__("Added Date")}</th>
                                                        {*                                    <th>{__("Actions")}</th>*}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {foreach $rows_active as $row}
                                                    <tr>
                                                        <td>{$row['sessions_id']}</td>
                                                        <td>{$row['title']}</td>
                                                        <td>{$row['created_by']['user_name']}</td>
                                                        <td>{$row['location']}</td>
                                                        <td>{$row['venue']}</td>
                                                        <td>{$row['added_on']|date_format:"%e %B %Y"}</td>
                                                        {*<td>
                                                        <button data-toggle="tooltip" data-placement="top" title='{__("Not approve")}' class="btn btn-xs btn-danger js_admin-session-decline" data-handle="session" data-id="{$row['sessions_id']}">
                                                        <i class="fa fa-close"></i>
                                                        </button>
                                                        <a data-toggle="tooltip" data-placement="top" title='{__("Approve")}' class="btn btn-xs btn-success js_admin-verify" data-handle="session" data-id="{$row['sessions_id']}">
                                                        <i class="fa fa-check"></i>
                                                        </a>
                                                        </td>*}
                                                    </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- create session -->
                        {elseif $view == "all_sessions"}

                            <!-- saved posts stream -->
                            {include file='_session_posts.tpl' _get="posts" _id=$user->_data['user_id'] _filter="product" _title=__("Sessions")}
                            <!-- saved posts stream -->

                        {elseif $view == "interest"}

                            <!-- saved posts stream -->
                            {include file='_session_interest.tpl' _get="posts" _id=$user->_data['user_id'] _filter="product" _title=__("Sessions")}
                            <!-- saved posts stream -->

                        {elseif $view == "my_interest"}

                            <div class="row">
                                <div class="col-sm-8">
                                    <h3>My Interest</h3>
                                </div>
                                <div class="col-sm-4">
                                    <input type="button" value="Add Interest" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">
                                </div>
                            </div>

                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Submit Interest Request</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="js_ajax-forms form-horizontal" data-url="users/send_interest_request.php" onsubmit="$('#interest_success').show();">
                                                <div>
                                                    <input type="text" class="form-control" name="interest_name" placeholder="Interest Name" required="">
                                                </div>
                                                <div style="margin-top: 10px;">
                                                    <input type="text" class="form-control" name="parent_interest_name" placeholder="Interest Parent Name" required="">
                                                </div>
                                                <div style="margin-top: 10px;">
                                                    <input type="submit" value="Submit" class="btn btn-info">
                                                    <label style="color: green; display: none;" id="interest_success">Interest Request Submitted Successfully</label>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <form method="POST" action="update_interest.php">
                                <select id="test-select-2" name="interest_dropdown[]">
                                    {$html}
                                </select>

                                <div style="margin-top: 10px;">
                                    <input type="submit" value="Update" class="btn btn-info">
                                </div>
                            </form>

                        {elseif $view == "games"}
                            <div class="panel panel-default">
                                <div class="panel-heading light clearfix">
                                    <div class="mt5">
                                        <strong>{__("Games")}</strong>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    {if count($games) > 0}
                                        <ul>
                                            {foreach $games as $_game}
                                                {include file='__feeds_game.tpl'}
                                            {/foreach}
                                        </ul>
                                    {else}
                                        <p class="text-center text-muted">
                                            {__("No games available")}
                                        </p>
                                    {/if}

                                    <!-- see-more -->
                                    {if count($games) >= $system['max_results']}
                                        <div class="alert alert-info see-more js_see-more" data-get="games">
                                            <span>{__("See More")}</span>
                                            <div class="loader loader_small x-hidden"></div>
                                        </div>
                                    {/if}
                                    <!-- see-more -->

                                </div>
                            </div>

                        {/if}
                    </div>
                    <!-- center panel -->

                    <!-- right panel -->
                    <div class="col-sm-12 col-md-5">
                        <!-- pro members -->
                        {if count($pro_members) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <div class="pull-right flip">
                                            <small><a href="{$system['system_url']}/packages">{__("Upgrade to Pro")}</a></small>
                                        </div>
                                    {/if}
                                    <strong class="text-primary"><i class="fa fa-bolt fa-fw"></i> {__("Pro Members")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $pro_members as $_member}
                                            <div class="col-xs-3 col-sm-4">
                                                <a class="friend-picture" href="{$system['system_url']}/{$_member['user_name']}" style="background-image:url({$_member['user_picture']});" >
                                                    <span class="friend-name">{$_member['user_fullname']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <!-- pro members -->


                        <!-- boosted pages -->
                        {if count($promoted_pages) > 0}
                            <div class="panel panel-default panel-friends">
                                <div class="panel-heading">
                                    {if $system['packages_enabled'] && !$user->_data['user_subscribed']}
                                        <div class="pull-right flip">
                                            <small><a href="{$system['system_url']}/packages">{__("Upgrade to Pro")}</a></small>
                                        </div>
                                    {/if}
                                    <strong class="text-primary"><i class="fa fa-bullhorn fa-fw"></i> {__("Promoted Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        {foreach $promoted_pages as $_page}
                                            <div class="col-xs-3 col-sm-4">
                                                <a class="friend-picture" href="{$system['system_url']}/pages/{$_page['page_name']}" style="background-image:url({$_page['page_picture']});" >
                                                    <span class="friend-name">{$_page['page_title']}</span>
                                                </a>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <!-- boosted pages -->

                        {include file='_ads.tpl'}
                        {include file='_widget.tpl'}

                        <!-- people you may know -->
                        {if count($new_people) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <div class="pull-right flip">
                                        <small><a href="{$system['system_url']}/friends/requests">{__("See All")}</a></small>
                                    </div>
                                    <strong>{__("People you may know")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_people as $_user}
                                            {include file='__feeds_user.tpl' _connection="add" _small=true}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}
                        <!-- people you may know -->

                        <!-- suggested pages -->
                        {if count($new_pages) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong>{__("Suggested Pages")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_pages as $_page}
                                            {include file='__feeds_page.tpl'}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}
                        <!-- suggested pages -->

                        <!-- suggested groups -->
                        {if count($new_groups) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong>{__("Suggested Groups")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_groups as $_group}
                                            {include file='__feeds_group.tpl'}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}
                        <!-- suggested groups -->

                        <!-- suggested sessions -->
                        {if count($new_sessions) > 0}
                            <div class="panel panel-default panel-widget">
                                <div class="panel-heading">
                                    <strong>{__("Suggested Sessions")}</strong>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        {foreach $new_sessions as $_session}
                                            {include file='__feeds_session.tpl'}
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        {/if}
                        <!-- suggested sessions -->

                        <!-- mini footer -->
                        {if ($view == "" || $view == "saved") && (count($new_people) > 0 || count($new_pages) > 0 || count($new_groups) > 0)}
                            <div class="row plr10 hidden-xs">
                                <div class="col-xs-12 mb5">
                                    {if count($static_pages) > 0}
                                        {foreach $static_pages as $static_page}
                                            <a href="{$system['system_url']}/static/{$static_page['page_url']}">
                                                {$static_page['page_title']}
                                            </a>{if !$static_page@last} · {/if}
                                        {/foreach}
                                    {/if}
                                    {if $system['contact_enabled']}
                                        · 
                                        <a href="{$system['system_url']}/contacts">
                                            {__("Contacts")}
                                        </a>
                                    {/if}
                                    {if $system['directory_enabled']}
                                        · 
                                        <a href="{$system['system_url']}/directory">
                                            {__("Directory")}
                                        </a>
                                    {/if}
                                    {if $system['market_enabled']}
                                        · 
                                        <a href="{$system['system_url']}/market">
                                            {__("Market")}
                                        </a>
                                    {/if}
                                    {if $system['system_public']}
                                        · 
                                        <a href="{$system['system_url']}/search">
                                            {__("Search")}
                                        </a>
                                    {/if}
                                </div>
                                <div class="col-xs-12">
                                    &copy; {'Y'|date} {$system['system_title']} · <span class="text-link" data-toggle="modal" data-url="#translator">{$system['language']['title']}</span>
                                </div>
                            </div>
                        {/if}
                        <!-- mini footer -->
                    </div>
                    <!-- right panel -->
                </div>
            </div>

        </div>
    </div>
    <!-- page content -->
{/if}

{include file='_footer.tpl'}