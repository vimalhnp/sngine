<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list-ol pr5 panel-icon"></i>
        <strong>{__("Assign Sessions")}</strong>
    </div>
    <div class="panel-body">

        <!-- Design -->
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
        <div class="form-group row">
            <label class="col-sm-12">
                {__("Need to verify below Sessions")}
            </label>
            <div class="col-sm-12">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                                <tr>
                                    <th>{__("ID")}</th>
                                    <th>{__("Name")}</th>
                                    <th>{__("Sharer")}</th>
                                    <th>{__("Title")}</th>
                                    {*<th>{__("Date")}</th>
                                    <th>{__("Time")}</th>
                                    <th>{__("Venue")}</th>*}
                                    <th>{__("Que/Ans")}</th>
                                    <th>{__("Accept Date")}</th>
                                    <th>{__("Actions")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $rows as $row}
                                    <tr>
                                        <td>{$row['sessions_presentor_id']}</td>
                                        <td><a href="{$system['system_url']}/session/{$row['sessions_data']['sessions_id']}" target="_blank">{$row['sessions_data']['title']}</a></td>
                                        <td>{$row['created_by']['user_name']}</td>
                                        <td>{$row['title']}</td>
                                        {*<td>{$row['date']}</td>
                                        <td>{$row['time']}</td>
                                        <td>{$row['venuw']}</td>*}
                                        <td>{$row['questionHtml']}</td>
                                        <td>{$row['added_on']|date_format:"%e %B %Y"}</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='{__("Not Assign")}' class="btn btn-xs btn-danger js_admin-decline_session" data-handle="session_unassign" data-id="{$row['sessions_presentor_id']}">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            <a data-toggle="tooltip" data-placement="top" title='{__("Assign")}' class="btn btn-xs btn-success js_admin-assign_session" data-handle="session_assign" data-id="{$row['sessions_presentor_id']}" data-sharerid="{$row['user_id']}" data-sessionid="{$row['sessions_id']}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- Design -->
    </div>
</div>