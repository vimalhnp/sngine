<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list-ol pr5 panel-icon"></i>
        <strong>{__("Add Venue")}</strong>
    </div>
    <div class="panel-body">

        <!-- Design -->
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="panel-body">
                    <form class="js_ajax-forms form-horizontal" data-url="admin/add_venue.php">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Venue Name")}
                            </label>
                            <div class="col-sm-9">
                                <input class="form-control" name="venue_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-left">
                                {__("Institute")}
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="institute_id">
                                    {foreach $institutes as $ii}
                                        <option value="{$ii['institute_id']}">{$ii['name']}</option>
                                    {/foreach}

                                </select>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="col-sm-3 control-label text-left">
                                {__("Description")}
                            </label> 
                            <div class="col-sm-9">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="col-sm-3 control-label text-left">
                                {__("Courses Offered")}
                            </label> 
                            <div class="col-sm-9">
                                <textarea class="form-control" name="courses_offered"></textarea>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <label class="col-sm-3 control-label text-left">
                                {__("Team(s)")}
                            </label> 
                            <div class="col-sm-9">
                                <input class="form-control" name="team_name[]">
                                <input class="form-control" name="team_role[]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">{__("Add Venue")}</button>
                            </div>
                        </div>

                        <!-- success -->
                        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                        <!-- success -->

                        <!-- error -->
                        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                        <!-- error -->
                    </form>
                </div>
            </div>
        </div>
        <!-- Design -->
    </div>
</div>