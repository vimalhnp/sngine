{include file='_head.tpl'}
{include file='_header.tpl'}

<!-- page content -->
<div class="container offcanvas">
    <div class="row">

        <!-- side panel -->
        <div class="col-xs-12 visible-xs-block offcanvas-sidebar mt20">
            {include file='_sidebar.tpl'}
        </div>
        <!-- side panel -->

        <div class="col-xs-12 offcanvas-mainbar">
            <!-- profile-header -->
            <div class="profile-header">
                <!-- profile-cover -->
                <div class="profile-cover-wrapper" {if $session_data['page_picture']} style="background-image:url('{$session_data['page_picture']}');" {/if}>
                    {if $user->_logged_in}
                        <div class="profile-cover-change-loader">
                            <div class="loader loader_large"></div>
                        </div>
                    {/if}
                </div>
                <!-- profile-cover -->

                <!-- profile-name -->
                <div class="profile-name-wrapper_new">
                    <a href="{$system['system_url']}/session/{$session_data['sessions_id']}">{$session_data['title']}</a>
                    {if $session_data['page_verified']}
                        <i data-toggle="tooltip" data-placement="top" title='{__("Verified Page")}' class="fa fa-check-circle fa-fw verified-badge"></i>
                    {/if}
                </div>
                <!-- profile-name -->

                <!-- profile-buttons -->
                <div class="profile-buttons-wrapper">
                    {if !$session_data['event_expired']}
                        {if $user->_logged_in && $session_data['i_attend']}
                            <button type="button" class="btn btn-default js_unattend-page" data-id="{$session_data['sessions_id']}">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Attending")}
                            </button>
                        {else}
                            <button type="button" class="btn btn-primary js_attend-page" data-id="{$session_data['sessions_id']}">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Attend")}
                            </button>
                        {/if}
                    {else}
                        {if $user->_logged_in && $session_data['i_join']}
                            <button type="button" class="btn btn-default js_unjoin-page" data-id="{$session_data['sessions_id']}">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Joined")}
                            </button>
                        {else}
                            <button type="button" class="btn btn-primary js_join-page" data-id="{$session_data['sessions_id']}">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Join")}
                            </button>
                        {/if}
                    {/if}

                    {if $user->_logged_in && $session_data['i_accept']}
                        {*<a href="" class="btn btn-default" data-toggle="modal" data-url="session/accept_editor.php?sessions_id={$session_data['sessions_id']}">
                        <div class="action no-desc">
                        <i class="fa fa-user-o"></i> {__("Decline")}
                        </div>
                        </a>*}
                    {else}
                        <a href="" class="btn btn-default" data-toggle="modal" data-url="session/accept_editor.php?sessions_id={$session_data['sessions_id']}">
                            <div class="action no-desc">
                                <i class="fa fa-user-o"></i> {__("Accept")}
                            </div>
                        </a>
                    {/if}

                    {if $user->_logged_in && $session_data['status'] == 4}
                        {if $session_data['i_thanks'] == 1}
                            <button type="button" class="btn btn-default" data-id="{$session_data['sessions_id']}" disabled="disabled">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Thank You")}
                            </button>
                        {else}
                            <button type="button" class="btn btn-primary js_thanks-page" data-id="{$session_data['sessions_id']}">
                                <i class="fa fa-thumbs-o-up"></i>
                                {__("Thanks")}
                            </button>
                        {/if}
                    {/if}

                    <a href="javascript:void(0);" class="btn btn-info" id="share_session_btn">
                        <div class="action no-desc" onclick="openShareSessionDiv();">
                            <i class="fa fa-share-alt"></i> {__("Share")}
                        </div>
                    </a>
                    <div id="share_session_div" class="share_session_div" style="display: none;">
                        <a href="http://www.facebook.com/sharer.php?u={$system['system_url']}/session/{$session_data['sessions_id']}" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/intent/tweet?url={$system['system_url']}/session/{$session_data['sessions_id']}&amp;text={$session_data['title']}" target="_blank"><i class="fa fa-twitter"></i></a>
                        <i class="fa fa-newspaper-o" onclick="shareSession('{$system['system_url']}/session/{$session_data['sessions_id']}')"></i>
                    </div>
                </div>
                <!-- profile-buttons -->

                <!-- profile-tabs -->
                <div class="profile-tabs-wrapper" style="padding-left: 0!important;">
                    <ul class="nav">
                        <li>
                            <a href="{$system['system_url']}/session/{$session_data['sessions_id']}">
                                {__("Details")}
                            </a>
                        </li>
                        {if $user->_logged_in && $session_data['status'] == 4}
                            <li>
                                <a href="{$system['system_url']}/session/{$session_data['sessions_id']}/photos">
                                    {__("Photos")} 
                                </a>
                            </li>
                        {/if}
                        {if !$session_data['event_expired']}
                            <li>
                                <a href="{$system['system_url']}/session/{$session_data['sessions_id']}/discussion">
                                    {__("Discussion")} 
                                </a>
                            </li>
                        {else}
                            <li>
                                <a href="{$system['system_url']}/session/{$session_data['sessions_id']}/comments">
                                    {__("Comments")} 
                                </a>
                            </li>
                            <li>
                                <a href="{$system['system_url']}/session/{$session_data['sessions_id']}/queans">
                                    {__("Questionnaires")} 
                                </a>
                            </li>
                        {/if}
                    </ul>
                </div>
                <!-- profile-tabs -->
            </div>
            <!-- profile-header -->

            <!-- profile-content -->
            <div class="row">
                <!-- profile-buttons alt -->
                <div class="col-sm-12">
                    <div class="panel panel-default profile-buttons-wrapper-alt">
                        <div class="panel-body">
                            {if !$session_data['event_expired']}
                                {if $user->_logged_in && $session_data['i_attend']}
                                    <button type="button" class="btn btn-default js_unattend-page" data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Attending")}
                                    </button>
                                {else}
                                    <button type="button" class="btn btn-primary js_attend-page" data-id="{$session_data['sessions_id']}">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        {__("Attend")}
                                    </button>
                                {/if}
                            {/if}
                            {if $user->_logged_in && $user->_data['user_id'] == $session_data['page_admin']}
                                {if $user->_data['can_boost_pages']}
                                    {if $session_data['page_boosted']}
                                        <button type="button" class="btn btn-default js_unboost-page" data-id="{$session_data['sessions_id']}">
                                            <i class="fa fa-bolt"></i>
                                            {__("Unboost")}
                                        </button>
                                    {else}
                                        <button type="button" class="btn btn-danger js_boost-page" data-id="{$session_data['sessions_id']}">
                                            <i class="fa fa-bolt"></i>
                                            {__("Boost")}
                                        </button>
                                    {/if}
                                {else}
                                    <a href="{$system['system_url']}/packages" class="btn btn-danger">
                                        <i class="fa fa-bolt"></i>
                                        {__("Boost Page")}
                                    </a>
                                {/if}
                                <a href="{$system['system_url']}/pages/{$session_data['page_name']}/settings" class="btn btn-default">
                                    <i class="fa fa-pencil"></i> {__("Update Info")}
                                </a>
                            {else}
                                <button class="btn btn-default js_report-page" data-id="{$session_data['sessions_id']}">
                                    <i class="fa fa-flag"></i> {__("Report")}
                                </button>
                            {/if}
                        </div>
                    </div>
                </div>
                <!-- profile-buttons alt -->

                <!-- view content -->
                {if $view == "single_session"}
                    <div class="col-sm-12">
                        <!-- about -->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <ul class="about-list">
                                    <!-- category -->
                                    {$session_data['addEditVenue']}
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-user fa-fw fa-lg"></i>
                                            <label>Initiated By : </label>
                                            {$session_data['created_by']['user_name']}
                                        </div>
                                    </li>
                                    <!-- category -->
                                    <!-- description -->
                                    {if $session_data['description']}
                                        <li>
                                            <div class="about-list-item">
                                                <i class="fa fa-info-circle fa-fw fa-lg"></i>
                                                <label>About : </label>
                                                {$session_data['description']}
                                            </div>
                                        </li>
                                    {/if}
                                    <!-- description -->
                                    <!-- category -->
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-plus fa-fw fa-lg"></i>
                                            <label>Total Attendees : </label>
                                            {$session_data['total_attend']}
                                        </div>
                                    </li>
                                    {if $session_data['session_attend_friends'] != ""}
                                        <li class="session_attend_friends_label_li">
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>Friends Attend Session : </label>
                                            </div>
                                        </li>
                                        <li class="session_attend_friends_main_li">
                                            <div class="about-list-item">
                                                {$session_data['session_attend_friends']}
                                            </div>
                                        </li>
                                    {else}
                                        <li>
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>Friends Attend Session : </label>
                                            </div>
                                        </li>
                                    {/if}
                                    {if $session_data['session_attend_friends'] != ""}
                                        <li class="session_attend_friends_label_li">
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>People Attended : </label>
                                            </div>
                                        </li>
                                        <li class="session_attend_friends_main_li">
                                            <div class="about-list-item">
                                                {$session_data['session_attend_peoples']}
                                            </div>
                                        </li>
                                    {else}
                                        <li>
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>People Attended : </label>
                                            </div>
                                        </li>
                                    {/if}
                                    {if $session_data['session_engaged_peoples'] != ""}
                                        <li class="session_attend_friends_label_li">
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>People Engaged in Discussion : </label>
                                            </div>
                                        </li>
                                        <li class="session_attend_friends_main_li">
                                            <div class="about-list-item">
                                                {$session_data['session_engaged_peoples']}
                                            </div>
                                        </li>
                                    {else}
                                        <li>
                                            <div class="about-list-item">
                                                <i class="fa fa-user fa-fw fa-lg"></i>
                                                <label>People Engaged in Discussion : </label>
                                            </div>
                                        </li>
                                    {/if}
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-users fa-fw fa-lg"></i>
                                            <label>Sharer : </label>
                                            {$session_data['presentors']}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-map-marker fa-fw fa-lg"></i>
                                            <label>Location : </label>
                                            {$session_data['location']}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-list-ol fa-fw fa-lg"></i>
                                            <label>Total Share(s) : </label>
                                            {$session_data['total_shares']}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-map fa-fw fa-lg"></i>
                                            <label>Venue : </label>
                                            {$session_data['venue']}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="about-list-item">
                                            <i class="fa fa-clock-o fa-fw fa-lg"></i>
                                            <label>Started At : </label>
                                            {$session_data['added_on']|date_format:"%e %B %Y"}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="about-list-item">
                                            {foreach $sessionInterestArr as $interest_key => $interest_value}
                                                <a href="{$system['system_url']}/interest/{$interest_value['interest_id']}" class="span interest_tag">{$interest_value['session_name']}</a>
                                            {/foreach}
                                        </div>
                                    </li>
                                    <!-- category -->
                                </ul>
                            </div>
                        </div>
                        <!-- about -->
                    </div>
                {elseif $view == "photos"}
                    <!-- photos -->
                    <div class="col-xs-12">
                        <div class="panel panel-default panel-photos">
                            <div class="panel-heading with-icon">
                                <!-- panel title -->
                                <div>
                                    <i class="fa fa-picture-o pr5 panel-icon"></i>
                                    <strong>{__("Photos")}</strong>
                                </div>
                                <!-- panel title -->

                            </div>
                            <div class="panel-body">
                                {if count($session_data['photos']) > 0}
                                    <ul class="row">
                                        {foreach $session_data['photos'] as $key=>$photo}
                                            <div class="col-xs-6 col-sm-2">
                                                <a class="pg_photo {if !$_small}large{/if} js_lightbox-nodata" href="{$system['system_uploads']}/{$photo}" data-id="{$photo}" data-image="{$system['system_uploads']}/{$photo}" data-context="{$_context}" style="background-image:url({$system['system_uploads']}/{$photo});"></a>
                                            </div>
                                        {/foreach}
                                    </ul>
                                    {*{if count($session_data['photos']) >= $system['min_results_even']}
                                    <!-- see-more -->
                                    <div class="alert alert-info see-more js_see-more" data-get="photos" data-id="{$session_data['sessions_id']}" data-type='page'>
                                    <span>{__("See More")}</span>
                                    <div class="loader loader_small x-hidden"></div>
                                    </div>
                                    <!-- see-more -->
                                    {/if}*}
                                {else}
                                    <p class="text-center text-muted mt10">
                                        {$session_data['title']} {__("doesn't have photos")}
                                    </p>
                                {/if}
                            </div>
                        </div>
                    </div>
                    <!-- photos -->

                {elseif $view == "discussion"}
                    <!-- photos -->
                    <div class="col-xs-12">
                        <div class="panel panel-default panel-photos">
                            <div class="panel-heading with-icon">
                                <!-- panel title -->
                                <div>
                                    <i class="fa fa-info pr5 panel-icon"></i>
                                    <strong>{__("Discussion")}</strong>
                                </div>
                                <!-- panel title -->

                            </div>
                            <div class="panel-body">
                                <ul class="discussed_list">

                                    {foreach $session_data['discussions'] as $key=>$discussions}
                                        <li>
                                            <div class="conversation clearfix {if $discussions['user_id'] == $user->_data['user_id']}{*right*}{/if}" id="{$discussions['sessions_discussion_id']}">
                                                <div class="conversation-user">
                                                    <a href="{$system['system_url']}/{$discussions['user_data']['user_name']}">
                                                        <img src="{$discussions['user_data']['user_picture']}" title="{$discussions['user_data']['user_fullname']}" alt="{$discussions['user_data']['user_fullname']}">
                                                    </a>
                                                </div>
                                                <div class="conversation-body">
                                                    <div class="text">
                                                        <p><a href="{$system['system_url']}/{$discussions['user_data']['user_name']}">{$discussions['user_data']['user_name']}</a></p>
                                                            {$discussions['post']}
                                                    </div>
                                                    <div class="time js_moment" data-time="{$discussions['added_on']}">
                                                        {$discussions['added_on']}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    {/foreach}
                                </ul>
                                <form class="js_ajax_add_discussion form-horizontal" method="post">
                                    {if $session_data['event_expired']} <label class="alert alert-danger col-sm-12">This Session is Expired, you can Comment on it.</label> {/if}
                                    <input type="hidden" name="id" value="{$session_data['sessions_id']}">
                                    <input type="hidden" name="edit" value="add_discussion">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Post new comment
                                        </label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="discussion_post" type="text" required="" {if $session_data['event_expired']}disabled{/if}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button type="submit" class="btn btn-primary" {if $session_data['event_expired']}disabled{/if}>POST</button>
                                        </div>
                                    </div>
                                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- photos -->
                {elseif $view == "comments"}
                    <!-- photos -->
                    <div class="col-xs-12">
                        <div class="panel panel-default panel-photos">
                            <div class="panel-heading with-icon">
                                <!-- panel title -->
                                <div>
                                    <i class="fa fa-info pr5 panel-icon"></i>
                                    <strong>{__("Comments")}</strong>
                                </div>
                                <!-- panel title -->

                            </div>
                            <div class="panel-body">
                                <ul class="discussed_list">

                                    {foreach $session_data['comments'] as $key=>$comments}
                                        <li>
                                            <div class="conversation clearfix {if $comments['user_id'] == $user->_data['user_id']}{*right*}{/if}" id="{$comments['sessions_comment_id']}">
                                                <div class="conversation-user">
                                                    <a href="{$system['system_url']}/{$comments['user_data']['user_name']}">
                                                        <img src="{$comments['user_data']['user_picture']}" title="{$comments['user_data']['user_fullname']}" alt="{$comments['user_data']['user_fullname']}">
                                                    </a>
                                                </div>
                                                <div class="conversation-body">
                                                    <div class="text">
                                                        <p><a href="{$system['system_url']}/{$comments['user_data']['user_name']}">{$comments['user_data']['user_name']}</a></p>
                                                            {$comments['post']}
                                                    </div>
                                                    <div class="time js_moment" data-time="{$comments['added_on']}">
                                                        {$comments['added_on']}
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    {/foreach}
                                </ul>
                                <form class="js_ajax_add_discussion form-horizontal" method="post">
                                    {if !$session_data['i_join']} <label class="alert alert-danger col-sm-12">Please <strong>Join</strong> the session to discuss on it.</label> {/if}
                                    <input type="hidden" name="id" value="{$session_data['sessions_id']}">
                                    <input type="hidden" name="edit" value="add_comment">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Post new comment
                                        </label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="comment_post" type="text" required="" {if !$session_data['i_join']}disabled{/if}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3">
                                            <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>POST</button>
                                        </div>
                                    </div>
                                    <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
                                    <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- photos -->
                {elseif $view == "queans"}
                    <!-- photos -->
                    <div class="col-xs-12">
                        <div class="panel panel-default panel-photos">
                            <div class="panel-heading with-icon">
                                <!-- panel title -->
                                <div>
                                    <i class="fa fa-info pr5 panel-icon"></i>
                                    <strong>{__("Questions and Answers")}</strong>
                                </div>
                                <!-- panel title -->

                            </div>
                            <div class="panel-body">
                                <div class="ask_a_question">
                                    <form class="js_ajax_add_qa_question form-horizontal" method="post">
                                        <input type="hidden" name="id" value="0">
                                        <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                                        <input type="hidden" name="edit" value="add_qa_question">
                                        {if !$session_data['i_join']}
                                            <div class="alert alert-danger"> Please join the Session to ask Questions! </div>
                                        {/if}
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">
                                                Ask new Question
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="question_post" type="text" required="" {if !$session_data['i_join']}disabled{/if}></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>POST</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="pull-right">
                                    <select class="form-control" onchange="commentSorting(this.value, {$session_data['sessions_id']});">
                                        <option value="date_asc">Date : ASC</option>
                                        <option value="date_desc">Date : DESC</option>
                                        <option value="rating_asc">Rating : ASC</option>
                                        <option value="rating_desc">Rating : DESC</option>
                                    </select>
                                </div>
                                <ul id="queans_list">

                                    {foreach $session_data['qaData'] as $key=>$qas}
                                        <li data-id="{$qas['sessions_qa_id']}" data-userid="{$qas['user_id']}">
                                            <div class="conversation clearfix question_div {if $qas['user_id'] == $user->_data['user_id']}{*right*}{/if}" id="{$qas['sessions_qa_id']}">
                                                <div class="conversation-user">
                                                    <a href="{$system['system_url']}/{$qas['user_data']['user_name']}">
                                                        <img src="{$qas['user_data']['user_picture']}" title="{$qas['user_data']['user_fullname']}" alt="{$qas['user_data']['user_fullname']}">
                                                    </a>
                                                </div>
                                                <div class="conversation-body">
                                                    <div class="text">
                                                        <p><a href="{$system['system_url']}/{$qas['user_data']['user_name']}">{$qas['user_data']['user_name']}</a></p>
                                                            {$qas['post']}
                                                    </div>
                                                    <div class="time js_moment" data-time="{$qas['added_on']}">
                                                        {$qas['added_on']}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix row" style="margin:25px 0 5px 40px">
                                                <span class="text-link js_open_answer_box">Answer</span> ({$qas['answers']|count}) | 
                                                {if $qas['i_like']}
                                                    <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
                                                {else}
                                                    <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
                                                {/if}
                                                (<span class="js_qacomment-likes-num_{$qas['sessions_qa_id']}">{$qas['total_likes']}</span>) |
                                                <span class="text-link publish_session">Share</span>

                                                <form class="js_ajax_add_qa_answer form-horizontal" method="post">
                                                    <input type="hidden" name="id" value="{$qas['sessions_qa_id']}">
                                                    <input type="hidden" name="posted_user_id" value="{$qas['user_id']}">
                                                    <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                                                    <input type="hidden" name="edit" value="add_qa_answer">

                                                    {if !$session_data['i_join']}
                                                        <div class="alert alert-danger"> Please join the Session to Answer this Question! </div>
                                                    {/if}

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">
                                                            Answer above Question
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <textarea class="form-control" name="comment_post" type="text" required="" {if !$session_data['i_join']}disabled{/if}></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-9 col-sm-offset-3">
                                                            <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>SUBMIT</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <ul class="answers">
                                                {foreach $qas['answers'] as $k=>$subqas}
                                                    <li data-id="{$subqas['sessions_qa_id']}" data-userid="{$subqas['user_id']}">
                                                        <div class="conversation clearfix ">
                                                            <div class="conversation-user">
                                                                <a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">
                                                                    <img src="{$subqas['user_data']['user_picture']}" title="{$subqas['user_data']['user_fullname']}" alt="{$subqas['user_data']['user_fullname']}">
                                                                </a>
                                                            </div>
                                                            <div class="conversation-body full_box">
                                                                <div class="text">
                                                                    <p><a href="{$system['system_url']}/{$subqas['user_data']['user_name']}">{$subqas['user_data']['user_name']}</a></p>
                                                                    <p class="answer_div more"> {$subqas['post']} </p>
                                                                </div>
                                                                <div class="time js_moment" data-time="{$subqas['added_on']}">
                                                                    {$subqas['added_on']}
                                                                </div>
                                                            </div>
                                                            {foreach $subqas['replys'] as $k=>$replys} 
                                                                <div class="conversation-body full_box reply_data">
                                                                    <div class="text">
                                                                        <p><a href="{$system['system_url']}/{$replys['user_data']['user_name']}">{$replys['user_data']['user_name']}</a></p>
                                                                        <p class="answer_div more"> {$replys['post']} </p>
                                                                    </div>
                                                                    <div class="time js_moment" data-time="{$replys['added_on']}">
                                                                        {$replys['added_on']}
                                                                    </div>
                                                                </div>
                                                            {/foreach}
                                                        </div>
                                                        <div class="conversation clearfix">
                                                            {if $subqas['i_like']}
                                                                <span class="{if $session_data['i_join']}text-link js_unlike-qacomment{/if}">{__("Upvoted")}</span>
                                                            {else}
                                                                <span class="{if $session_data['i_join']}text-link js_like-qacomment{/if}">{__("Upvote")}</span>
                                                            {/if}
                                                            (<span class="js_qacomment-likes-num_{$subqas['sessions_qa_id']}">{$subqas['total_likes']}</span>)
                                                            &nbsp;&nbsp;
                                                            <span class="text-link js_open_reply_box">Reply</span>
                                                            <div class="reply_box_div">
                                                                <br/>
                                                                <form class="js_ajax_add_qa_reply form-horizontal" method="post">
                                                                    <input type="hidden" name="id" value="{$subqas['sessions_qa_id']}">
                                                                    <input type="hidden" name="posted_user_id" value="{$subqas['user_id']}">
                                                                    <input type="hidden" name="sessions_id" value="{$session_data['sessions_id']}">
                                                                    <input type="hidden" name="edit" value="add_qa_reply">
                                                                    {if !$session_data['i_join']}
                                                                        <div class="alert alert-danger"> Please join the Session to Reply on this Question! </div>
                                                                    {/if}
                                                                    <div class="form-group">
                                                                        <div class="col-sm-11">
                                                                            <textarea class="form-control" name="comment_post" type="text" rows="1" required="" {if !$session_data['i_join']}disabled{/if}></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-11">
                                                                            <button type="submit" class="btn btn-primary" {if !$session_data['i_join']}disabled{/if}>REPLY</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                {/foreach}
                                            </ul>
                                        </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- photos -->

                {/if}
            </div>
            <!-- profile-content -->
        </div>

    </div>
</div>
<!-- page content -->

{include file='_footer.tpl'}