<div class="panel panel-default">
    <div class="panel-heading with-icon">
        <i class="fa fa-list-ol pr5 panel-icon"></i>
        <strong>{__("Interest Request")}</strong>
    </div>
    <div class="panel-body">

        <!-- Design -->
        <!-- success -->
        <div class="alert alert-success mb0 mt10 x-hidden" role="alert"></div>
        <!-- success -->

        <!-- error -->
        <div class="alert alert-danger mb0 mt10 x-hidden" role="alert"></div>
        <!-- error -->
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover js_dataTable">
                            <thead>
                                <tr>
                                    <th>{__("Interest")}</th>
                                    <th>{__("Parent Interest")}</th>
                                    <th>{__("Created By")}</th>
                                    <th>{__("Added Date")}</th>
                                    <th>{__("Actions")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $rows as $row}
                                    <tr>
                                        <td>{$row['interest']}</td>
                                        <td>{$row['parent_interest']}</td>
                                        <td>{$row['created_by']['user_name']}</td>
                                        <td>{$row['added_on']|date_format:"%e %B %Y"}</td>
                                        <td>
                                            <button data-toggle="tooltip" data-placement="top" title='{__("Decline")}' class="btn btn-xs btn-danger js_admin-interest_request-decline" data-handle="session" data-id="{$row['interest_request_id']}" data-created_by="{$row['created_by']['user_id']}">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            <a data-toggle="tooltip" data-placement="top" title='{__("Approve")}' class="btn btn-xs btn-success js_admin-interest_request-verify" data-handle="session" data-id="{$row['interest_request_id']}" data-created_by="{$row['created_by']['user_id']}">
                                                <i class="fa fa-check"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Design -->
    </div>
</div>