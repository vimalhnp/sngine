<?php

require('bootstrap.php');

$db->query(sprintf("DELETE FROM user_interest WHERE user_id = %s", secure($user->_data['user_id'], 'int'))) or _error(SQL_ERROR_THROWEN);
foreach ($_POST['interest_dropdown'] as $interest_key => $interest_value) {
    $db->query(sprintf("INSERT INTO user_interest (user_id, interest, added_on, modified_on) VALUES (%s, %s, %s, %s)", secure($user->_data['user_id'], 'int'), secure($interest_value), secure(date("Y-m-d H:i:s")), secure(date("Y-m-d H:i:s")))) or _error(SQL_ERROR_THROWEN);
}

header("Location: " . $system['system_url'] . "/my_interest");